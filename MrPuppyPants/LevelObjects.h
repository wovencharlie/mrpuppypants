//
//  LevelObjects.h
//  MrPants
//
//  Created by Charles Schulze on 9/9/12.
//  Copyright 2012 CS54 INC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface LevelObjects : CCNode
{

    
}

@property (nonatomic,retain) NSString *bgImage;
@property (nonatomic,retain) NSString *sunImage;
@property (nonatomic,retain) NSString *midBgImage;
@property (nonatomic,retain) NSString *levelImage;
@property (nonatomic,retain) NSString *altImage;
@property (nonatomic,retain) NSMutableArray *ballsArray;

@property (nonatomic,assign) CGPoint bgImagePoint;
@property (nonatomic,assign) CGPoint sunImagePoint;
@property (nonatomic,assign) CGPoint midImagePoint;
@property (nonatomic,assign) CGPoint altImagePoint;
@property (nonatomic,assign) CGPoint mrPantsStartPoint;
@property (nonatomic,assign) int maxAltitude;
@property (nonatomic,assign) int minAltitude;


@end
