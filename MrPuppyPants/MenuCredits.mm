//
//  MenuBetweenLevels.m
//  MrPants
//
//  Created by Charles Schulze on 9/15/12.
//  Copyright 2012 CS54 INC. All rights reserved.
//

#import "MenuCredits.h"
#import "GameData.h"
#import "MenuSelectLevel.h"
#import "Constants.h"
#import "ButtDance.h"
#import "BaseLevel.h"
#import "MenuBuyGuns.h"
#import "IntroLayer.h"

@implementation MenuCredits

+(CCScene *) scene
{
	CCScene *scene = [CCScene node];
    MenuCredits *layer = [MenuCredits node];
    [scene addChild: layer];
    return scene;
}

-(id) init
{
    if((self = [super init]))
    {
        //175
        CGSize size = [[CCDirector sharedDirector] winSize];
        
        gameData = [GameData sharedData];
        //int currentLevel = [gameData returnLevel];
        CGPoint menuButtonPosition = ccp(105, 280);
        CGPoint mainMenusPosition = ccp(size.width/2,(size.height - 40)/2);
        
        if(IS_IPAD){
            menuButtonPosition = ccp(210, 680);
        }
        
        CCSprite *imageSprite = [CCSprite spriteWithFile:IMAGE_BG_THANKS_SCREEN];
        imageSprite.position = ccp(size.width / 2,size.height/2);
        [self addChild:imageSprite];
        
        CCSpriteFrameCache *frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
       
        CCMenuItemImage *backButton = [CCMenuItemImage itemWithNormalSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_BL_BACK_BUTTON]]
                                                             selectedSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_BL_BACK_BUTTON]]
                                                                     target:self selector:@selector(backButtonTapped:)];
        
        
        CCMenu *menuLower = [CCMenu menuWithItems:backButton, nil];
        menuLower.position = menuButtonPosition;
        [menuLower alignItemsHorizontally];
        [self addChild:menuLower];
        
    }
    
   	return self;
}


- (void)musicOnOffTapped:(id)sender{

    //CCTransitionSplitRows *transition=[CCTransitionFade transitionWithDuration:.2 scene:[MenuBuyGuns scene]];
    //[[CCDirector sharedDirector] replaceScene:transition];
    if([gameData areSoundFXMuted]){
        [gameData turnSoundFXOn];
    }else{
        [gameData turnSoundFXOff];
    }

}

- (void)effectsOnOffTapped:(id)sender
{
    //CCTransitionSplitRows *transition=[CCTransitionSplitRows transitionWithDuration:.7 scene:[MenuSelectLevel scene]];
    //[[CCDirector sharedDirector] replaceScene:transition];
    if([gameData areAmbientFXMuted]){
        [gameData turnAmbientFXOn];
    }else{
        [gameData turnAmbientFXOff];
    }
}

- (void)backButtonTapped:(id)sender
{
    [[CCDirector sharedDirector] replaceScene:[IntroLayer scene]];
}

-(void)dealloc
{
    [super dealloc];
}

@end
