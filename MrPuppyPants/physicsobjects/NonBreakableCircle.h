//
//  RopeThrower.h
//  verletRopeTestProject
//
//  Created by Muhammed Ali Khan on 8/19/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "BaseBreakableItem.h"
#import "Cat01.h"

@class NonBreakableCircle;

@protocol BreakableBlockDelegate <NSObject>
- (void)bombExplodedAtPosition:(CGPoint)pos;
@end

typedef enum circleTypes {WHEEL,ROCK} CircleType;
@interface NonBreakableCircle : BaseBreakableItem
{
    CCSprite *boxSprite;
    CircleType circleTypes;
}

- (id) initInParent:(CCLayer *)parentLayer At:(CGPoint)pos rotateTo:(float)rotation withBlockType:(CircleType)theCircleType inWorld:(b2World *)world;
- (void)movePositionY:(float)y andX:(float)x;
- (void)update:(ccTime) dt;
@property(nonatomic,assign)id delegate;

@end

