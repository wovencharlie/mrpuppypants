//
//  RopeThrower.h
//  verletRopeTestProject
//
//  Created by Muhammed Ali Khan on 8/19/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "BaseBullet.h"

@interface Bullet : BaseBullet {
    

}

@property (nonatomic,readwrite,assign) float radius;
@property (nonatomic,readwrite,assign) float density;
@property (nonatomic,readwrite,assign) float restitution;
@property (nonatomic,readwrite,assign) float friction;
@property (nonatomic, retain) NSString *ballImage;
@property (nonatomic,assign) int tag;
-(void) resetPosition:(CGPoint)pos;
@end

