//
//  RopeThrower.m
//  verletRopeTestProject
//
//  Created by Muhammed Ali Khan on 8/19/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "BreakableTrashCan.h"
#import "CCAnimationHelper.h"
#import "Constants.h"

@implementation BreakableTrashCan
- (id) initInParent:(CCLayer *)parentLayer At:(CGPoint)pos rotateTo:(float)rotation inWorld:(b2World *)world{
	if((self = [super init]))
    {
        _world = world;
	}
    
    [self createItem:pos rotateTo:rotation];
    //[self performSelector:@selector(remove) withObject:nil afterDelay:3];
	return self;
}
-(void) createItem:(CGPoint)pos rotateTo:(float)rotation
{
    [super createItem:pos rotateTo:rotation];
    b2BodyDef bodyDef;
    bodyDef.type = b2_dynamicBody;
    boxSprite = [CCSprite spriteWithSpriteFrameName:IMAGE_TRASH_CAN];
    
    [self addChild:boxSprite z:0 tag:TAG_BREAKABLE_ITEM];
    bodyDef.userData = boxSprite;
    b2Body *body = _world->CreateBody(&bodyDef);
    body->SetTransform(b2Vec2(pos.x, pos.y),rotation);
    b2PolygonShape shape;
    
    float width = (float) SIZE_CRATE_01_WIDTH / 64;
    float height = (float) 24 / 64;
    
    shape.SetAsBox(width,height);
    b2FixtureDef fixtureDef;
    fixtureDef.shape = &shape;
    fixtureDef.density = 2.0f;
    fixtureDef.friction = 1.0f;
    fixtureDef.restitution = 0.1f;
    body->CreateFixture(&fixtureDef);
    
    pointedBody = body;
    hitForceToBreak_ = 10.0f;
    hitForceToChangeTexture_ = .1f;
    
    emitter = [[CCParticleSystemQuad alloc]initWithFile:PLIST_TRASH_CAN_FIRE];
    emitter.scale = .5;
    emitter.tag = EMITTER_TAG;
    emitter.position = ccp((pos.x * 32) - 3,(pos.y * 32) + 15);
    //[self addChild:emitter z:-10];

}

-(void) showExplodeTexture
{
    [self performSelector:@selector(exploder) withObject:nil afterDelay:0.5];
    
    
}
-(void)exploder
{
    // emitter = [[CCParticleSystemQuad alloc]initWithFile:BLOCK_BREAK_EMITTER_PLIST];
    //emitter.position = boxSprite.position;
    //[self addChild:emitter];
    NSLog(@"show explod texture");
}

-(void) showHurtTexture
{
    /*
    [self removeChildByTag:EMITTER_TAG cleanup:YES];
    [emitter stopSystem];
    [emitter release];
    emitter = nil;
     */
    [self changeTexture];
}

-(void) changeTexture
{
    CCTexture2D* tex = [[CCTextureCache sharedTextureCache] addImage:IMAGE_TRASH_CAN];
    [boxSprite setTexture: tex];
}

- (void) dealloc
{
    [super dealloc];
}
@end


