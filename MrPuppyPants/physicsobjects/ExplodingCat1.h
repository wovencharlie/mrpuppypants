//
//  RopeThrower.h
//  verletRopeTestProject
//
//  Created by Muhammed Ali Khan on 8/19/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "BaseBreakableItem.h"
#import "Cat01.h"
#import "Cat02.h"
#import "Cat03.h"
#import "CCSprite.h"

typedef enum catType {CAT_01,CAT_02,CAT_03} CatType;

@protocol ExplodingCat1Delegate <NSObject>
- (void)catExplodedAtPosition:(CGPoint)pos;
@end

@interface ExplodingCat1 : BaseBreakableItem 
{
    CCSprite *boxSprite;
    CatType theCatType;
}
@property(nonatomic,assign)id delegate;
- (id) initInParent:(CCLayer *)parentLayer At:(CGPoint)pos rotateTo:(float)rotation inWorld:(b2World *)world  catType:(CatType)catType;
@end

