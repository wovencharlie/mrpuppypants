//
//  RopeThrower.m
//  verletRopeTestProject
//
//  Created by Muhammed Ali Khan on 8/19/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ExplodingCat1.h"
#import "CCAnimationHelper.h"
#import "BaseLevel.h"
@implementation ExplodingCat1

@synthesize delegate;

- (id) initInParent:(CCLayer *)parentLayer At:(CGPoint)pos rotateTo:(float)rotation inWorld:(b2World *)world catType:(CatType)catType {
	if((self = [super init]))
    {
        _world = world;
        theCatType = catType;
	}
    
    
    //[super initInParent:parentLayer At:pos rotateTo:rotation inWorld:world];
    [self createItem:pos rotateTo:rotation];
    //[self performSelector:@selector(remove) withObject:nil afterDelay:3];
	return self;
}
-(void) createItem:(CGPoint)pos rotateTo:(float)rotation
{
    [super createItem:pos rotateTo:rotation];
        
    b2BodyDef bodyDef;
    bodyDef.type = b2_dynamicBody;
    
    float density = 0.8f;
    float restitution = .1;
    int theTag = TAG_BREAKABLE_ENEMY;
    
    switch (theCatType){
        case CAT_01:
            boxSprite = [[Cat01 alloc] initWithImage];
            break;
        case CAT_02:
            boxSprite = [[Cat02 alloc] initWithImage];
            break;
        case CAT_03:
            boxSprite = [[Cat03 alloc] initWithImage];
            theTag = TAG_BREAKABLE_ENEMY_NON_HURTING;
            density = 5;
            restitution = 0;
            break;
        default:
            //none
            
            break;
    }
    
    
    [self addChild:boxSprite z:0 tag:theTag];
    bodyDef.userData = boxSprite;
    b2Body *body = _world->CreateBody(&bodyDef);
    body->SetTransform(b2Vec2(pos.x, pos.y),rotation);
    b2PolygonShape shape;
    
    CGPoint spritePosition = boxSprite.anchorPoint;
    spritePosition.y = boxSprite.anchorPoint.y - .1;
    boxSprite.anchorPoint = spritePosition;
    float width = (float)SIZE_CAT_WIDTH / 64;
    float height = (float)SIZE_CAT_HEIGHT / 64;
    NSLog(@"%f",width);
    NSLog(@"%f",height);
    
    int hitForceMultiplier = 1;
    if(IS_IPAD)
    {
        width *= 2;
        height *= 2;
        hitForceMultiplier = 12;
        density *=2;
        //density *=2;
    }
    
    
    shape.SetAsBox(width,height);
    b2FixtureDef fixtureDef;
    fixtureDef.shape = &shape;
    fixtureDef.filter.groupIndex = COLLIDE_GENERIC_ALWAYS_COLLIDE;
    fixtureDef.density = density;
    fixtureDef.friction = 1.0f;
    fixtureDef.restitution = restitution;
    body->CreateFixture(&fixtureDef);
    
    scoreAmount = 5000;
    pointedBody = body;
    //changeSprite = boxSprite;
    hitForceToBreak_ = TO_BREAK_CAT * hitForceMultiplier;
    hitForceToChangeTexture_ = TO_BREAK_CAT / 2;
}

-(void) showExplodeTexture
{
    NSLog(@"SHOW EXPLOD TEXTURE");
    //TODO CS - make base cat class :-D
    [boxSprite showExplodeState];
}

-(void)remove{
    CGPoint myPos = CGPointMake(pointedBody->GetPosition().x, pointedBody->GetPosition().y);
    [self.delegate catExplodedAtPosition:myPos];
    [super remove];
    
}

-(void) showHurtTexture
{
    [boxSprite showHurtState];
}

- (void) dealloc
{
    [boxSprite release];
    boxSprite = nil;
    [super dealloc];
}
@end


