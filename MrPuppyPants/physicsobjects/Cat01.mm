//
//  Ship.m
//  SpriteBatches
//
//  Created by Steffen Itterheim on 04.08.10.
//
//  Updated by Andreas Loew on 20.06.11:
//  * retina display
//  * framerate independency
//  * using TexturePacker http://www.texturepacker.com
//
//  Copyright Steffen Itterheim and Andreas Loew 2010-2011.
//  All rights reserved.
//

#import "Cat01.h"

#import "CCAnimationHelper.h"
#import "SimpleAudioEngine.h"
#import "GameData.h"
@interface Cat01 (PrivateMethods)
-(id) initWithShipImage;
@end


@implementation Cat01


- (id) initWithImage{
	if((self = [super initWithSpriteFrameName:CAT_ANIMATION_IDLE_FRAME]))
    {
        self.tag = TAG_BREAKABLE_ENEMY;
        [self showIdleState];
        
	}
	return self;
}

-(void)showIdleState
{
    //0 - 102
    [self stopAllActions];
    float nextBurp = rand() % (20 - 5) + 5;
    [self unschedule:@selector(shouldBurp:)];
    [self unschedule:@selector(shouldShowIdleState:)];
    
    [self schedule:@selector(shouldBurp:) interval:nextBurp];
    
    CCAnimation* anim = [CCAnimation animationWithFrame:CAT_ANIMATION_BASE_FRAME frameCount:91 delay:0.02f startFrame:101];
    
    // run the animation by using the CCAnimate action
    CCAnimate* animate = [CCAnimate actionWithAnimation:anim];
    CCRepeatForever* repeat = [CCRepeatForever actionWithAction:animate];
    [self runAction:repeat];
    
    //[self burpState];
}
-(void)shouldBurp: (ccTime) dt
{
    //[self unschedule:@selector(shouldBurp:)];
    //[self unschedule:@selector(shouldShowIdleState:)];
    //[self schedule:@selector(shouldShowIdleState:) interval:5.0f];
    
    
    [self burpState];
}
-(void)shouldShowIdleState:(ccTime) dt
{
    [self showIdleState];
}

-(void)burpState
{
    [self stopAllActions];
    
    //CCAnimation* anim = [CCAnimation animationWithFrame:@"cat-animation.swf/0" frameCount:191 -101 delay:0.02f startFrame:0];
    CCAnimation* anim = [CCAnimation animationWithFrame:CAT_ANIMATION_BASE_FRAME frameCount:101 delay:0.02f startFrame:0];
    CCAnimate* animate = [CCAnimate actionWithAnimation:anim];
    [self runAction:animate];
    [self unschedule:@selector(shouldBurp:)];
    [self schedule:@selector(shouldShowIdleState:) interval:2.5];
    
    if(ENABLE_AUDIO && ![[GameData sharedData]areAmbientFXMuted]){
        [[SimpleAudioEngine sharedEngine] playEffect:AUDIO_CAT_BURP];
    }
}

-(void)showExplodeState
{
    [self stopAllActions];
    //400 193 - 385;
    
    if(ENABLE_AUDIO){
        [[SimpleAudioEngine sharedEngine] playEffect:AUDIO_CAT_EXPLODE];
    }
    
    // create an animation object from all the sprite animation frames
    CCAnimation* anim = [CCAnimation animationWithFrame:CAT_ANIMATION_BASE_FRAME frameCount:36 delay:0.02f startFrame:193];
    
    // run the animation by using the CCAnimate action
    CCAnimate* animate = [CCAnimate actionWithAnimation:anim];
    //CCRepeatForever* repeat = [CCRepeatForever actionWithAction:animate];
    [self runAction:animate];
    
    [self unschedule:@selector(shouldShowIdleState:)];
    [self unschedule:@selector(shouldBurp:)];
}
-(void)showHurtState
{
    if(ENABLE_AUDIO && ![[GameData sharedData]areAmbientFXMuted]){
        [[SimpleAudioEngine sharedEngine] playEffect:AUDIO_BALL_BOUNCE];
    }
    
    [self unschedule:@selector(shouldShowIdleState:)];
    [self unschedule:@selector(shouldBurp:)];
    [self schedule:@selector(shouldBurp:) interval:3];
    
    
    [self stopAllActions];
    CCAnimation* anim = [CCAnimation animationWithFrame:CAT_ANIMATION_BASE_FRAME frameCount:104 delay:0.02f startFrame:0];
    CCAnimate* animate = [CCAnimate actionWithAnimation:anim];
    [self runAction:animate];
}

-(void)dealloc
{
    
    [super dealloc];
}



@end
