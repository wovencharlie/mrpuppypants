//
//  RopeThrower.h
//  verletRopeTestProject
//
//  Created by Muhammed Ali Khan on 8/19/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "BaseBreakableItem.h"

@interface Plank01 : BaseBreakableItem
{
    CCSprite *boxSprite;
    int thePlankType;
}

- (id) initInParent:(CCLayer *)parentLayer At:(CGPoint)pos rotateTo:(float)rotation plankType:(int)plankType inWorld:(b2World *)world;
@end

