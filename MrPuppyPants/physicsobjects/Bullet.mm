//
//  RopeThrower.m
//  verletRopeTestProject
//
//  Created by Muhammed Ali Khan on 8/19/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Bullet.h"

@implementation Bullet

- (id) initInParent:(CCLayer *)parentLayer At:(CGPoint)pos inWorld:(b2World *)world{
	if((self = [super init])) 
    {
        _world = world;
        self.radius = 0.10f;
        self.density = 2.0f;
        self.restitution = 0.1f;
        self.friction = 1.0f;
	}
    
    killed = NO;
	return self;
}

-(void) resetPosition:(CGPoint)pos{
    if(!killed){
        pointedBody->SetTransform(b2Vec2(pos.x/PTM_RATIO, pos.y/PTM_RATIO), 0);
    }
}

-(void) createBullet:(CGPoint)pos
{
    if(!self.ballImage)
    {
        self.ballImage = IMAGE_ORANGE_BALL;
    }
    b2BodyDef bodyDef;
    bodyDef.bullet = true;
    bodyDef.type = b2_dynamicBody;
    
    CCSprite *sprite = [CCSprite spriteWithSpriteFrameName:self.ballImage];
    [self addChild:sprite z:0 tag:201];
    sprite.scale = 1;
    bodyDef.position.Set(pos.x/PTM_RATIO, pos.y/PTM_RATIO);
    bodyDef.userData = sprite;
    b2Body *body = _world->CreateBody(&bodyDef);
    
    b2CircleShape shape;
    float radiusInMeters = self.radius;
    
    if(IS_IPAD){
        radiusInMeters *= 2;
    }
    
    if([self.ballImage isEqualToString:IMAGE_BLACK_BALL]){
        radiusInMeters *= 1.5;
        sprite.scale *= 1.5;
    }
    
    shape.m_radius = radiusInMeters;
    
    b2FixtureDef fixture;
    fixture.filter.groupIndex = COLLIDE_GROUP_BULLET;
    fixture.shape = &shape;
    fixture.density = self.density;
    fixture.friction = self.friction;
    fixture.restitution = self.restitution;
    
    body->CreateFixture(&fixture);
    pointedBody = body;
    
    //[self scheduleOnce:@selector(remove) delay:5];
    [self schedule:@selector(remove) interval:0 repeat:0 delay:5];
    //[self performSelector:@selector(remove) withObject:nil afterDelay:5];
}


- (void) dealloc
{
    self.ballImage = nil;
    [super dealloc];
}
@end


