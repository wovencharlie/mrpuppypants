//
//  RopeThrower.m
//  verletRopeTestProject
//
//  Created by Muhammed Ali Khan on 8/19/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Plank01.h"
#import "CCAnimationHelper.h"
#import "Constants.h"

@implementation Plank01
- (id) initInParent:(CCLayer *)parentLayer At:(CGPoint)pos rotateTo:(float)rotation plankType:(int)plankType inWorld:(b2World *)world{
	if((self = [super init]))
    {
        _world = world;
	}
    
    thePlankType = plankType;
    [self createItem:pos rotateTo:rotation];
    //[self performSelector:@selector(remove) withObject:nil afterDelay:3];
	return self;
}
-(void) createItem:(CGPoint)pos rotateTo:(float)rotation
{
    [super createItem:pos rotateTo:rotation];
    b2BodyDef bodyDef;
    bodyDef.type = b2_dynamicBody;
    
    hitForceThreashhold_ = .5;
    float width = (float) SIZE_PLANK_01_WIDTH / 64;
    float height = (float) SIZE_PLANK_01_HEIGHT / 64;
    
    if(thePlankType == 1)
    {
        boxSprite = [CCSprite spriteWithSpriteFrameName:IMAGE_PLANK_01];
        hitForceToBreak_ = 9930.0f;
        hitForceToChangeTexture_ = 9910.10f;
        hitForceThreashhold_ = 500;
    }
    if(thePlankType == 2)
    {
        boxSprite = [CCSprite spriteWithSpriteFrameName:IMAGE_PLANK_02];
        width = (float) SIZE_PLANK_02_WIDTH / 64;
        height = (float) SIZE_PLANK_02_HEIGHT / 64;
        hitForceToBreak_ = TO_BREAK_PLANK_2;
        hitForceToChangeTexture_ = TO_BREAK_PLANK_2 / 2;

    }
    else if(thePlankType == 3)
    {
        boxSprite = [CCSprite spriteWithSpriteFrameName:IMAGE_PLANK_03];
        width = (float) SIZE_PLANK_03_WIDTH / 64;
        height = (float) SIZE_PLANK_03_HEIGHT / 64;
        hitForceToBreak_ = TO_BREAK_PLANK_3;
        hitForceToChangeTexture_ = TO_BREAK_PLANK_3 / 2;
    }
    
    int multiplier = 1;
    int breakMultiplier = 1;
    if(IS_IPAD)
    {
        width *= 2;
        height *= 2;
        multiplier = 2;
        hitForceThreashhold_ *= 2;
        breakMultiplier = 3;
    }
    
    hitForceToBreak_ *= breakMultiplier;
    
    [self addChild:boxSprite z:0 tag:TAG_BREAKABLE_ITEM];
    bodyDef.userData = boxSprite;
    b2Body *body = _world->CreateBody(&bodyDef);
    body->SetTransform(b2Vec2(pos.x, pos.y),rotation);
    b2PolygonShape shape;
    
    shape.SetAsBox(width,height);
    b2FixtureDef fixtureDef;
    fixtureDef.filter.groupIndex = COLLIDE_GROUP_PLANK;
    fixtureDef.shape = &shape;
    fixtureDef.density = 1.0 * multiplier;
    fixtureDef.friction = 1.0f;
    fixtureDef.restitution = 0;
    body->CreateFixture(&fixtureDef);
    
    pointedBody = body;
    
}

-(void) showExplodeTexture
{
    //[self performSelector:@selector(exploder) withObject:nil afterDelay:0.5];
    
    
}
-(void)exploder
{
    //emitter = [[CCParticleSystemQuad alloc]initWithFile:BLOCK_BREAK_EMITTER_PLIST];
    //emitter.position = boxSprite.position;
    //[self addChild:emitter];
    NSLog(@"show explod texture");
}

-(void) showHurtTexture
{
    //[self changeTexture];
}

-(void) changeTexture
{
    //CCTexture2D* tex = [[CCTextureCache sharedTextureCache] addImage:IMAGE_CRATE_01_BROKEN];
    //[boxSprite setTexture: tex];
}

- (void) dealloc
{
    [super dealloc];
}
@end


