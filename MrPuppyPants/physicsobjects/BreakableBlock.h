//
//  RopeThrower.h
//  verletRopeTestProject
//
//  Created by Muhammed Ali Khan on 8/19/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "BaseBreakableItem.h"
#import "Cat01.h"

@class BreakableBlock;

@protocol BreakableBlockDelegate <NSObject>
- (void)bombExplodedAtPosition:(CGPoint)pos;
@end

typedef enum blockTypes {CRATE,BARREL,CINDER,TRASH,FISH,BBQ,TV,PLATFORM1,PLATFORM2,WARP_01,WARP_02,CRATE_BOMB} BlockType;
@interface BreakableBlock : BaseBreakableItem
{
    CCSprite *boxSprite;
    BlockType blockType;
}

- (id) initInParent:(CCLayer *)parentLayer At:(CGPoint)pos rotateTo:(float)rotation withBlockType:(BlockType)theBlockType inWorld:(b2World *)world;
- (void)movePositionY:(float)y andX:(float)x;
- (void)update:(ccTime) dt;
@property(nonatomic,assign)id delegate;

@end

