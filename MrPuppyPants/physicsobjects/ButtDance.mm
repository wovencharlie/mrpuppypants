//
//  Ship.m
//  SpriteBatches
//
//  Created by Steffen Itterheim on 04.08.10.
//
//  Updated by Andreas Loew on 20.06.11:
//  * retina display
//  * framerate independency
//  * using TexturePacker http://www.texturepacker.com
//
//  Copyright Steffen Itterheim and Andreas Loew 2010-2011.
//  All rights reserved.
//

#import "ButtDance.h"

#import "CCAnimationHelper.h"
#import "SimpleAudioEngine.h"


@implementation ButtDance


- (id) initWithImage{
	if((self = [super initWithSpriteFrameName:MRPANTS_BUTT_DANCE]))
    {
        self.tag = TAG_BREAKABLE_ENEMY;
        [self showIdleState];
        
	}
	return self;
}

-(void)showIdleState
{
    CCAnimation* anim = [CCAnimation animationWithFrame:MRPANTS_BUTT_DANCE_FRAME frameCount:28 delay:0.04f startFrame:0];
    
    // run the animation by using the CCAnimate action
    CCAnimate* animate = [CCAnimate actionWithAnimation:anim];
    CCRepeatForever* repeat = [CCRepeatForever actionWithAction:animate];
    [self runAction:repeat];
    
    
}

-(void)dealloc
{
    
    [super dealloc];
}



@end
