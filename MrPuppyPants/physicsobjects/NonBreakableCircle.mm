//
//  RopeThrower.m
//  verletRopeTestProject
//
//  Created by Muhammed Ali Khan on 8/19/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "NonBreakableCircle.h"
#import "CCAnimationHelper.h"

@implementation NonBreakableCircle
@synthesize delegate;
- (id) initInParent:(CCLayer *)parentLayer At:(CGPoint)pos rotateTo:(float)rotation withBlockType:(CircleType)theCircleType inWorld:(b2World *)world{
	if((self = [super init]))
    {
        _world = world;
        circleTypes = theCircleType;
	}
    
    
    
    [self createItem:pos rotateTo:rotation];
    //[self performSelector:@selector(remove) withObject:nil afterDelay:3];
	return self;
}
-(void) createItem:(CGPoint)pos rotateTo:(float)rotation
{
    [super createItem:pos rotateTo:rotation];
    
    hitForceThreashhold_ = .5;
    NSString *imageName;
    int16 groupIndex = COLLIDE_GENERIC_ALWAYS_COLLIDE;

    //CGFloat density = 1.0f;
    
    b2BodyDef bodyDef;
    bodyDef.type = b2_dynamicBody;
    int tag = TAG_BREAKABLE_ITEM;
    
    float radiusInMeters = 0.4375f;
    float restitution = 0.6;
    switch (circleTypes)
    
    {
        case WHEEL:
            imageName = @"large-wheel.png";
            hitForceToBreak_ = TO_BREAK_CRATE;
            scoreAmount = 1000;
            //density = 1.0f;
            break;
        case ROCK:
            imageName = @"rock.png";
            hitForceToBreak_ = TO_BREAK_CRATE;
            scoreAmount = 1000;
            //density = 1.0f;
            radiusInMeters = 0.15;
            restitution = 0;
            break;
        default:
            imageName = @"large-wheel.png";
            hitForceToBreak_ = TO_BREAK_CRATE;
            scoreAmount = 1000;
            //density = 1.0f;
            break;     
    }

    hitForceToChangeTexture_ = hitForceToBreak_ / 2;
    
    bodyDef.type = b2_dynamicBody;
    boxSprite = [CCSprite spriteWithSpriteFrameName:imageName];
    [self addChild:boxSprite z:0 tag:TAG_NON_BREAKABLE_OBJECT];
    float yPos = pos.y;
    float xPos = pos.x;
    int multiplier = 1;
    int breakMultiplier = 1;
    if(IS_IPAD){
        //yPos *= 2;
        //xPos *= 2;
        //multiplier = .5;
    }
    
    bodyDef.userData = boxSprite;
    b2Body *body = _world->CreateBody(&bodyDef);
    body->SetTransform(b2Vec2(xPos, yPos),0);
    b2CircleShape shape6;
    
    if(IS_IPAD){
        radiusInMeters *= 2;
    }
    shape6.m_radius = radiusInMeters;
    b2FixtureDef fixtureDef6;
    fixtureDef6.shape = &shape6;
    fixtureDef6.filter.groupIndex = groupIndex;
    fixtureDef6.density = 4.0f * multiplier;
    fixtureDef6.friction = 1.0f;
    fixtureDef6.restitution = restitution;
    body->CreateFixture(&fixtureDef6);
    
    pointedBody = body;
    
}

- (void)update:(ccTime) dt
{
    /*
    CCSprite *imageToRotate = (CCSprite *) pointedBody->GetUserData();
    imageToRotate.rotation += 1;
    switch (blockType){
        case WARP_01:
            //imageName = IMAGE_CRATE_01;
            
            break;
            
        case WARP_02:
            //imageName = IMAGE_BARREL_01;
            //imageToRotate.rotation -= 1;
            break;
            
        default:
            
            break;
    }
     */
}

- (void)movePositionY:(float)y andX:(float)x
{
    b2Vec2 position = pointedBody->GetPosition();
    position.y += y / PTM_RATIO;
    position.x += x / PTM_RATIO;
    pointedBody->SetTransform(position, 0);
}

-(void) showExplodeTexture
{
    [self performSelector:@selector(exploder) withObject:nil afterDelay:0.5];
    NSLog(@"show explode texture");
    
}
-(void)exploder
{
    return;
    switch (circleTypes){
        case ROCK:
            NSLog(@"setOffBomb");
            [self setOffBomb];
            break;
            
        default:
            NSLog(@"non");
            break;
    }
    //emitter = [[CCParticleSystemQuad alloc]initWithFile:BLOCK_BREAK_EMITTER_PLIST];
    //emitter.position = boxSprite.position;
    //[self addChild:emitter];
    //[[SimpleAudioEngine sharedEngine] playEffect:AUDIO_GLASS_BREAK];
    
}

-(void)setOffBomb
{
    b2Vec2 bPos = pointedBody->GetPosition();
    CGPoint explodePosition = CGPointMake(bPos.x * PTM_RATIO, bPos.y * PTM_RATIO);
    [self.delegate bombExplodedAtPosition:explodePosition];
}

-(void) showHurtTexture
{
    [self changeTexture];
}

-(void) changeTexture
{
    
    //CCTexture2D* tex = [[CCTextureCache sharedTextureCache] addImage:imageName];
    //[boxSprite setTexture: tex];
}

- (void) dealloc
{
    [self unschedule:@selector(update:)];
    [super dealloc];
}
@end


