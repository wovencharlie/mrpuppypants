//
//  RopeThrower.m
//  verletRopeTestProject
//
//  Created by Muhammed Ali Khan on 8/19/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ExplodingCat2.h"
#import "CCAnimationHelper.h"
#import "BaseLevel.h"
@implementation ExplodingCat2
- (id) initInParent:(CCLayer *)parentLayer At:(CGPoint)pos rotateTo:(float)rotation inWorld:(b2World *)world{
	if((self = [super init]))
    {
        _world = world;
	}
    
    //[super initInParent:parentLayer At:pos rotateTo:rotation inWorld:world];
    [self createItem:pos rotateTo:rotation];
    //[self performSelector:@selector(remove) withObject:nil afterDelay:3];
	return self;
}
-(void) createItem:(CGPoint)pos rotateTo:(float)rotation
{
    [super createItem:pos rotateTo:rotation];
    
    b2BodyDef bodyDef;
    bodyDef.type = b2_dynamicBody;
    boxSprite = [[Cat02 alloc] initWithImage];
    
    [self addChild:boxSprite z:0 tag:TAG_BREAKABLE_ENEMY];
    bodyDef.userData = boxSprite;
    b2Body *body = _world->CreateBody(&bodyDef);
    body->SetTransform(b2Vec2(pos.x, pos.y),rotation);
    b2PolygonShape shape;
    
    CGPoint spritePosition = boxSprite.anchorPoint;
    spritePosition.y = boxSprite.anchorPoint.y - .1;
    boxSprite.anchorPoint = spritePosition;
    float width = (float)SIZE_CAT_WIDTH / 64;
    float height = (float)SIZE_CAT_HEIGHT / 64;
    NSLog(@"%f",width);
    NSLog(@"%f",height);
    
    if(IS_IPAD)
    {
        width *= 2;
        height *= 2;
        hitForceToBreak_ *=2;
    }
    
    shape.SetAsBox(width,height);
    b2FixtureDef fixtureDef;
    fixtureDef.shape = &shape;
    fixtureDef.filter.groupIndex = COLLIDE_GENERIC_ALWAYS_COLLIDE;
    fixtureDef.density = 1.0f;
    fixtureDef.friction = 1.0f;
    fixtureDef.restitution = 0.1f;
    body->CreateFixture(&fixtureDef);
    
    scoreAmount = 5000;
    pointedBody = body;
    //changeSprite = boxSprite;
    hitForceToBreak_ = TO_BREAK_CAT;
    hitForceToChangeTexture_ = TO_BREAK_CAT / 2;
}

-(void) showExplodeTexture
{
    
    [boxSprite showExplodeState];
    
}

-(void)remove
{
    
    [super remove];
    
}

-(void) showHurtTexture
{
    [boxSprite showHurtState];
}

- (void) dealloc
{
    [boxSprite release];
    boxSprite = nil;
    [super dealloc];
}
@end


