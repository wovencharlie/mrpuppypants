//
//  RopeThrower.m
//  verletRopeTestProject
//
//  Created by Muhammed Ali Khan on 8/19/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "BreakableBlock.h"
#import "CCAnimationHelper.h"

@implementation BreakableBlock
@synthesize delegate;
- (id) initInParent:(CCLayer *)parentLayer At:(CGPoint)pos rotateTo:(float)rotation withBlockType:(BlockType)theBlockType inWorld:(b2World *)world{
	if((self = [super init]))
    {
        _world = world;
         blockType = theBlockType;
	}
    
   
    
    [self createItem:pos rotateTo:rotation];
    //[self performSelector:@selector(remove) withObject:nil afterDelay:3];
	return self;
}
-(void) createItem:(CGPoint)pos rotateTo:(float)rotation
{
    [super createItem:pos rotateTo:rotation];
    
    hitForceThreashhold_ = .5;
    NSString *imageName;
    int16 groupIndex = COLLIDE_GENERIC_ALWAYS_COLLIDE;
    float width;
    float height;
    CGFloat density = 1.0f;
    
    b2BodyDef bodyDef;
    bodyDef.type = b2_dynamicBody;
    int tag = TAG_BREAKABLE_ITEM;
    
    int restitution = 0.1;
    switch (blockType)
    
    {
        case CRATE:
            imageName = IMAGE_CRATE_01;
            width = (float) SIZE_CRATE_01_WIDTH / 64;
            height = (float) SIZE_CRATE_01_HEIGHT / 64;
            hitForceToBreak_ = TO_BREAK_CRATE;
            scoreAmount = 1000;
            density = 1.0f;
            break;
        case CRATE_BOMB:
            imageName = IMAGE_CRATE_BOMB;
            width = (float) SIZE_CRATE_01_WIDTH / 64;
            height = (float) SIZE_CRATE_01_HEIGHT / 64;
            hitForceToBreak_ = TO_BREAK_CRATE;
            tag = TAG_BOMB;
            scoreAmount = 1000;
            density = 1.0f;
            break;
        case TV:
            imageName = IMAGE_TV;
            width = (float) SIZE_TV_WIDTH / 64;
            height = (float) SIZE_TV_HEIGHT / 64;
            hitForceToBreak_ = TO_BREAK_TV;
            scoreAmount = 1000;
            density = 1.0f;
            break;
            
        case BBQ:
            imageName = IMAGE_BBQ;
            width = (float) SIZE_BBQ_WIDTH / 64;
            height = (float) SIZE_BBQ_HEIGHT / 64;
            hitForceToBreak_ = TO_BREAK_BBQ;
            scoreAmount = 1000;
            density = 1.0f;
            break;
            
        case FISH:
            imageName = IMAGE_FISH;
            width = (float) SIZE_FISH_WIDTH / 64;
            height = (float) SIZE_FISH_HEIGHT / 64;
            hitForceToBreak_ = TO_BREAK_FISH;
            scoreAmount = 1000;
            density = 1.0f;
            break;
            
        case BARREL:
            imageName = IMAGE_BARREL_01;
            width = (float) SIZE_BARREL_WIDTH / 64;
            height = (float) SIZE_BARREL_HEIGHT / 64;
            hitForceToBreak_ = TO_BREAK_BARREL;
            scoreAmount = 1000;
            density = 2.0f;
            break;
        case CINDER:
            imageName = IMAGE_CINDERBLOCK_01;
            width = (float) SIZE_CINDER_WIDTH / 64;
            height = (float) SIZE_CINDER_HEIGHT / 64;
            hitForceToBreak_ = TO_BREAK_CINDER;
            scoreAmount = 1000;
            density = 40.0f;
            hitForceThreashhold_ = 500;
            restitution = 0;
            break;
            
        case PLATFORM1:
            imageName = IMAGE_PLATFORM_01;
            width = (float) SIZE_PLATFORM_01_WIDTH / 64;
            height = (float) SIZE_PLATFORM_01_HEIGHT / 64;
            hitForceToBreak_ = 50000;
            scoreAmount = 1000;
            density = 0.0f;
            bodyDef.type = b2_staticBody;
            tag = TAG_NON_BREAKABLE_OBJECT;
            hitForceThreashhold_ = 500;
            break;
            
        case PLATFORM2:
            imageName = IMAGE_PLATFORM_02;
            width = (float) SIZE_PLATFORM_02_WIDTH / 64;
            height = (float) SIZE_PLATFORM_02_HEIGHT / 64;
            hitForceToBreak_ = 50000;
            scoreAmount = 1000;
            density = 0.0f;
            bodyDef.type = b2_staticBody;
            tag = TAG_NON_BREAKABLE__NOT_HURTING_OBJECT;
            
            hitForceThreashhold_ = 500;
            break;
        case WARP_01:
            imageName = WARP_1_ANIMATION_BASE_FRAME;
            width = (float) SIZE_WARP_SPIRAL_WIDTH / 64;
            height = (float) SIZE_WARP_SPIRAL_HEIGHT / 64;
            hitForceToBreak_ = 50000;
            scoreAmount = 1000;
            density = 0.0f;
            bodyDef.type = b2_staticBody;
            tag = TAG_WARP_HOLE;
            groupIndex = COLLIDE_WARP_HOLE;
            hitForceThreashhold_ = 500;
            //[self schedule: @selector(update:) interval:.1];
            break;
        case WARP_02:
            imageName = WARP_2_ANIMATION_BASE_FRAME;
            width = (float) SIZE_WARP_SPIRAL_WIDTH / 64;
            height = (float) SIZE_WARP_SPIRAL_HEIGHT / 64;
            hitForceToBreak_ = 50000;
            scoreAmount = 1000;
            density = 0.0f;
            bodyDef.type = b2_staticBody;
            tag = TAG_WARP_HOLE_02;
            groupIndex = COLLIDE_WARP_HOLE_02;
            hitForceThreashhold_ = 500;
            //[self schedule: @selector(update:) interval:.1];
            break;
            
        default:
            imageName = IMAGE_CRATE_01;
            width = (float) SIZE_CRATE_01_WIDTH / 64;
            height = (float) SIZE_CRATE_01_HEIGHT / 64;
            hitForceToBreak_ = TO_BREAK_CRATE;
            scoreAmount = 1000;
            break;
            
            
    }
    
    if(IS_IPAD)
    {
        width *= 2;
        height *= 2;
    }
    
    hitForceToChangeTexture_ = hitForceToBreak_ / 2;

    //
    if([imageName isEqualToString:WARP_2_ANIMATION_BASE_FRAME]){
        [self startWarpAnimation];
    }
    else if([imageName isEqualToString:WARP_1_ANIMATION_BASE_FRAME]){
        [self startWarp1Animation];
    }else{
        boxSprite = [CCSprite spriteWithSpriteFrameName:imageName];
    }
    
    
    [self addChild:boxSprite z:0 tag:tag];
    bodyDef.userData = boxSprite;
    b2Body *body = _world->CreateBody(&bodyDef);
    body->SetTransform(b2Vec2(pos.x, pos.y),rotation);
    b2PolygonShape shape;
    
    shape.SetAsBox(width,height);
    b2FixtureDef fixtureDef;
    fixtureDef.filter.groupIndex = groupIndex;
    
    fixtureDef.shape = &shape;
    fixtureDef.density = density;
    fixtureDef.friction = 1.0f;
    fixtureDef.restitution = restitution;
    body->CreateFixture(&fixtureDef);
    
    pointedBody = body;
    
    
    
}

-(void)startWarp1Animation{
    // create an animation object from all the sprite animation frames
    boxSprite = [CCSprite spriteWithSpriteFrameName:WARP_1_ANIMATION_IDLE_FRAME];
    CCAnimation* anim = [CCAnimation animationWithFrame:WARP_1_ANIMATION_BASE_FRAME frameCount:15 delay:0.04f startFrame:0];
    
    // run the animation by using the CCAnimate action
    CCAnimate* animate = [CCAnimate actionWithAnimation:anim];
    CCRepeatForever* repeat = [CCRepeatForever actionWithAction:animate];
    [boxSprite runAction:repeat];
}

-(void)startWarpAnimation{
    // create an animation object from all the sprite animation frames
    boxSprite = [CCSprite spriteWithSpriteFrameName:WARP_2_ANIMATION_IDLE_FRAME];
    CCAnimation* anim = [CCAnimation animationWithFrame:WARP_2_ANIMATION_BASE_FRAME frameCount:15 delay:0.04f startFrame:0];
    
    // run the animation by using the CCAnimate action
    CCAnimate* animate = [CCAnimate actionWithAnimation:anim];
    CCRepeatForever* repeat = [CCRepeatForever actionWithAction:animate];
    [boxSprite runAction:repeat];
}

- (void)update:(ccTime) dt
{
    /*
    CCSprite *imageToRotate = (CCSprite *) pointedBody->GetUserData();
    imageToRotate.rotation += 1;
    switch (blockType){
        case WARP_01:
            //imageName = IMAGE_CRATE_01;
            
            break;
            
        case WARP_02:
            //imageName = IMAGE_BARREL_01;
            //imageToRotate.rotation -= 1;
            break;
            
        default:
            
            break;
    }
     */
    
}

- (void)movePositionY:(float)y andX:(float)x
{
    b2Vec2 position = pointedBody->GetPosition();
    position.y += y / PTM_RATIO;
    position.x += x / PTM_RATIO;
    pointedBody->SetTransform(position, 0);
}

-(void) showExplodeTexture
{
    [self performSelector:@selector(exploder) withObject:nil afterDelay:0.5];
    NSLog(@"show explode texture");
    
}
-(void)exploder
{
    switch (blockType){
        case CRATE_BOMB:
            NSLog(@"setOffBomb");
            [self setOffBomb];
            break;
            
        default:
            NSLog(@"non");
            break;
    }
    //emitter = [[CCParticleSystemQuad alloc]initWithFile:BLOCK_BREAK_EMITTER_PLIST];
    //emitter.position = boxSprite.position;
    //[self addChild:emitter];
    //[[SimpleAudioEngine sharedEngine] playEffect:AUDIO_GLASS_BREAK];
    
}

-(void)setOffBomb
{
    b2Vec2 bPos = pointedBody->GetPosition();
    CGPoint explodePosition = CGPointMake(bPos.x * PTM_RATIO, bPos.y * PTM_RATIO);
    [self.delegate bombExplodedAtPosition:explodePosition];
}

-(void) showHurtTexture
{
    [self changeTexture];
}

-(void) changeTexture
{
    
    //CCTexture2D* tex = [[CCTextureCache sharedTextureCache] addImage:imageName];
    //[boxSprite setTexture: tex];
}

- (void) dealloc
{
    [self unschedule:@selector(update:)];
    [super dealloc];
}
@end


