//
//  MenuBetweenLevels.m
//  MrPants
//
//  Created by Charles Schulze on 9/15/12.
//  Copyright 2012 CS54 INC. All rights reserved.
//

#import "EndGoodBoy.h"
#import "GameData.h"
#import "MenuSelectLevel.h"
#import "Constants.h"
#import "ButtDance.h"
#import "BaseLevel.h"
#import "MenuBuyGuns.h"
#import "IntroLayer.h"
#import "MenuBetweenLevels.h"

@implementation EndGoodBoy

+(CCScene *) scene
{
	CCScene *scene = [CCScene node];
    EndGoodBoy *layer = [EndGoodBoy node];
    [scene addChild: layer];
    return scene;
}

-(id) init
{
    if((self = [super init]))
    {
        //175
        [Flurry logEvent:@"BEAT ALL 24 Levels!"]; 
        CGSize size = [[CCDirector sharedDirector] winSize];
        
        gameData = [GameData sharedData];
        //int currentLevel = [gameData returnLevel];
        CGPoint mainMenusPosition = ccp((size.width + 100)/2,(size.height - 170)/2);
        
        if(IS_IPAD){
            mainMenusPosition = ccp((size.width + 200)/2,(size.height - 340)/2);
        }
        
        CCSprite *imageSprite = [CCSprite spriteWithFile:IMAGE_BG_GOOD_BOY];
        imageSprite.position = ccp(size.width / 2,size.height/2);
        [self addChild:imageSprite];
        
        CCSpriteFrameCache *frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
       

        
        CCMenuItemImage *backButton = [CCMenuItemImage itemWithNormalSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_CONTINUE_BUTTON]]
                                                             selectedSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_CONTINUE_BUTTON]]
                                                                     target:self selector:@selector(backButtonTapped:)];
        
        
        CCMenu *menuLower = [CCMenu menuWithItems:backButton, nil];
        menuLower.position = mainMenusPosition;
        [menuLower alignItemsHorizontally];
        [self addChild:menuLower];

        
    }
    
   	return self;
}

- (void)backButtonTapped:(id)sender
{
    [[CCDirector sharedDirector] replaceScene:[MenuBetweenLevels scene]];
}

-(void)dealloc
{
    [super dealloc];
}

@end
