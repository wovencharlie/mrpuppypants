//
//  LevelObjects.m
//  MrPants
//
//  Created by Charles Schulze on 9/9/12.
//  Copyright 2012 CS54 INC. All rights reserved.
//

#import "LevelObjects.h"
#import "Constants.h"

@implementation LevelObjects

@synthesize bgImage,sunImage,midBgImage,levelImage,altImage,ballsArray;
@synthesize bgImagePoint,sunImagePoint,midImagePoint,altImagePoint,mrPantsStartPoint,maxAltitude,minAltitude;

-(id)init
{
    if((self = [super init]))
    {
        
    }
    return self;
}

-(void)dealloc
{
    bgImage = nil;
    sunImage = nil;
    midBgImage = nil;
    levelImage = nil;
    altImage = nil;
    [ballsArray release];
    ballsArray = nil;
    [super dealloc];
}
@end
