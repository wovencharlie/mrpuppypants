//
//  Ship.m
//  SpriteBatches
//
//  Created by Steffen Itterheim on 04.08.10.
//
//  Updated by Andreas Loew on 20.06.11:
//  * retina display
//  * framerate independency
//  * using TexturePacker http://www.texturepacker.com
//
//  Copyright Steffen Itterheim and Andreas Loew 2010-2011. 
//  All rights reserved.
//

#import "MrPants.h"
#import "Constants.h"
#import "CCAnimationHelper.h"


@interface MrPants (PrivateMethods)
-(id) initWithShipImage;
@end


@implementation MrPants
//@synthesize rotationOfGun;

+(id) ship
{
	return [[self alloc] initWithShipImage];
}

-(id) initWithShipImage
{
	// Loading the Ship's sprite using a sprite frame name (eg the filename)
	if ((self = [super initWithSpriteFrameName:MR_PANTS_WALK_IDLE_FRAME]))
	{
        
        self.hasStoppedWalking = YES;
		//[self startWalkAnimation];
        dot = [CCSprite spriteWithSpriteFrameName:IMAGE_DOT];
        [self addChild:dot];
        dot.opacity = 0;
        
        gun = [[SpringGun alloc] initInParent:ccp(0, 0)];

        gun.position = dccp(17, 17);
        [self addChild:gun];

        //[self startJetpackAnimation];
	}
	return self;
}

-(void)shootGun{
    [gun showExplosion];
}


- (void)ball1Selected{
    [gun ball1Selected];
}

- (void)ball2Selected{
    [gun ball2Selected];
}

- (void)ball3Selected{
    [gun ball3Selected];
}

- (void)ball4Selected{
    [gun ball4Selected];
}

-(void)startGenericFlyAnimation{
    // create an animation object from all the sprite animation frames
    CCAnimation* anim = [CCAnimation animationWithFrame:MR_PANTS_BASE_FRAME frameCount:22 delay:0.05f startFrame:0];
    
    // run the animation by using the CCAnimate action
    CCAnimate* animate = [CCAnimate actionWithAnimation:anim];
    CCRepeatForever* repeat = [CCRepeatForever actionWithAction:animate];
    [self runAction:repeat];

}

-(void)startWalkAnimation{
    self.hasStoppedWalking = NO;
    // create an animation object from all the sprite animation frames
    CCAnimation* anim = [CCAnimation animationWithFrame:MR_PANTS_WALK_BASE_FRAME frameCount:11 delay:0.05f startFrame:0];
    
    // run the animation by using the CCAnimate action
    CCAnimate* animate = [CCAnimate actionWithAnimation:anim];
    CCRepeatForever* repeat = [CCRepeatForever actionWithAction:animate];
    [self runAction:repeat];
}

-(void)stopWalkAnimation{
    self.hasStoppedWalking = YES;
    [self stopAllActions];
    
    CCSpriteFrameCache* cache = [CCSpriteFrameCache sharedSpriteFrameCache];
    [self setDisplayFrame:[cache spriteFrameByName:MR_PANTS_WALK_IDLE_FRAME]];
    
}

-(void)startJetpackAnimation{
    
    [self startGenericFlyAnimation];
    jetpack = [CCSprite spriteWithSpriteFrameName:JETPACK_IDLE_FRAME];
    CCAnimation* anim = [CCAnimation animationWithFrame:JETPACK_BASE_FRAME frameCount:13 delay:0.01f startFrame:0];
    
    CCAnimate* animate = [CCAnimate actionWithAnimation:anim];
    CCRepeatForever* repeat = [CCRepeatForever actionWithAction:animate];
    [jetpack runAction:repeat];
    jetpack.position = dccp(17,-20);
    [self addChild:jetpack z:-100];
}


-(void)setRotationOfGun:(float)rotation
{
    //self.rotationOfGun = rotation;

    //pawsAndGun.rotation = rotation;
    gun.rotation = rotation;
}

-(void)setSpringScale:(float)scale
{
    [gun setSpringScale:scale];
}

-(void)dealloc
{
    [self removeChild:gun cleanup:YES];
    [self removeChild:jetpack cleanup:YES];
    
    NSLog(@"--------- REMOVING REMOVING REMOVING REMOVING REMOVING");
    [super dealloc];
}


@end
