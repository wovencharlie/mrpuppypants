//
//  AppDelegate.h
//  MrPuppyPants
//
//  Created by Charlie Schulze on 12/15/12.
//  Copyright __MyCompanyName__ 2012. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "cocos2d.h"
#import <RevMobAds/RevMobAds.h>
#import "GameData.h"
#import "BaseLevel.h"

@interface AppController : NSObject <UIApplicationDelegate, CCDirectorDelegate, RevMobAdsDelegate>
{
	UIWindow *window_;
	MyRootViewController *navController_;
	CCLayer *mCurrentLayer;
	CCDirectorIOS	*director_;							// weak ref
}

@property (nonatomic, retain) CCLayer *currentLayer;
@property (nonatomic, retain) UIWindow *window;
@property (readonly) UINavigationController *navController;
@property (readonly) CCDirectorIOS *director;

@end
