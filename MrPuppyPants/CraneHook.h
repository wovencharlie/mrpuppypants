//
//  CraneHook.h
//  MrPants
//
//  Created by Charles Schulze on 9/1/12.
//  Copyright 2012 CS54 INC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "Box2D.h"
#import "Constants.h"
#import "ExplodingCat1.h"


@interface CraneHook : CCNode
{
    b2Body* pointedBody;
    b2World* _world;
    ExplodingCat1 *catNumber5;
    CCSprite *craneHook;
    CCSprite *ball;
    CCLayer *parent;
    BOOL ballReleased;
}
-(id) initInParent:(CCLayer *)parentLayer At:(CGPoint)pos inWorld:(b2World *)world;
-(void)releaseBall;
@end
