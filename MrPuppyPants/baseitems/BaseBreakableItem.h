//
//  RopeThrower.h
//  verletRopeTestProject
//
//  Created by Muhammed Ali Khan on 8/19/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "Box2D.h"
#import "CCActionTween.h"
#import "SimpleAudioEngine.h"
#import "Constants.h"

//@class BaseBreakableItem;

/*
@protocol BaseBreakableItemDelegate <NSObject>
- (void)bombExplodedAtPosition:(CGPoint)pos;
@end
 */

@interface BaseBreakableItem : CCSprite {
    
    b2Body* pointedBody;
    b2World* _world;
    CCParticleSystemQuad *emitter;
    BOOL killed;
    BOOL setForRemoval;
    float hitForceToBreak_;
    float hitForceToChangeTexture_;
    float hitForceThreashhold_;
    CCSprite * changeSprite;
    CCSprite *scoreSprite;
    
    int scoreAmount;
}
-(id) initInParent:(CCLayer *)parentLayer At:(CGPoint)pos rotateTo:(float)rotation inWorld:(b2World *)world;
-(void) createItem:(CGPoint)pos rotateTo:(float)rotation;
-(void) changeTexture;
-(void) remove;
-(void) removeAndClean;
-(void) applyBulletForce:(b2Vec2)force;
-(void) getPointingReference:(b2Body *)pointing_body;
-(void) kill;
-(void) hitWithForce:(float)force fromBullet:(BOOL)fromBullet;
-(void) showHurtTexture;
-(void) showExplodeTexture;
//@property (nonatomic, assign) id  <BaseBreakableItemDelegate> delegate;

@end
