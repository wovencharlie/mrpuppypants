//
//  HelloWorldLayer.h
//  MrPants
//
//  Created by Charles Schulze on 6/30/12.
//  Copyright CS54 INC 2012. All rights reserved.
//


// When you import this file, you import all the cocos2d classes
#import "cocos2d.h"
#import "Box2D.h"
#import "GLES-Render.h"
//#import "GroundPlane.h"
#import "GB2ShapeCache.h"
#import "MyContactListener.h"
#import "MrPants.h"
#import "Bullet.h"
#import "BreakableBlock.h"
#import "ExplodingCat1.h"
//#import "SnoozeMeter.h"
#import "ExplodingCat2.h"
#import "SimpleAudioEngine.h"
#import "CTUtility.h"
#import "CCAnimationHelper.h"
#import "CCActionTween.h"
#import "CCRadioMenu.h"
#import "GameData.h"
#import "BallsMenu.h"
#import "BreakableTrashCan.h"

#import "LevelObjects.h"
#import "Plank01.h"
#import "CraneHook.h"
#import "HUDLayer.h"


#import "MenuBetweenLevels.h"
#import "MenuSelectLevel.h"

#import "CCShake.h"
#import "NonBreakableCircle.h"
#import "MenuPauseResume.h"

//Defines
#define PLATFORM_GROUND @"platformGround"
#define PLATFORM_MID @"platformMid"
#define PLATFORM_TOP @"platformTop"



@interface BaseLevel : CCLayer <BallsMenuDelegate, ExplodingCat1Delegate>
{
    
    BOOL shouldPlayBounceAudio;
    BOOL warpHit;
    BOOL warp02Hit;
    BOOL warpUsed;
    
    CCSprite *levelFrontBackground;
    
	b2World* world;
	GLESDebugDraw *m_debugDraw;
    
    int screenWidth;
    int screenHeight;
    NSString *levelImage;
    b2BodyDef frontPlaneBodyDef;

    MyContactListener *_contactListener;
    MrPants *mrPants;

    float gunRoation;
    CGPoint startPosGun;
    Boolean shouldFire;
    float powerMeterPower;
    
    Bullet *currentBullet;
    Boolean touchIsDown;
    
    Boolean throwInProgress;
    
    CCSprite *bg;
    
    CCSprite *mid;
    CCSprite *bgDay;
    
    
    CCSprite *sun;
    CGPoint positionInSpring;
    
    CCParticleSystemQuad *emitter;
    CCParticleSystemQuad *bulletEmitter;
    CCParticleSystemQuad *blockBreakEmitter;
    BOOL allowedToShoot;
    
    BOOL gameWon;
    
    CCSprite *crossHairs;

    CCMenuItem *orangeBallButton;
    CCMenuItem *blackBallButton;
    CCMenuItemImage *fireButton;
    
    CGPoint currentCrosshairsLocation;
        
    NSMutableArray *bulletsArray;
    
    BOOL nextShotReady;
    
    b2Vec2 currentVelocity;
    NSString *currentBallImage;
    
    b2Vec2 lastPathForce;
    b2Vec2 currentPathForce;
    
    //BOOL animateToNextPoint;
    
    float currentPosXOfPath;
    float currentPosYOfPath;
    
    
    CCMenuItemImage *nextLevelButton;
    CCMenuItemImage *tryAgainButton;
    CCMenuItemImage *resetLevelButton;
    
    
    int currentBallNumber;
    int totalBallsInThisLevel;
    int pointTotalThisRound;
    int pointsToPassLevel;
    int numberOfCatsThisLevel;
    int numberOfCatsKilledThisLevel;
    
    LevelObjects *levelObjects;
    
    
    CCSprite *press_here;
    CCSprite *press_to_fire;
    CCSprite *change_ball;
    
    CraneHook *hook;
    CCSprite *craneHookButton;
    
    HUDLayer *hudLayer;
    
    int moneyScoreAnimation;
    BOOL hasWonLevel;
    
    CGFloat touchLeftSideStartPosY;
    CGFloat touchLeftSideCurrentPosY;
    BOOL leftSideTouchIsDown;
    BOOL hasTakenAim;
    
    CCSprite *ropeImage;
    
    
    //rope stuff
    CGPoint start_, end_;
	CGPoint *movePtRef_;
	//	curve parameters are used to create cubic/quad curves
	//FRCurve *frcurve_;
    
    
    //Not being used but might use later 9-15-12
    CGPoint startTouchLocation;
    CGPoint duringTouchLocation;
    
    CGFloat yAdjust;
    CGFloat xAdjust;
    
    BOOL ballsDestroyed;
    //End mibhg use block
    
    
    BallsMenu *ballsMenu;
    int currentBallTick;
    
    BOOL shouldAnimateSpringScale;
    
    CCSprite *rotatingArrowWarp;
    CCSprite *rotatingArrowWarp2;
    
    BOOL shouldRotateWarp1;
    BOOL shouldRotateWarp2;
    BOOL isFirstLevel;
    
    CCSprite *sliderBG;
    CCSprite *sliderThumb;
    
    BreakableBlock *movingPlatform;
    
    float movingPlatformY;
    float moveAmount;
    
    CCSprite *slidePaw;
    CCSprite *easierShot;
    
    BOOL canPlayBounceSound;
    
    BOOL movingArrowWarp;
    BOOL movingArrowWarpX;

    float platformStartY;
    float platformStartX;
    
    NSString *currentPlatformName;
    BOOL canChangePlatform;
    
    CCMenuItemImage *upButton;
    CCMenuItemImage *downButton;
    
    int currentSection;
    //IMAGE_BACK_ARROW_BUTTON
    //CCSprite *testLocationOfCrosshairs;
    
    int coinsThisLevel;
    
    BOOL timerRunning;
    NSTimeInterval startTime;
    NSTimeInterval resumeTime;
    
    NSTimeInterval previousElapsed;
    
    BOOL triedToWinLevel;
    
    BOOL hasPlayedLoseSound;
}

@property (nonatomic,readwrite,assign) float bulletPower;
@property (nonatomic,readwrite,assign) float currentDensity;
@property (nonatomic,readwrite,assign) float bulletRestitution;
@property (nonatomic,readwrite,assign) float bulletFriction;
@property (nonatomic,readwrite,assign) int drawAmount;
@property (nonatomic, assign) BOOL isGamePaused;
@property (nonatomic,readwrite,assign) int altDrawSteps;
@property (nonatomic,readwrite,assign) int drawDotAfterSteps;
@property (nonatomic,retain) CCActionTween *myXTween;
@property (nonatomic,retain) CCActionTween *myYTween;

-(void)pauseGame;
-(void)resumeLevel;
-(void)rotateGun;
-(void)moveMrPants;
-(void)buildLevel;
-(void)createLevels;
+(BaseLevel*) sharedLevel;
-(void)youWonOrLost;
-(void)animateDrawPathState;
-(void)crossHairsSetPosition:(CGPoint)atPoint fade:(BOOL)shouldFade;
-(b2Vec2) getTrajectoryPoint:(b2Vec2) startingPosition andStartVelocity:(b2Vec2) startingVelocity andSteps: (float)n;
-(void) drawPathForCurrentTurretState:(b2Vec2)startVelocity;

- (void)tryAgainItemTapped:(id)sender;
-(void)moveCrosshairs;
-(void)actionResetLevel;
-(void)actionNextLevel;
-(BOOL)adjustBallsMenuCountsShouldFire;

-(void)createOutOfBallsMenu;
// returns a CCScene that contains the Layer as the only child
+(CCScene *) scene;
-(void)destroyAllBullets;
-(void)createMenu;
-(void)fireBullet;
-(void)dotSetup;
-(void)crossHairsSetHidden;
-(void)createDeathFloor;
// adds a new sprite at a given coordinate
-(void)initLevelItemsAndGraphics;
-(void)addGBSpriteWithCoords:(CGPoint)p;
-(void)duringTick: (ccTime) dt;
-(void)setAllowedToShoot:(ccTime) dt;
-(void)youWinLevel:(ccTime)dt;
-(void)createBullet:(CGPoint)p force:(b2Vec2)force;
-(BOOL) checkCircleCollision:(CGPoint)center1  :(float)radius1  :(CGPoint) center2  :(float) radius2;
- (GLfloat) calculateAngle:(GLfloat)x1 :(GLfloat)y1 :(GLfloat)x2 :(GLfloat)y2;

-(void)somethingKilledWasCat:(BOOL)wasCat withPoints:(int)points;

- (void)menuButtonTapped:(id)sender;
-(void)setNextShotReady:(ccTime)dt;

-(void)addPoints:(int)numberOfPoints;
-(void)hitSprites:(CCSprite *)spriteA hitSpriteB:(CCSprite *)spriteB withContact:(MyContact) contact;
-(void)hideMenuItems;
-(void)animateSpringScale;
-(void)followBallTick: (ccTime) dt;

-(void)createLevel1;
-(void)createLevel2;
/*
-(void)createLevel2;
-(void)createLevel3;
-(void)createLevel4;
-(void)createLevel5;
-(void)createLevel6;
-(void)createLevel7;
-(void)createLevel8;
-(void)createLevel9;
-(void)createLevel10;
-(void)createLevel11;
-(void)createLevel12;
//-(void)createLevel13;
//-(void)createLevel14;
 */
@end
