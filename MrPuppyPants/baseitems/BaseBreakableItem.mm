//
//  RopeThrower.m
//  verletRopeTestProject
//
//  Created by Muhammed Ali Khan on 8/19/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "BaseBreakableItem.h"
#import "BaseLevel.h"

@implementation BaseBreakableItem

//@synthesize delegate;

- (id) initInParent:(CCLayer *)parentLayer At:(CGPoint)pos rotateTo:(float)rotation inWorld:(b2World *)world{
	if((self = [super init]))
    {
        _world = world;
        
	}
    
    
    
    
    //[self createItem:pos];
    //[self performSelector:@selector(remove) withObject:nil afterDelay:3];
	return self;
}

-(void) createItem:(CGPoint)pos rotateTo:(float)rotation
{
    killed = NO;
    setForRemoval = NO;
    hitForceToBreak_ = 8.0f;
    hitForceToChangeTexture_ = 1.0f;
    hitForceThreashhold_ = .5;
}
-(void)applyBulletForce:(b2Vec2)force
{
    pointedBody->SetLinearVelocity(force);
}

-(void)hitWithForce:(float)force fromBullet:(BOOL)fromBullet
{
    //Global *global = [Global instance];
    
    if(force > hitForceThreashhold_){
        hitForceToBreak_ = (hitForceToBreak_ - force);
    }

    if(fromBullet && hitForceToBreak_ > 0)
    {
        // global.score += (force * 150);
    }
    
    if(hitForceToBreak_ < hitForceToChangeTexture_ && hitForceToBreak_ > 0)
    {
        [self showHurtTexture];
    }
    if(hitForceToBreak_ < 0 && !setForRemoval)
    {
        setForRemoval = YES;
        //global.score += 1000;
        [self showExplodeTexture];
        //[self remove];
        
        [self performSelector:@selector(remove) withObject:nil afterDelay:0.4];
        
    }
}

-(void) showExplodeTexture
{
    
}

-(void) showHurtTexture
{
    
}

-(void) openWarp
{
    
}


-(void)remove
{
    [self openWarp];
    if(!killed)
    {
        b2Body *body = pointedBody;
        
        
        //b2Filter filter = pointedBody->GetFixtureList()->GetFilterData();
        //filter.groupIndex = COLLIDE_GROUP_BULLET;
        
        
        if (body->GetUserData() != NULL)
        {
            CCSprite *sprite = (CCSprite *) body->GetUserData();

            
            if(scoreAmount == 1000)
            {
                scoreSprite = [CCSprite spriteWithSpriteFrameName:IMAGE_1000];
                [[BaseLevel sharedLevel] somethingKilledWasCat:NO withPoints:1000];
            }
            else if(scoreAmount == 5000)
            {
                scoreSprite = [CCSprite spriteWithSpriteFrameName:IMAGE_5000];
                [[BaseLevel sharedLevel] somethingKilledWasCat:YES withPoints:5000];
            }
            else
            {
                scoreSprite = [CCSprite spriteWithSpriteFrameName:IMAGE_1000];
                [[BaseLevel sharedLevel] somethingKilledWasCat:NO withPoints:1000];
            }
            scoreSprite.scale = 0;
            scoreSprite.position = sprite.position;
            [self addChild:scoreSprite];
            id zoomScore = [CCSequence actions:[CCScaleTo actionWithDuration:.2 scale:1.0],[CCScaleTo actionWithDuration:.2 scale:1],[CCScaleTo actionWithDuration:.1 scale:0], nil];
            
            [scoreSprite runAction:[CCSequence actions:zoomScore, nil]];
         
            
            //emitter = [[CCParticleSystemQuad alloc]initWithFile:@"blockBreak2.plist"];
            //emitter.position = sprite.position;
            //[self addChild:emitter];
            [self removeChild:sprite cleanup:YES];
        }
        killed = YES;
        _world->DestroyBody(body);
        //[self removeAndClean];
        
        [self performSelector:@selector(removeAndClean) withObject:nil afterDelay:0.6];
        
    }
}

-(void)createScoreSprite
{
    
}


-(void)removeAndClean
{
    //[emitter release];
    [self removeFromParentAndCleanup:true];
    killed = YES;
}

-(void) getPointingReference:(b2Body *)pointing_body
{
    pointedBody = pointing_body;
}

-(void) kill
{
    if(!killed)
    {
        b2Body *body = pointedBody;
        CCSprite *sprite = (CCSprite *) body->GetUserData();
        [self removeChild:sprite cleanup:YES];
        _world->DestroyBody(body);
        [self removeAndClean];
    }
}
-(void)changeTexture
{
    
}

- (void) dealloc
{
    [super dealloc];
}
@end


