//
//  RopeThrower.h
//  verletRopeTestProject
//
//  Created by Muhammed Ali Khan on 8/19/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "Box2D.h"
#import "Constants.h"
@interface BaseBullet : CCNode {
    
    b2Body* pointedBody;
    b2World* _world;
    CCParticleSystemQuad *emitter;
    BOOL killed;
}
-(BOOL) isKilled;
-(void) createBullet:(CGPoint)pos;
-(void) remove;
-(void) removeAndClean;
-(void) updatePointer;
-(void) applyBulletForce:(b2Vec2)force;
-(void) getPointingReference:(b2Body *)pointing_body;
-(void) kill;
-(void) removeAllVelocity;
-(b2Body *)getPointedBody;
-(id) initInParent:(CCLayer *)parentLayer At:(CGPoint)pos inWorld:(b2World *)world;
@end

