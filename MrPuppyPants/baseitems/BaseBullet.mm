//
//  RopeThrower.m
//  verletRopeTestProject
//
//  Created by Muhammed Ali Khan on 8/19/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "BaseBullet.h"


@implementation BaseBullet

- (id) initInParent:(CCLayer *)parentLayer At:(CGPoint)pos inWorld:(b2World *)world{
	if((self = [super init])) 
    {
        _world = world;
	}
    
    killed = NO;
    
    //[self createBullet:pos];
    
	return self;
}

-(BOOL) isKilled{
    return killed;
}

-(void) createBullet:(CGPoint)pos
{    
    [self performSelector:@selector(remove) withObject:nil afterDelay:28];
}

-(void)applyBulletForce:(b2Vec2)force
{
    pointedBody->SetLinearVelocity(force);
}

-(void)remove
{
    if(!killed)
    {
        b2Body *body = pointedBody; 
        
        if (body->GetUserData() != NULL) {
            CCSprite *sprite = (CCSprite *) body->GetUserData();
            
#if ENABLE_EMITTERS
            emitter = [[CCParticleSystemQuad alloc]initWithFile:@"MagicFire.plist"];
            emitter.position = sprite.position;
            [self addChild:emitter];
#endif
            [self removeChild:sprite cleanup:YES];
            
            _world->DestroyBody(body);
            [self performSelector:@selector(removeAndClean) withObject:nil afterDelay:1.5];
            
            
            
        }
        
        killed = YES;
    }
}

-(void)removeAndClean
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
#if ENABLE_EMITTERS
    [emitter release];
#endif
    [self removeFromParentAndCleanup:true];
    killed = YES;
}

-(b2Body *)getPointedBody
{
    return pointedBody;
}
-(void) getPointingReference:(b2Body *)pointing_body
{
    pointedBody = pointing_body;
}
-(void) removeAllVelocity{
    if(!killed)
    {
        pointedBody->SetLinearVelocity(b2Vec2(0, 0));
    }
}
-(void) kill
{
    if(!killed)
    {
        b2Body *body = pointedBody; 
        CCSprite *sprite = (CCSprite *) body->GetUserData();
        [self removeChild:sprite cleanup:YES];
        _world->DestroyBody(body);        
        [self removeAndClean];
    }
}


-(void)updatePointer 
{
    
}

- (void) dealloc
{
    [self remove];
    [super dealloc];
}
@end


