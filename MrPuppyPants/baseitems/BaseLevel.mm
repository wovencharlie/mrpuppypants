//
//  BaseLevel.mm
//  MrPants
//
//  Created by Charles Schulze on 6/30/12.
//  Copyright CS54 INC 2012. All rights reserved.
//


// Import the interfaces
#import "BaseLevel.h"
#import "HUDLayer.h"
#import "EndGoodBoy.h"
#import "AppDelegate.h"
#import "TapForTap.h"

//FIX TIME STEP
///////--->>>>>>>>>>>>>
const float32 FIXED_TIMESTEP = 1.0f / 60.0f;

// Minimum remaining time to avoid box2d unstability caused by very small delta times
// if remaining time to simulate is smaller than this, the rest of time will be added to the last step,
// instead of performing one more single step with only the small delta time.
const float32 MINIMUM_TIMESTEP = 1.0f / 600.0f;

const int32 VELOCITY_ITERATIONS = 8;
const int32 POSITION_ITERATIONS = 8;

// maximum number of steps per tick to avoid spiral of death
const int32 MAXIMUM_NUMBER_OF_STEPS = 25;

//Pixel to metres ratio. Box2D uses metres as the unit for measurement.
//This ratio defines how many pixels correspond to 1 Box2D "metre"
//Box2D is optimized for objects of 1x1 metre therefore it makes sense
//to define the ratio so that your most common object type is 1x1 metre.


// enums that will be used as tags
enum {
	kTagTileMap = 1,
	kTagBatchNode = 1,
	kTagAnimation1 = 1,
};


// BaseLevel implementation
@implementation BaseLevel



#pragma mark ContactFilter

class myContactFilter : public b2ContactFilter
{
	// There are more callbacks than this one, see manual
	bool ShouldCollide(b2Fixture* fixtureA, b2Fixture* fixtureB);
};


// add a method to test the collision data for filter settings
bool myContactFilter::ShouldCollide(b2Fixture* fixtureA, b2Fixture* fixtureB)
{
    
	b2Filter filter1 = fixtureA->GetFilterData();
	b2Filter filter2 = fixtureB->GetFilterData();
	// test to let pass or calculate collision results
    
    
    if(filter1.groupIndex == COLLIDE_GROUP_BULLET && filter2.groupIndex == COLLIDE_WARP_HOLE){
        return true;
    }
    if(filter1.groupIndex == COLLIDE_WARP_HOLE && filter2.groupIndex == COLLIDE_GROUP_BULLET){
        return true;
    }
    
    if(filter1.groupIndex == COLLIDE_GROUP_BULLET && filter2.groupIndex == COLLIDE_WARP_HOLE_02){
        return true;
    }
    if(filter1.groupIndex == COLLIDE_WARP_HOLE_02 && filter2.groupIndex == COLLIDE_GROUP_BULLET){
        return true;
    }
    
    //Anything else hitting the warphole should not be effected
    if(filter1.groupIndex == COLLIDE_WARP_HOLE || filter2.groupIndex == COLLIDE_WARP_HOLE || filter1.groupIndex == COLLIDE_WARP_HOLE_02 || filter2.groupIndex == COLLIDE_WARP_HOLE_02)
    {
        return false;
    }

    if(filter1.groupIndex == COLLIDE_DEATH_FLOOR || filter2.groupIndex == COLLIDE_DEATH_FLOOR)
    {
        return true;
    }
    
    if(filter1.groupIndex == COLLIDE_GROUP_PLANK && filter2.groupIndex == COLLIDE_GROUP_PLANK) {
        return true;
    }
    
    if(filter1.groupIndex == COLLIDE_GENERIC_ALWAYS_COLLIDE && filter2.groupIndex == COLLIDE_GENERIC_ALWAYS_COLLIDE)
    {
        return true;
    }
    
    if(filter1.groupIndex == COLLIDE_GENERIC_ALWAYS_COLLIDE && filter2.groupIndex == COLLIDE_GROUP_PLANK)
    {
        return true;
    }
    
	if(filter1.groupIndex == filter2.groupIndex) {
		
        return false;
	} else {
		return true;
	}
}


static BaseLevel* layerInstance;

+(BaseLevel*) sharedLevel
{
	NSAssert(layerInstance != nil, @"table not yet initialized!");
    
	return layerInstance;
}

+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	BaseLevel *layer = [BaseLevel node];
	
    AppController *app = (AppController*) [[UIApplication sharedApplication] delegate];
    app.currentLayer = layer;
    
	// add layer as a child to scene
	[scene addChild: layer];
    
	// return the scene
	return scene;
}
@synthesize bulletPower;
@synthesize currentDensity;
@synthesize drawAmount;
@synthesize altDrawSteps;
@synthesize drawDotAfterSteps;
@synthesize myXTween;
@synthesize myYTween;
@synthesize bulletRestitution;
@synthesize bulletFriction;

- (CGPoint)convertPoint:(CGPoint)point {
    return point;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        return ccp(32 + point.x*2, 64 + point.y*2);
    } else {
        return point;
    }
}


#pragma mark --- init

-(void)createBallsMenu
{
    
    ballsMenu= [[BallsMenu alloc] initInPosition:dccp(180, 280)];
    [self addChild:ballsMenu];
    ballsMenu.delegate = self;
    
    
    [ballsMenu.orangeLabel setString:[levelObjects.ballsArray objectAtIndex:0]];
    [ballsMenu.blackLabel setString:[levelObjects.ballsArray objectAtIndex:1]];
    [ballsMenu.redLabel setString:[levelObjects.ballsArray objectAtIndex:2]];
    [ballsMenu.blueLabel setString:[levelObjects.ballsArray objectAtIndex:3]];
    
    if([ballsMenu.orangeLabel.string intValue] < 1){
        [ballsMenu.orangeBallButton setVisible:NO];
    }
    if([ballsMenu.blackLabel.string intValue] < 1){
        ballsMenu.blackBall.opacity = 40;
        ballsMenu.blackLabel.opacity = 20;
        [ballsMenu.blackBallButton setIsEnabled:NO];
    }
    if([ballsMenu.redLabel.string intValue] < 1){
        ballsMenu.redBall.opacity = 40;
        ballsMenu.redLabel.opacity = 20;
        [ballsMenu.redBallButton setIsEnabled:NO];
    }
    if([ballsMenu.blueLabel.string intValue] < 1){
        ballsMenu.blueBall.opacity = 40;
        ballsMenu.blueLabel.opacity = 20;
        [ballsMenu.blueBallButton setIsEnabled:NO];
    }
    
    [self ball1Selected];
    
    
    //levelObject.ballsArray
}

- (void)ball1Selected
{
    nextShotReady = NO;
    
    self.bulletPower = BULLET_POWER_ORANGE;
    self.currentDensity = BULLET_DENSETY_ORANGE;
    self.bulletRestitution = BULLET_RESTITUTION_ORANGE;
    self.bulletFriction = BULLET_FRICTION_ORANGE;
    currentBallImage = IMAGE_ORANGE_BALL;
    self.drawAmount = BULLET_DRAW_AMOUNT_ORANGE;
    self.altDrawSteps = BULLET_ALT_DRAW_STEPS_ORANGE;
    self.drawDotAfterSteps = BULLET_DRAW_DOT_AFTER_ORANGE;
    
    
    //self.bulletPower *= 1.4;
    //self.currentDensity *= 2;
    //self.bulletRestitution *= 2;
    [mrPants ball1Selected];
    
    if([ballsMenu.orangeLabel.string intValue] > 0)
    {
        nextShotReady = YES;
        [self dotSetup];
    }
    
    
}

- (void)moveToPlatformName:(NSString *)platformName
{
    if([currentPlatformName isEqualToString:platformName] || !canChangePlatform){
        //no change
        return;
    }
    
    
    


    
    canChangePlatform = YES;
    int yTO = 0;
    int yFrom = 0;
    
    int MID_PLATFORM_Y = 108;
    int GROUND_PLATFORM_Y = 30;
    int XTO = 60;
    
    upButton.opacity = 255;
    downButton.opacity = 255;
    
    if([currentPlatformName isEqualToString:PLATFORM_GROUND]){
        if([platformName isEqualToString:PLATFORM_MID]){
            yTO = MID_PLATFORM_Y;
            yFrom = GROUND_PLATFORM_Y;
            XTO = 50;
            upButton.opacity = 70;
        }
    }else if([currentPlatformName isEqualToString:PLATFORM_MID]){
        if([platformName isEqualToString:PLATFORM_GROUND]){
            yTO = GROUND_PLATFORM_Y;
            yFrom = MID_PLATFORM_Y;
            downButton.opacity = 70;
        }
    }else if([currentPlatformName isEqualToString:PLATFORM_TOP]){
        
    }
    
    [mrPants startWalkAnimation];
    
    currentPlatformName = platformName;
    
    id flipLeft = [CCSequence actions:[CCScaleTo actionWithDuration:0 scaleX:-.85 scaleY:.85], nil];
    
    id moveLeft = [CCSequence actions:[CCMoveTo actionWithDuration:.6 position:dccp(-20,yFrom)], nil];
    id easeActionLeft = [CCEaseSineIn actionWithAction:moveLeft];
    
    id moveUp = [CCSequence actions:[CCMoveTo actionWithDuration:0 position:dccp(-20,yTO)],[CCScaleTo actionWithDuration:0 scaleX:.85 scaleY:.85], nil];
    
    id moveRight = [CCSequence actions:[CCMoveTo actionWithDuration:.6 position:dccp(XTO,yTO)], nil];
    id easeActionRight = [CCEaseSineOut actionWithAction:moveRight];
    
    [mrPants runAction:[CCSequence actions:flipLeft, easeActionLeft, moveUp, easeActionRight,
                        [CCCallFunc actionWithTarget:mrPants selector:@selector(stopWalkAnimation)],
                        nil]];
    
    //
    
}

- (void)ball2Selected
{
    nextShotReady = NO;
    //[self moveToPlatformName:PLATFORM_MID];
    
    self.bulletPower = BULLET_POWER_BLACK;
    self.currentDensity = BULLET_DENSETY_BLACK;
    self.drawAmount = BULLET_DRAW_AMOUNT_BLACK;
    self.altDrawSteps = BULLET_ALT_DRAW_STEPS_BLACK;
    self.drawDotAfterSteps = BULLET_DRAW_DOT_AFTER_BLACK;
    currentBallImage = IMAGE_BLACK_BALL;
    self.bulletRestitution = BULLET_RESTITUTION_BLACK;
    self.bulletFriction = BULLET_FRICTION_BLACK;
    
    [mrPants ball2Selected];

    
    if([ballsMenu.blackLabel.string intValue] > 0)
    {
        nextShotReady = YES;
        [self dotSetup];
    }
    
    
}
- (void)ball3Selected
{
    nextShotReady = NO;
    
    //[self moveToPlatformName:PLATFORM_GROUND];
    
    self.bulletPower = BULLET_POWER_RED;
    self.currentDensity = BULLET_DENSETY_RED;
    self.drawAmount = BULLET_DRAW_AMOUNT_RED;
    self.altDrawSteps = BULLET_ALT_DRAW_STEPS_RED;
    self.drawDotAfterSteps = BULLET_DRAW_DOT_AFTER_RED;
    currentBallImage = IMAGE_RED_BALL;
    self.bulletRestitution = BULLET_RESTITUTION_RED;
    self.bulletFriction = BULLET_FRICTION_RED;

    [mrPants ball3Selected];
    
    if([ballsMenu.redLabel.string intValue] > 0)
    {
        nextShotReady = YES;
        [self dotSetup];
    }
}
- (void)ball4Selected
{
    nextShotReady = NO;
    
    self.bulletPower = BULLET_POWER_BLUE;
    self.currentDensity = BULLET_DENSETY_BLUE;
    self.drawAmount = BULLET_DRAW_AMOUNT_BLUE;
    self.altDrawSteps = BULLET_ALT_DRAW_STEPS_BLUE;
    self.drawDotAfterSteps = BULLET_DRAW_DOT_AFTER_BLUE;
    currentBallImage = IMAGE_BLUE_BALL;
    self.bulletRestitution = BULLET_RESTITUTION_BLUE;
    self.bulletFriction = BULLET_FRICTION_BLUE;

    [mrPants ball4Selected];
    
    if([ballsMenu.blueLabel.string intValue] > 0)
    {
        nextShotReady = YES;
        [self dotSetup];
    }
}

-(void)resumeBackgroundMusic
{
    [[SimpleAudioEngine sharedEngine] resumeBackgroundMusic];
}

-(void)createOutOfBallsMenu
{
    
    [self unschedule:@selector(createOutOfBallsMenu:)];
    BOOL bulletFound = NO;
    for (Bullet *bullet in bulletsArray){
        if(bullet){
            if(!bullet.isKilled){
                bulletFound = YES;
            }
            
        }
    }
    
    if(bulletFound){
        [self performSelector:@selector(createOutOfBallsMenu) withObject:nil afterDelay:4.0f];
        return;
    }
    
    
    if(hasWonLevel){
        return;
    }
    CCSpriteFrameCache *shared = [CCSpriteFrameCache sharedSpriteFrameCache];
    
    tryAgainButton = [CCMenuItemImage
                      itemWithNormalSprite:[CCSprite spriteWithSpriteFrame:[shared spriteFrameByName:IMAGE_TRY_AGAIN]]
                      selectedSprite:[CCSprite spriteWithSpriteFrame:[shared spriteFrameByName:IMAGE_TRY_AGAIN]]
                      target:self selector:@selector(tryAgainItemTapped:)];

    CCMenu *menu = [CCMenu menuWithItems:tryAgainButton, nil];
    
    CGSize screenSize = [CCDirector sharedDirector].winSize;
    
    
    menu.position = ccp(screenSize.width/2, screenSize.height/2);
    [menu alignItemsVertically];
    [self addChild:menu z:99000];
    
    if(ENABLE_MUSIC && ![[GameData sharedData]areSoundFXMuted] && !hasPlayedLoseSound){
        hasPlayedLoseSound = YES;
        [[SimpleAudioEngine sharedEngine] playEffect:AUDIO_LOSE pitch:1.0 pan:0.0 gain:0.4];
        [[SimpleAudioEngine sharedEngine] pauseBackgroundMusic];
        [self performSelector:@selector(resumeBackgroundMusic) withObject:nil afterDelay:3.5];
    }
    
    //[self runAction:[CCShake actionWithDuration:.45f amplitude:ccp(12,8) shakes:12]];
}
-(void)createMenu{
    if(self.isGamePaused){
        triedToWinLevel = YES;
        return;
    }
    [self destroyAllBullets];
    CCTransitionSplitRows *transition=[CCTransitionSplitRows transitionWithDuration:.7 scene:[MenuBetweenLevels scene]];
    if(ENABLE_MUSIC && ![[GameData sharedData]areSoundFXMuted]){
        [[SimpleAudioEngine sharedEngine] playEffect:AUDIO_WIN];
    }
    [[CCDirector sharedDirector] replaceScene:transition];
}

-(void)goodBoy{
    [self destroyAllBullets];
    CCTransitionSplitRows *transition=[CCTransitionSplitRows transitionWithDuration:.7 scene:[EndGoodBoy scene]];
    if(ENABLE_MUSIC && ![[GameData sharedData]areSoundFXMuted]){
        [[SimpleAudioEngine sharedEngine] playEffect:AUDIO_WIN];
    }
    [[CCDirector sharedDirector] replaceScene:transition];
}


-(void)actionResetLevel{
    
    [self unschedule:@selector(timerAfterThrow:)];
    [self unschedule:@selector(followBallTick:)];
    
    [self destroyAllBullets];
    
    CCTransitionSplitRows *transition=[CCTransitionSplitRows transitionWithDuration:.7 scene:[BaseLevel scene]];
    [[CCDirector sharedDirector] replaceScene:transition];
}
-(void)actionNextLevel{
    [self destroyAllBullets];
    //[[GameData sharedData] levelUp];
    CCTransitionFlipX *transition=[CCTransitionFlipX transitionWithDuration:1 scene:[BaseLevel scene]];
    [[CCDirector sharedDirector] replaceScene:transition];
}


- (void)nextLevelItemTapped:(id)sender{
    [self actionNextLevel];
}

- (void)tryAgainItemTapped:(id)sender{
    [self actionResetLevel];
}

-(void)addPoints:(int)numberOfPoints{
    pointTotalThisRound += numberOfPoints;
    if (numberOfCatsKilledThisLevel >= numberOfCatsThisLevel)
    {
        [self performSelector:@selector(youWonOrLost) withObject:nil afterDelay:2.0f];
        [self stopLevelTimer];
        nextShotReady = NO;
        return;
    }
    if(currentBallNumber == totalBallsInThisLevel)
    {
        [self hideMenuItems];
        [self performSelector:@selector(createOutOfBallsMenu) withObject:nil afterDelay:4.0f];
        nextShotReady = NO;
    }
}

- (void)menuButtonTapped:(id)sender{
    [self destroyAllBullets];
    
    CCTransitionSplitRows *transition=[CCTransitionSplitRows transitionWithDuration:.7 scene:[MenuSelectLevel scene]];
    [[CCDirector sharedDirector] replaceScene:transition];
    
    int currentLevel = [[GameData sharedData] returnLevel];
    NSMutableString* menuSelected = [NSMutableString stringWithFormat:@"Level : %d :Menu Button Tapped",currentLevel];
    [Flurry logEvent:menuSelected];
}


- (void)pauseGame{
    if(!self.isGamePaused){
        [self pauseSchedulerAndActions];
        self.isGamePaused = YES;
        [self pauseLevelTimer];
        [self pauseAllBullets];
        [self addChild:[MenuPauseResume scene] z:9999999];
        
        
        if([[GameData sharedData] shouldShowAd]){
            AppController *app = (AppController*) [[UIApplication sharedApplication] delegate];
            //[TapForTapAppWall showWithRootViewController:[app navController]];
            
            [TapForTapInterstitial showWithRootViewController:[app navController]];
        }
    }
}
- (void)pauseButtonTapped:(id)sender{
    [self pauseGame];
}

-(void)resumeLevel{
    if(self.isGamePaused){
        self.isGamePaused = NO;
        [self resumeLevelTimer];
        [self resumeSchedulerAndActions];
        [self resumeAllBullets];
        if(triedToWinLevel){
            [self createMenu];
        }
    }
}

-(id) init{
	if( (self=[super init])) {
        
        warpHit = NO;
        warp02Hit = NO;
        warpUsed = NO;
        
        CCSpriteFrameCache* frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
		[frameCache addSpriteFramesWithFile:MR_PANTS_PLIST];
        [frameCache addSpriteFramesWithFile:MR_PANTS_WALK_PLIST];
        [frameCache addSpriteFramesWithFile:JETPACK_PLIST];
        [frameCache addSpriteFramesWithFile:GUN_EXPLOSION_PLIST];
        
        [frameCache addSpriteFramesWithFile:CAT_ANIMATION_PLIST];
        [frameCache addSpriteFramesWithFile:CAT_2_ANIMATION_PLIST];
        [frameCache addSpriteFramesWithFile:CAT_3_ANIMATION_PLIST];
        [frameCache addSpriteFramesWithFile:WARP_2_PLIST];
        
        [[GB2ShapeCache sharedShapeCache] addShapesWithFile:@"levels_physEdit_v1.plist"];

        CGSize screenSize = [CCDirector sharedDirector].winSize;
        
        hasWonLevel = NO;
        layerInstance = self;
        ballsDestroyed = NO;
        
        CCSprite *resetImage = [CCSprite spriteWithSpriteFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:IMAGE_RESET_BUTTON] ];
        CCSprite *resetImageSelected = [CCSprite spriteWithSpriteFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:IMAGE_RESET_BUTTON] ];
        CCSprite *menuImage = [CCSprite spriteWithSpriteFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:IMAGE_MENU_BUTTON] ];
        CCSprite *pauseImage = [CCSprite spriteWithSpriteFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:IMAGE_MENU_PAUSE_BUTTON] ];
        CCSprite *pauseImageSelected = [CCSprite spriteWithSpriteFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:IMAGE_MENU_PAUSE_BUTTON] ];
        
        CCSprite *menuImageSelected = [CCSprite spriteWithSpriteFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:IMAGE_MENU_BUTTON] ];
        
                
        resetLevelButton = [CCMenuItemImage itemWithNormalSprite:resetImage selectedSprite:resetImageSelected target:self selector:@selector(tryAgainItemTapped:)];
                                                        
        CCMenuItem *menuButton = [CCMenuItemImage itemWithNormalSprite:menuImage selectedSprite:menuImageSelected target:self selector:@selector(menuButtonTapped:)];
        CCMenuItem *pauseButton = [CCMenuItemImage itemWithNormalSprite:pauseImage selectedSprite:pauseImageSelected target:self selector:@selector(pauseButtonTapped:)];

        CCMenu *menu = [CCMenu menuWithItems:pauseButton, nil];
        int yPos = 280;
        int hudXMod = 80;
        if(IS_IPAD){
            yPos = 670 / 2;
            hudXMod = 100;
        }
        else if(screenSize.width > 480){
            hudXMod = 160;
        }

        int menuPosition = 364 + hudXMod;
        menu.position = dccp(menuPosition, yPos);
        [menu alignItemsHorizontally];
        [self addChild:menu];
        
        
        
        currentBallImage = IMAGE_ORANGE_BALL;
        self.bulletPower = BULLET_POWER_ORANGE;
        self.currentDensity = BULLET_DENSETY_ORANGE;
        self.bulletRestitution = BULLET_RESTITUTION_ORANGE;
        self.bulletFriction = BULLET_FRICTION_ORANGE;
        currentBallImage = IMAGE_ORANGE_BALL;
        
        self.drawAmount = BULLET_DRAW_AMOUNT_ORANGE;
        self.altDrawSteps = BULLET_ALT_DRAW_STEPS_ORANGE;
        self.drawDotAfterSteps = BULLET_ALT_DRAW_STEPS_ORANGE;
        
        bulletsArray = [[NSMutableArray alloc] init];
        
		// enable touches
		self.isTouchEnabled = YES;
        
        
        if(ENABLE_AUDIO && ![[GameData sharedData]areAmbientFXMuted]){
            [[SimpleAudioEngine sharedEngine] preloadEffect:AUDIO_CAT_MEOW_01];
        }
        
		// enable accelerometer
		self.isAccelerometerEnabled = YES;
		
		
        
		CCLOG(@"Screen width %0.2f screen height %0.2f",screenSize.width,screenSize.height);
        
        screenWidth = (int) screenSize.width;
        screenHeight = (int) screenSize.height;
		// Define the gravity vector.
		b2Vec2 gravity;
        if(IS_IPAD){
            gravity.Set(0.0f, -20.0f);
        }else
        {
            gravity.Set(0.0f, -10.0f);
        }

		// Construct a world object, which will hold and simulate the rigid bodies.
		world = new b2World(gravity);
        world->SetAllowSleeping(true);
		
		world->SetContinuousPhysics(true);
        world->SetContactFilter(new myContactFilter);
		_contactListener = new MyContactListener();
        world->SetContactListener(_contactListener);
        
		// Debug Draw functions
		m_debugDraw = new GLESDebugDraw( PTM_RATIO );
		world->SetDebugDraw(m_debugDraw);
		
		uint32 flags = 0;
        
       
        
		//flags += b2DebugDraw::e_shapeBit;
        //		flags += b2DebugDraw::e_jointBit;
        //		flags += b2DebugDraw::e_aabbBit;
        //		flags += b2DebugDraw::e_pairBit;
        //		flags += b2DebugDraw::e_centerOfMassBit;
		//m_debugDraw->SetFlags(flags);
		
        
        throwInProgress = NO;
        //-------------------------------------------
        //-------------------------------------------
        //-------------------------------------------
        shouldAnimateSpringScale = YES;
        
		//Set up sprite
        
        
		CCLabelTTF *label = [CCLabelTTF labelWithString:@"Tap screen" fontName:@"Marker Felt" fontSize:32];
		//[self addChild:label z:0];
		[label setColor:ccc3(0,0,255)];
		label.position = dccp( screenSize.width/2, screenSize.height-50);
		
		[self schedule: @selector(tick:)];
        
        [self initLevelItemsAndGraphics];
        
        self.anchorPoint = ccp(0,0);

        allowedToShoot = YES;
        gameWon = NO;

        [self unschedule:@selector(resetGameReset:)];
        [self schedule: @selector(resetGameReset:) interval:1.0];
        
        hudLayer = [HUDLayer node];
        [self addChild:hudLayer];

        [self createDeathFloor];
        [self buildLevel];
        [self createBallsMenu];
        
                
        int timeToLevelComplete = 60;
        
        id moveUp = [CCSequence actions:[CCMoveTo actionWithDuration:timeToLevelComplete position:ccp(280,180)], nil];
        [sun runAction:[CCSequence actions:moveUp, nil]];
        
        
        movingPlatformY = 0;
        moveAmount = .3;
        
        if(IS_IPAD){
            moveAmount = .6;
        }
        
        //create up / down buttons
        upButton = [CCMenuItemImage itemWithNormalSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_BACK_ARROW_BUTTON]]
                                                             selectedSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_BACK_ARROW_BUTTON]]
                                                                     target:self selector:@selector(upButtonTapped:)];
        
        downButton = [CCMenuItemImage itemWithNormalSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_BACK_ARROW_BUTTON]]
                                                           selectedSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_BACK_ARROW_BUTTON]]
                                                                   target:self selector:@selector(downButtonTapped:)];
        
        upButton.opacity = 255;
        downButton.opacity = 70;
        
        upButton.rotation = 90;
        downButton.rotation = -90;
        upButton.scale = .65;
        downButton.scale = .65;
        
        CCMenu *backButtonMenu = [CCMenu menuWithItems:upButton,downButton, nil];
        backButtonMenu.position = dccp(35, 180);
        [backButtonMenu alignItemsVertically];
        [self addChild:backButtonMenu];
        
        //[mrPants startJetpackAnimation];

	}
	return self;
}

- (void)upButtonTapped:(id)sender{
    
    if(easierShot){
        [self removeChild:easierShot cleanup:YES];
        easierShot = nil;
    }
    
    if(!mrPants.hasStoppedWalking){
        
        return;
    }
    
    [self moveToPlatformName:PLATFORM_MID];
}

- (void)downButtonTapped:(id)sender{
    
    if(!mrPants.hasStoppedWalking){
        
        return;
    }
    
    [self moveToPlatformName:PLATFORM_GROUND];
}

-(void)startLevelTimer{
    timerRunning = YES;
    startTime = [NSDate timeIntervalSinceReferenceDate];
    [self updateTime];
}

-(void)pauseLevelTimer{
    timerRunning = NO;
}

-(void)resumeLevelTimer{
    timerRunning = YES;
    startTime = [NSDate timeIntervalSinceReferenceDate] - previousElapsed;
    [self updateTime];
}
-(void)stopLevelTimer{
    if(timerRunning == NO)return;
    
    timerRunning = NO;
    
    NSTimeInterval elapsed = previousElapsed;
    
    NSNumberFormatter *fmt = [[NSNumberFormatter alloc] init];
    [fmt setPositiveFormat:@"0.#"];
    NSString *timeString = [fmt stringFromNumber:[NSNumber numberWithFloat:elapsed]];
    double timeFormattedFromString = [timeString doubleValue];
    
    [fmt release];
    fmt = nil;
    
    [hudLayer.timeToCompleteLevel setString:[[GameData sharedData] formattedTimeForLevel:timeFormattedFromString]];
    [[GameData sharedData] setBestTimeForLevel:timeFormattedFromString];
}

-(void)updateTime
{
    if(timerRunning == NO)return;
    NSTimeInterval currentTime = [NSDate timeIntervalSinceReferenceDate];
    NSTimeInterval elapsed = (currentTime - startTime) + resumeTime;
    
    
    NSNumberFormatter *fmt = [[NSNumberFormatter alloc] init];
    [fmt setPositiveFormat:@"0.#"];
    NSString *timeString = [fmt stringFromNumber:[NSNumber numberWithFloat:elapsed]];
    double timeFormattedFromString = [timeString doubleValue];
    [fmt release];
    fmt = nil;
    
    
    [hudLayer.timeToCompleteLevel setString:[[GameData sharedData] formattedTimeForLevel:timeFormattedFromString]];
    previousElapsed = elapsed;
    [self performSelector:@selector(updateTime) withObject:self afterDelay:0.1];
}




-(void)buildLevel{
    levelObjects = [[LevelObjects alloc] init];
    [[GameData sharedData] populateLevelObjects:levelObjects];
    
    totalBallsInThisLevel = 0;
    
    for(int i = 0;i<levelObjects.ballsArray.count;i++){
        totalBallsInThisLevel += [[levelObjects.ballsArray objectAtIndex:i] intValue];
    }
    
    int currentLevel = [[GameData sharedData] returnLevel];
    NSMutableString* levelLoaded = [NSMutableString stringWithFormat:@"Level Loaded %d", currentLevel];
    [Flurry logEvent:levelLoaded];
    
   
    moneyScoreAnimation = 0;
    numberOfCatsKilledThisLevel = 0;
    numberOfCatsThisLevel = 1;
    currentBallNumber = 0;
    pointTotalThisRound = 0;
    coinsThisLevel = 0;
    pointsToPassLevel = 1000;
    

    CGSize winSize = [[CCDirector sharedDirector] winSize];
    CGPoint point = ccp(winSize.width/2, winSize.height/2);
    
    bg = [CCSprite spriteWithFile:levelObjects.bgImage];
    //bg.anchorPoint = ccp(0,0);
    //bg.anchorPoint = CGPointMake(0, 0);
    bg.position = point;//levelObjects.bgImagePoint;
    [self addChild:bg z:-1000];
    //CGRect myRect = CGRectMake(0,0,960/2,320);
    //[bg setTextureRect:myRect];
    
    sun = [CCSprite spriteWithSpriteFrameName:levelObjects.sunImage];
    //sun.anchorPoint = CGPointMake(0, 0);
    [self addChild:sun z:-998];
    sun.position = levelObjects.sunImagePoint;
    
    mid = [CCSprite spriteWithFile:levelObjects.midBgImage];
    mid.position = levelObjects.midImagePoint;
    int randomSize = arc4random_uniform(100);
    CGFloat randomScale = (randomSize * .01) + .4;
    int level = [[GameData sharedData] returnLevel];
    if(level == 3){
        mid.anchorPoint = CGPointMake(1, 0);
        mid.scaleX = -1;
    }
    else{
        mid.anchorPoint = CGPointMake(0, 0);
        mid.scaleX = 1;
    }
    
    if(randomScale < 1)
    {
        randomScale = 1;
    }
    mid.anchorPoint = CGPointMake(0, 0);
    randomScale = 2.0;
    mid.scale = randomScale;
    //mid.position = ccp(0,arc4random_uniform(50));
    
    mid.position = ccp(0,-120);
    
    [self addChild:mid z:-800];
    int levelNumber = [[GameData sharedData] returnLevel];
    NSString *levelString = @"01";
    
    if(levelNumber < 25){
        if(levelNumber < 10){
            levelString = [NSString stringWithFormat:@"0%d",levelNumber];
        }
        else{
            levelString = [NSString stringWithFormat:@"%d",levelNumber];
        }
    }else{
        levelNumber -= 24;
        if(levelNumber < 10){
            levelString = [NSString stringWithFormat:@"0%d",levelNumber];
        }
        else{
            levelString = [NSString stringWithFormat:@"%d",levelNumber];
        }
    }
   
    //TODO CS - for testing levels only
  
    levelImage = [levelObjects.levelImage stringByAppendingString:levelString];
  
    [self addGBSpriteWithCoords:ccp(0, 0)];
    
    
    if(levelObjects.altImage.length > 0)
    {
        //CCSprite *frontPipe = [CCSprite spriteWithFile:levelObjects.altImage];
        //frontPipe.anchorPoint = CGPointMake(0, 0);
        //frontPipe.position = levelObjects.altImagePoint;
        //[self addChild:frontPipe z:800];
    }
   
    
    //set target out so gun does not go backwards
    currentCrosshairsLocation = dccp(300,100);
    
    //Common stuff
    mrPants = [MrPants ship];
    mrPants.scale = .85;
    mrPants.position = levelObjects.mrPantsStartPoint;//CGPointMake(180, 180);
    [self addChild:mrPants z:10 tag:4];

    
    startPosGun = ccp(mrPants.position.x + GUN_POS_X_OFFSET, mrPants.position.y + GUN_POS_Y_OFFSET);
    
    emitter = [[CCParticleSystemQuad alloc]initWithFile:ROCKET_PLIST];
    emitter.position = ccp(mrPants.position.x,mrPants.position.y + ROCKET_Y_OFFSET);
    emitter.scale = .5;
    //[self addChild:emitter];
    [self createLevels];
    
    //int level = [[GameData sharedData] returnLevel];
    NSString *myLevelHighScore = [NSString stringWithFormat:@"highScoreLevel%d",level];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
   
    /*
    id myHighScore = [defaults objectForKey:myLevelHighScore];
    if(myHighScore)
    {
        NSString *highScoreLabel = [NSString stringWithFormat:@"High Score :%@",myHighScore];
        [hudLayer.highscoreLabel setString:highScoreLabel];
    }
     */
    
    
    sliderBG = [CCSprite spriteWithSpriteFrameName:IMAGE_JETPACK_SLIDER_BG];
    sliderThumb = [CCSprite spriteWithSpriteFrameName:IMAGE_JETPACK_SLIDER_THUMB];
    
    sliderBG.anchorPoint = ccp(0,0);
    sliderThumb.anchorPoint = ccp(0,0);
    
    int posY = 72;
    int posX = 5;
    
    sliderBG.position = dccp(posX,posY);
    sliderThumb.position = ccp(posX + 6,posY + 6);
    
    //[self addChild:sliderBG];
    //[self addChild:sliderThumb];
    
}

-(void)createDeathFloor{
    b2BodyDef bodyDef;
    bodyDef.type = b2_staticBody;
    CCSprite *boxSprite = [CCSprite spriteWithSpriteFrameName:IMAGE_PLANK_01];
    
    [self addChild:boxSprite z:0 tag:TAG_DEATH_FLOOR];
    bodyDef.userData = boxSprite;
    b2Body *body = world->CreateBody(&bodyDef);
    body->SetTransform(b2Vec2(0.0, -6),0);
    b2PolygonShape shape;
    
    float width = (float) 10052 / 64;
    float height = (float) SIZE_PLANK_01_HEIGHT / 64;
    
    shape.SetAsBox(width,height);
    b2FixtureDef fixtureDef;
    fixtureDef.shape = &shape;
    fixtureDef.filter.groupIndex = COLLIDE_DEATH_FLOOR;
    fixtureDef.density = 6.0f;
    fixtureDef.friction = 1.0f;
    fixtureDef.restitution = 0.1f;
    body->CreateFixture(&fixtureDef);
}

-(void)initLevelItemsAndGraphics{
    
}


#pragma mark --- Win / Reset states

-(void)somethingKilledWasCat:(BOOL)wasCat withPoints:(int)points{
    if(wasCat){
        numberOfCatsKilledThisLevel++;
        [self addCoinAmount:10];
        
    }else{
        [self addCoinAmount:10];
    }
    [self addPoints:points];
}

-(void)addCoinAmount:(int)value{
    coinsThisLevel += value;
}

-(void)youWonOrLost{
    GameData *gameData = [GameData sharedData];
    [self unschedule:@selector(youWonOrLost:)];
    [self unschedule:@selector(createOutOfBallsMenu:)];
    int addBallsPoints = (totalBallsInThisLevel - currentBallNumber) * BALLS_POINT_WORTH;
    addBallsPoints += [gameData returnScoreAddition];
    int totalPoints = pointTotalThisRound;
    totalPoints += addBallsPoints;
    //pointTotalThisRound += addBallsPoints;
    
    if(!hasWonLevel)
    {
        
        //[self performSelector:@selector(createOutOfBallsMenu) withObject:nil afterDelay:4.0f];
        if([gameData returnLevel] == 24 && !gameData.hasWon24thLevel){
           [self addCoinAmount:5000];
        }

        [gameData addToTotalNumberOfCoins:coinsThisLevel];
        [gameData addBonusCoins];
        gameData.pointsScoredCurrentLevel = totalPoints;
        [gameData setHighScoreForLevel:totalPoints];
        [gameData setLevelWon:[gameData returnLevel]];
        
        
        
        nextShotReady = NO;
        [self unschedule:@selector(duringTick:)];
        [self unschedule:@selector(timerAfterThrow:)];
        [self unschedule:@selector(followBallTick:)];
        currentBullet = nil;
        [self destroyAllBullets];
        

        
        
        if([gameData returnLevel] == 24 && !gameData.hasWon24thLevel){
            gameData.hasWon24thLevel = YES;
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"goodBoyWon"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [self goodBoy];
        }
        else{
            [self createMenu];
        }
        
        
        [self hideMenuItems];
        NSLog(@"YOU WON!");
    }
        
    hasWonLevel = YES;
}

-(void)hideMenuItems{
    change_ball.opacity = 0;
    press_here.opacity = 0;
    press_to_fire.opacity = 0;
}

-(void)resetGameReset: (ccTime) dt{
    [self unschedule:@selector(resetGameReset:)];
}

-(void)youWinLevel:(ccTime)dt{
    //gameWon = YES;
    //[self hideMenuItems];
    
}

-(void) addGBSpriteWithCoords:(CGPoint)p{
    levelFrontBackground = [CCSprite spriteWithFile:[NSString stringWithFormat:@"%@.png", levelImage]];
	[self addChild:levelFrontBackground z:-1];
	
	levelFrontBackground.position = ccp(p.x, p.y);
	
	frontPlaneBodyDef.type = b2_staticBody;
    levelFrontBackground.tag = TAG_WORLD_LEVEL;
    
	frontPlaneBodyDef.position.Set(p.x/PTM_RATIO, p.y/PTM_RATIO);
	frontPlaneBodyDef.userData = levelFrontBackground;
	b2Body *body = world->CreateBody(&frontPlaneBodyDef);
    //minScale = self.contentSize.width / levelFrontBackground.contentSize.width;
    
    // add the fixture definitions to the body
	[[GB2ShapeCache sharedShapeCache] addFixturesToBody:body forShapeName:levelImage];
    [levelFrontBackground setAnchorPoint:[[GB2ShapeCache sharedShapeCache] anchorPointForShape:levelImage]];
}

-(void)hideFollowPath
{
    CCSprite* dot = (CCSprite *)[self getChildByTag:556];
    while (dot) {
        [dot removeFromParentAndCleanup:YES];
        dot = (CCSprite *)[self getChildByTag:556];
    }
    
    [self crossHairsSetHidden];
}

-(void)hidePath
{
    CCSprite* dot = (CCSprite *)[self getChildByTag:555];
    while (dot) {
        [dot removeFromParentAndCleanup:YES];
        dot = (CCSprite *)[self getChildByTag:555];
    }
    
    [self crossHairsSetHidden];
}
#pragma mark trajectory stuff
-(void) drawPathForCurrentTurretState:(b2Vec2)startVelocity
{
    
    currentVelocity = startVelocity;
    
    b2BodyDef bodyDef;
    bodyDef.type = b2_dynamicBody;
    b2Body *body = world->CreateBody(&bodyDef);
    b2CircleShape shape;
    float radiusInMeters = 0.1f;
    if(IS_IPAD){
        radiusInMeters *= 2;
    }
    shape.m_radius = radiusInMeters;
    b2FixtureDef fixture;
    fixture.shape = &shape;
    fixture.density = self.currentDensity;//2.0f;
    fixture.friction = 1.0f;
    fixture.restitution = 0.1f;
    body->CreateFixture(&fixture);
    body->SetLinearVelocity(startVelocity);
    
    b2Vec2 newVelocity = body->GetLinearVelocity();
    
    world->DestroyBody(body);

    //Step 1: remove previous path
    CCSprite* dot = (CCSprite *)[self getChildByTag:555];
    while (dot) {
        [dot removeFromParentAndCleanup:YES];
        dot = (CCSprite *)[self getChildByTag:555];
    }
    
    
    
    //Step 2: draw new path
    
    CGPoint lastPosition;
    GLfloat angle;
    
    startPosGun = dccp(mrPants.position.x + GUN_POS_X_OFFSET, mrPants.position.y + GUN_POS_Y_OFFSET);
    
    for (int i = 1; i <= self.drawAmount; i++) {
        
        //get the postion at this iteration
        if(i > self.drawDotAfterSteps){
            b2Vec2 trajectoryPosition = [self getTrajectoryPoint:[CTUtility toMeters:startPosGun] andStartVelocity:newVelocity andSteps:i*self.altDrawSteps];
            
            //create the point
            CCSprite* dot = [CCSprite spriteWithSpriteFrameName:IMAGE_DOT];
            if(i %2){
                dot.scale = 0.25f;
            }
            else{
                dot.scale = 0.15f;
            }
            
            dot.opacity = 200;
            
            
            //fade out the path
            // CGFloat ratio = (float)i/drawAmount;
            //[dot setOpacity:0 + ratio*200];
            
            //add the point to the stage
            [self addChild:dot z:0 tag:Z_DOTS];
            
            dot.position = [CTUtility toPixels:trajectoryPosition];
            
            angle = [self calculateAngle:lastPosition.x :-lastPosition.y :dot.position.x :-dot.position.y] ;
            
            lastPosition = dot.position;
            
        }
        
    }
    
    crossHairs.rotation = -(angle-90);
    [self crossHairsSetPosition:lastPosition fade:NO];
}

-(b2Vec2) getTrajectoryPoint:(b2Vec2) startingPosition andStartVelocity:(b2Vec2) startingVelocity andSteps: (float)n
{
    //velocity and gravity are given per second but we want time step values here
    // seconds per time step (at 60fps)
    float t = 1 / 60.0f;
    
    // m/s
    b2Vec2 stepVelocity = t * startingVelocity;
    
    // m/s/s
    b2Vec2 stepGravity = t * t * world->GetGravity();
    
    return startingPosition + n * stepVelocity + 0.5f * (n*n+n) * stepGravity;
}

#pragma mark other
int currentTime = 0;
int totalTime = 10;
-(void) draw
{
    [super draw];
	ccGLEnableVertexAttribs( kCCVertexAttribFlag_Position );
	
	kmGLPushMatrix();
	
	world->DrawDebugData();
	
	kmGLPopMatrix();
    
}

-(void)setNextShotReady:(ccTime)dt
{
    
    [self unschedule:@selector(setNextShotReady:)];
    nextShotReady = [self getBallsMenuCountsShouldFire];
    if (numberOfCatsKilledThisLevel >= numberOfCatsThisLevel)
    {
        
        [self performSelector:@selector(youWonOrLost) withObject:nil afterDelay:2.0f];
        
        [self stopLevelTimer];
        nextShotReady = NO;
        return;
    }

    if(currentBallNumber == totalBallsInThisLevel)
    {
        [self hideMenuItems];
        [self performSelector:@selector(createOutOfBallsMenu) withObject:nil afterDelay:4.0f];
        nextShotReady = NO;
    }
    
}

-(void)resumeAllBullets{
    if(ballsDestroyed){
        return;
    }
    for (Bullet *bullet in bulletsArray){
        if(bullet){
            [bullet resumeSchedulerAndActions];
        }
    }
}
-(void)pauseAllBullets{
    if(ballsDestroyed){
        return;
    }
    for (Bullet *bullet in bulletsArray){
        if(bullet){
            [bullet pauseSchedulerAndActions];
        }
    }
}

-(void)destroyAllBullets
{
    if(ballsDestroyed)
    {
        return;
    }
    
    ballsDestroyed = YES;
    
    for (Bullet *bullet in bulletsArray)
    {
        if(bullet)
        {
            [bullet kill];
        }
        [bullet release];
        bullet = nil;
        
    }
    [bulletsArray release];
    bulletsArray = nil;
}

#pragma mark ---___---___---___---___---___---___ HIT SPRITS

-(void)hitSprites:(CCSprite *)spriteA hitSpriteB:(CCSprite *)spriteB withContact:(MyContact) contact
{
    CCSprite *ballSprite = NULL;
    CCSprite *boxSprite = NULL;
    b2Fixture *ballFixture = NULL;
    b2Fixture *boxFixture = NULL;
    b2Fixture *breakableFixture = NULL;
        
    b2Filter boxFilter;
    
    CGFloat threasholdToRegister = .8;
    if(IS_IPAD){
        threasholdToRegister = 1.6;
    }
    
    //Bullet hits a breakable item

    if(TAG_BULLET == spriteA.tag && TAG_WARP_HOLE == spriteB.tag)
    {
        Bullet *bullet = (Bullet *)spriteA.parent;
        [self shootBallFromWarpWithBullet:bullet isWarp1:YES];
        return;
    }
    else if(TAG_WARP_HOLE == spriteA.tag && TAG_BULLET == spriteB.tag)
    {
        Bullet *bullet = (Bullet *)spriteB.parent;
        [self shootBallFromWarpWithBullet:bullet isWarp1:YES];
        return;
    }
    
    if(TAG_BULLET == spriteA.tag && TAG_WARP_HOLE_02 == spriteB.tag)
    {
        Bullet *bullet = (Bullet *)spriteA.parent;
        [self shootBallFromWarpWithBullet:bullet isWarp1:NO];
        return;
    }
    else if(TAG_WARP_HOLE_02 == spriteA.tag && TAG_BULLET == spriteB.tag)
    {
        Bullet *bullet = (Bullet *)spriteB.parent;
        [self shootBallFromWarpWithBullet:bullet isWarp1:NO];
        return;
    }
    
    
    
    if(TAG_BULLET == spriteA.tag && TAG_BOMB == spriteB.tag)
    {
        ballSprite = spriteA;
        boxSprite = spriteB;
        boxFixture = contact.fixtureB;
        ballFixture = contact.fixtureA;
    }
    else if(TAG_BOMB == spriteA.tag && TAG_BULLET == spriteB.tag)
    {
        NSLog(@"BOMB HIT");
        boxSprite = spriteA;
        ballSprite = spriteB;
        boxFixture = contact.fixtureA;
        ballFixture = contact.fixtureB;
    }
    

    
    if (TAG_BULLET == spriteA.tag && TAG_BREAKABLE_ITEM == spriteB.tag)
    {
        ballSprite = spriteA;
        boxSprite = spriteB;
        boxFixture = contact.fixtureB;
        ballFixture = contact.fixtureA;
        
        
    }
    else if (spriteA.tag == TAG_BREAKABLE_ITEM && spriteB.tag == TAG_BULLET)
    {
        boxSprite = spriteA;
        ballSprite = spriteB;
        boxFixture = contact.fixtureA;
        ballFixture = contact.fixtureB;
        
    }
    
    BOOL mustKill = NO;
    
    if (TAG_BULLET == spriteA.tag && TAG_BREAKABLE_ENEMY == spriteB.tag)
    {
        ballSprite = spriteA;
        boxSprite = spriteB;
        ballFixture = contact.fixtureA;
        mustKill = YES;
    }
    else if (spriteA.tag == TAG_BREAKABLE_ENEMY && spriteB.tag == TAG_BULLET)
    {
        boxSprite = spriteA;
        ballSprite = spriteB;
        ballFixture = contact.fixtureB;
        mustKill = YES;
    }
    
    if (TAG_BULLET == spriteA.tag && TAG_BREAKABLE_ENEMY_NON_HURTING == spriteB.tag){
        ballSprite = spriteA;
        boxSprite = spriteB;
        ballFixture = contact.fixtureA;
    }else if (spriteA.tag == TAG_BREAKABLE_ENEMY_NON_HURTING && spriteB.tag == TAG_BULLET){
        boxSprite = spriteA;
        ballSprite = spriteB;
        ballFixture = contact.fixtureB;
    }
    
    
    
    //Breakable item hits the enemy
    
    if (TAG_BREAKABLE_ITEM == spriteA.tag && TAG_BREAKABLE_ENEMY == spriteB.tag)
    {
        ballSprite = spriteA;
        boxSprite = spriteB;
        breakableFixture = contact.fixtureA;
    }
    else if (spriteA.tag == TAG_BREAKABLE_ENEMY && spriteB.tag == TAG_BREAKABLE_ITEM)
    {
        boxSprite = spriteA;
        ballSprite = spriteB;
        breakableFixture = contact.fixtureB;
        
    }
    
    
    
    //TAG_NON_BREAKABLE_OBJECT
    //Breakable item hits the a breakable item
    
    if (TAG_BREAKABLE_ITEM == spriteA.tag && TAG_BREAKABLE_ITEM == spriteB.tag)
    {
        ballSprite = spriteA;
        boxSprite = spriteB;
        breakableFixture = contact.fixtureA;
        threasholdToRegister *= 2;
    }
    else if (spriteA.tag == TAG_BREAKABLE_ITEM && spriteB.tag == TAG_BREAKABLE_ITEM)
    {
        boxSprite = spriteA;
        ballSprite = spriteB;
        breakableFixture = contact.fixtureB;
        threasholdToRegister *= 2;
        
    }
    
    shouldPlayBounceAudio = NO;
    
    if (TAG_BULLET == spriteA.tag && TAG_WORLD_LEVEL == spriteB.tag){
        if(contact.force > 2){
            shouldPlayBounceAudio = YES;
        }
    }else if (spriteA.tag == TAG_WORLD_LEVEL && spriteB.tag == TAG_BULLET){
        if(contact.force > 2){
            shouldPlayBounceAudio = YES;
        }
    }else if (spriteA.tag == TAG_BULLET && spriteB.tag == TAG_NON_BREAKABLE_OBJECT){
        if(contact.force > 2){
            shouldPlayBounceAudio = YES;
        }
    }else if (spriteA.tag == TAG_NON_BREAKABLE_OBJECT && spriteB.tag == TAG_BULLET){
        if(contact.force > 2){
            shouldPlayBounceAudio = YES;
        }
    }
    
    if(shouldPlayBounceAudio){
        [self playBounceAudio];
    }
    
    
    if (TAG_NON_BREAKABLE_OBJECT == spriteA.tag && TAG_BREAKABLE_ENEMY == spriteB.tag)
    {
        ballSprite = spriteA;
        boxSprite = spriteB;
        breakableFixture = contact.fixtureA;
    }
    else if (spriteA.tag == TAG_BREAKABLE_ENEMY && spriteB.tag == TAG_NON_BREAKABLE_OBJECT)
    {
        boxSprite = spriteA;
        ballSprite = spriteB;
        breakableFixture = contact.fixtureB;
        
    }
    
    
    if (TAG_NON_BREAKABLE_OBJECT == spriteA.tag && TAG_BREAKABLE_ITEM == spriteB.tag)
    {
        ballSprite = spriteA;
        boxSprite = spriteB;
        breakableFixture = contact.fixtureA;
       // threasholdToRegister = 2;
    }
    else if (spriteA.tag == TAG_BREAKABLE_ITEM && spriteB.tag == TAG_NON_BREAKABLE_OBJECT)
    {
        boxSprite = spriteA;
        ballSprite = spriteB;
        breakableFixture = contact.fixtureB;
       // threasholdToRegister = 2;
    }
    
    
    
    //Kill things hitting the death floor
    if (TAG_DEATH_FLOOR == spriteA.tag && TAG_BREAKABLE_ENEMY == spriteB.tag)
    {
        BreakableBlock *block = (BreakableBlock *) spriteB.parent;
        [block hitWithForce:2000 fromBullet:YES];
        NSLog(@"Something hit death floor");
    }
    else if (spriteA.tag == TAG_BREAKABLE_ENEMY && spriteB.tag == TAG_DEATH_FLOOR)
    {
        BreakableBlock *block = (BreakableBlock *) spriteA.parent;
        [block hitWithForce:2000 fromBullet:YES];
        NSLog(@"Something hit death floor");
    }
    
    //Kill things hitting the death floor
    if (TAG_DEATH_FLOOR == spriteA.tag && TAG_BREAKABLE_ITEM == spriteB.tag)
    {
        BreakableBlock *block = (BreakableBlock *) spriteB.parent;
        [block hitWithForce:2000 fromBullet:YES];
        ballFixture = contact.fixtureA;   
        NSLog(@"Something hit death floor");
    }
    else if (spriteA.tag == TAG_BREAKABLE_ITEM && spriteB.tag == TAG_DEATH_FLOOR)
    {
        BreakableBlock *block = (BreakableBlock *) spriteA.parent;
        [block hitWithForce:2000 fromBullet:YES];
        ballFixture = contact.fixtureB; 
         NSLog(@"Something hit death floor");
    }
    
    if (TAG_BULLET == spriteA.tag && 2001 == spriteB.tag)
    {
        CCTexture2D* tex = [[CCTextureCache sharedTextureCache] addImage:IMAGE_GREEN_BUTTON];
        [craneHookButton setTexture:tex];
        [hook releaseBall];
    }
    else if (spriteA.tag == 2001 && spriteB.tag == TAG_BULLET)
    {
        CCTexture2D* tex = [[CCTextureCache sharedTextureCache] addImage:IMAGE_GREEN_BUTTON];
        [craneHookButton setTexture:tex];
        [hook releaseBall];
    }
    
    
    
    b2Vec2 velocity;
    float32 currentSpeed = 0;
    
    if(ballFixture)
    {
        b2Body *myBallBody = ballFixture->GetBody();
        velocity = myBallBody->GetLinearVelocity();
        currentSpeed = contact.force + 5;//velocity.Length();
       
        /*
        b2Filter newFilter = boxFilter;
        newFilter.groupIndex = -1;
        boxFixture->SetFilterData(newFilter);
        
        newFilter = ballFixture->GetFilterData();
        newFilter.groupIndex = -1;
        ballFixture->SetFilterData(newFilter);
        */
        if(contact.force > threasholdToRegister)
        {
            [self playBounceAudio];
        }
        
        if(mustKill){
            NSLog(@"MUST KILL");
            BreakableBlock *block = (BreakableBlock *) boxSprite.parent;
            [block hitWithForce:20 fromBullet:YES];
        }
        else if(currentSpeed > threasholdToRegister)
        {
            //TODO FIX THIS
            BreakableBlock *block = (BreakableBlock *) boxSprite.parent;
            [block hitWithForce:currentSpeed fromBullet:YES];
            
            
        }
    }
    if(breakableFixture)
    {
        
        b2Body *breakableBody = breakableFixture->GetBody();
        
        velocity = breakableBody->GetLinearVelocity();
        currentSpeed = contact.force / 2;
        
        if(IS_IPAD){
            currentSpeed *= .4;
        }
        
        if(currentSpeed > threasholdToRegister)
        {
            //TODO FIX THIS
            BreakableBlock *block = (BreakableBlock *) boxSprite.parent;
            [block hitWithForce:currentSpeed fromBullet:YES];
            
        }
    }
}

-(void)playBounceAudio
{
    if(ENABLE_AUDIO && canPlayBounceSound  && ![[GameData sharedData]areAmbientFXMuted]){
        canPlayBounceSound = NO;
        [[SimpleAudioEngine sharedEngine] playEffect:AUDIO_BALL_BOUNCE];
        [self performSelector:@selector(setAllowedToPlayBounceAudio) withObject:nil afterDelay:.3];
    }
}

-(void)setAllowedToPlayBounceAudio{
    canPlayBounceSound = YES;
}

-(void)moveMrPants
{
    if(leftSideTouchIsDown)
    {
        sliderThumb.position = ccp(sliderThumb.position.x,touchLeftSideCurrentPosY * 2);
        CGFloat newPositionY = mrPants.position.y;
        newPositionY += (touchLeftSideCurrentPosY * 2 - mrPants.position.y) * .04;
        mrPants.position = ccp(mrPants.position.x,newPositionY);
        startPosGun = dccp(mrPants.position.x + GUN_POS_X_OFFSET, mrPants.position.y + GUN_POS_Y_OFFSET);
        emitter.position = dccp(mrPants.position.x,mrPants.position.y + ROCKET_Y_OFFSET);
        [self dotSetup];
        [self rotateGun];
        [mrPants setRotationOfGun:-gunRoation];
        
        end_ = mrPants.position;
    }
}


-(BOOL)getBallsMenuCountsShouldFire
{
    int cbt = 0;
    CCLabelTTF *currentLabel;
    
    if(ballsMenu.orangeBallButton.isSelected){
        currentLabel = ballsMenu.orangeLabel;
        cbt = [currentLabel.string intValue];
    }
    else if(ballsMenu.blackBallButton.isSelected){
        currentLabel = ballsMenu.blackLabel;
        cbt = [currentLabel.string intValue];
        
    }
    else if(ballsMenu.redBallButton.isSelected){
        currentLabel = ballsMenu.redLabel;
        cbt = [currentLabel.string intValue];
        
    }
    else if(ballsMenu.blueBallButton.isSelected){
        currentLabel = ballsMenu.blueLabel;
        cbt = [currentLabel.string intValue];
    }
    else{
        currentLabel = nil;
        //nothing
    }
    if(cbt < 0)
    {
        cbt = 0;
    }
    
    [currentLabel setString:[NSString stringWithFormat:@"%d",cbt]];
    
    if(cbt == 0)
    {
        return NO;
    }
    
    return YES;
}

-(BOOL)adjustBallsMenuCountsShouldFire
{
    int cbt = 0;
    CCLabelTTF *currentLabel;
    
    if(ballsMenu.orangeBallButton.isSelected){
        currentLabel = ballsMenu.orangeLabel;
        cbt = [currentLabel.string intValue];
        cbt--;
    }
    else if(ballsMenu.blackBallButton.isSelected){
        currentLabel = ballsMenu.blackLabel;
        cbt = [currentLabel.string intValue];
        cbt--;
        
    }
    else if(ballsMenu.redBallButton.isSelected){
        currentLabel = ballsMenu.redLabel;
        cbt = [currentLabel.string intValue];
        cbt--;
        
    }
    else if(ballsMenu.blueBallButton.isSelected){
        currentLabel = ballsMenu.blueLabel;
        cbt = [currentLabel.string intValue];
        cbt--;
    }
    else{
        currentLabel = nil;
        //nothing
    }
    if(cbt < 0)
    {
        cbt = 0;
    }
    
    [currentLabel setString:[NSString stringWithFormat:@"%d",cbt]];
    
    if(cbt == 0)
    {
        return NO;
    }
    
    return YES;
}

-(void)afterStep
{
    std::vector<MyContact>::iterator pos;
    
    for(pos = _contactListener->_contacts.begin(); pos != _contactListener->_contacts.end(); ++pos)
    {
        MyContact contact = *pos;
        b2Body *a = contact.fixtureA->GetBody();
        b2Body *b = contact.fixtureB->GetBody();
        
        if (!a->GetUserData() || !b->GetUserData())
        {
            return;
        }
        
        CCSprite *spriteA = (CCSprite *) a->GetUserData();
        CCSprite *spriteB = (CCSprite *) b->GetUserData();
        
        if(spriteA && spriteB)
        {
            if (TAG_BULLET == spriteA.tag || spriteB.tag == TAG_BULLET)
            {
                if(throwInProgress)
                {
                    throwInProgress = NO;
                }
            }
            
            
            if(spriteA.tag > 0 && spriteB.tag > 0)
            {
                [self hitSprites:spriteA hitSpriteB:spriteB withContact:contact];
            }
        }
        
    }
}

-(void)step:(ccTime)dt {
    float32 frameTime = dt;
    int stepsPerformed = 0;
    while ( (frameTime > 0.0) && (stepsPerformed < MAXIMUM_NUMBER_OF_STEPS) ){
        float32 deltaTime = std::min( frameTime, FIXED_TIMESTEP );
        frameTime -= deltaTime;
        if (frameTime < MINIMUM_TIMESTEP) {
            deltaTime += frameTime;
            frameTime = 0.0f;
        }
        world->Step(deltaTime,VELOCITY_ITERATIONS,POSITION_ITERATIONS);
        stepsPerformed++;
        [self afterStep]; // process collisions and result from callbacks called by the step
    }
    world->ClearForces ();
}

-(void)shootBallFromWarpWithBullet:(Bullet *)bullet isWarp1:(BOOL)isWarp1{
    
    CCSprite *currentWarp = nil;
    if(isWarp1){
        currentWarp = rotatingArrowWarp;
    }else{
        currentWarp = rotatingArrowWarp2;
    }

    CGPoint myPointAroundTheCircle = ccp(0, 0);
    float radiusOfMyCircle = 21.0f;
    float angleOfMyPointAroundTheCircle = CC_DEGREES_TO_RADIANS(currentWarp.rotation);
    
    CGPoint anchorPointOfMyCircle = currentWarp.position;
    
    myPointAroundTheCircle.x = cos(angleOfMyPointAroundTheCircle) * radiusOfMyCircle;
    myPointAroundTheCircle.y = sin(-angleOfMyPointAroundTheCircle) * radiusOfMyCircle;
    
    myPointAroundTheCircle = ccpAdd(anchorPointOfMyCircle, myPointAroundTheCircle);
    
    CGPoint shootVector = ccpSub(myPointAroundTheCircle,currentWarp.position);
    CGFloat shootAngle = ccpToAngle(shootVector);
    
    double x1 = cos(shootAngle);
    double y1 = sin(shootAngle);

    float power = 8;
    if(IS_IPAD){
        power *= 2;
    }
    
    b2Vec2 force = b2Vec2(x1 * power ,y1 * power);
    
    [bullet removeAllVelocity];
    [bullet resetPosition:currentWarp.position];
    [bullet applyBulletForce:force];
}

-(void)tick:(ccTime)dt
{
    if(self.isGamePaused)
    {
        return;
    }
    //[self moveMrPants];
    
    [self step:dt];
    
    if(movingPlatform)
    {
        movingPlatformY += .1;
        if(movingPlatformY > 40)
        {
            movingPlatformY = 0;
            moveAmount = -moveAmount;
        }
        
        [movingPlatform movePositionY:moveAmount andX:0];
        
    }
    
    if(rotatingArrowWarp && shouldRotateWarp1){
        rotatingArrowWarp.rotation += 1;
    }
    if(rotatingArrowWarp2 && shouldRotateWarp2){
        rotatingArrowWarp2.rotation += 1;
    }
    
    int warpMoveAmount = 33;
    
    if(movingArrowWarp){
        
        if(!platformStartY){
            platformStartY = 0;
        }
        platformStartY += .1;
        if(platformStartY > warpMoveAmount)
        {
            platformStartY = 0;
            moveAmount = -moveAmount;
        }
        CGPoint currentWarpPos = rotatingArrowWarp2.position;
        currentWarpPos.y += moveAmount;
        
        rotatingArrowWarp2.position = currentWarpPos;
    }
    
    if(movingArrowWarpX){
        
        if(!platformStartX){
            platformStartX = 0;
        }
        platformStartX += .1;
        if(platformStartX > warpMoveAmount)
        {
            platformStartX = 0;
            moveAmount = -moveAmount;
        }
        CGPoint currentWarpPos = rotatingArrowWarp2.position;
        currentWarpPos.x += moveAmount;
        
        rotatingArrowWarp2.position = currentWarpPos;
    }
    
    if(bg){
        bg.rotation -= .05;
    }

    if(moneyScoreAnimation != coinsThisLevel)
    {
        
        
        if(moneyScoreAnimation < coinsThisLevel)
        {
            NSString *moneyLabel = @"$";
            int counterIncrement = coinsThisLevel * .1;
            moneyScoreAnimation += counterIncrement;
            NSString *myString = [NSString stringWithFormat:@"%i",moneyScoreAnimation];
            NSString *scoreLabelString = [moneyLabel stringByAppendingString:myString];
            [hudLayer.moneyLabel setString:scoreLabelString];
        }
        else
        {
            if(moneyScoreAnimation != coinsThisLevel)
            {
                NSString *moneyLabel = @"$";
                NSString *myString = [NSString stringWithFormat:@"%d",coinsThisLevel];
                NSString *scoreLabelString = [moneyLabel stringByAppendingString:myString];
                [hudLayer.moneyLabel setString:scoreLabelString];
                moneyScoreAnimation = coinsThisLevel;
            }
            
        }
    }

    if(gunRoation && touchIsDown)
    {
        [mrPants setRotationOfGun:-gunRoation];
        [self animateSpringScale];
        shouldAnimateSpringScale = NO;
        
    }
    else
    {
        //[mrPants setSpringScale:0];
    }
    
    
	
	//Iterate over the bodies in the physics world
	for (b2Body* b = world->GetBodyList(); b; b = b->GetNext())
    {
		if (b->GetUserData() != NULL) {
			//Synchronize the AtlasSprites position and rotation with the corresponding body
			CCSprite *myActor = (CCSprite*)b->GetUserData();
			myActor.position = CGPointMake( b->GetPosition().x * PTM_RATIO, b->GetPosition().y * PTM_RATIO);
            
            if(myActor.tag == TAG_WARP_HOLE || myActor.tag == TAG_WARP_HOLE_02)
            {
                //myActor.rotation -= 10;
            }
            else
            {
                myActor.rotation = -1 * CC_RADIANS_TO_DEGREES(b->GetAngle());
            }
			
		}
    }
    
    
}


-(void)setAllowedToShoot:(ccTime)dt
{
    [self unschedule:@selector(setAllowedToShoot:)];
    allowedToShoot = YES;
}

-(void)crossHairsSetPosition:(CGPoint)atPoint fade:(BOOL)shouldFade
{
    if(!crossHairs)
    {
        crossHairs = [CCSprite spriteWithSpriteFrameName:IMAGE_CROSSHAIRS];
        [self addChild:crossHairs z:Z_CROSSHAIR];
        crossHairs.opacity = 0;
    }
    if(shouldFade)
    {
        shouldFade = NO;
        if(crossHairs.opacity != 255)
        {
            id fadeCrossHairs = [CCFadeIn actionWithDuration:.3];
            [crossHairs runAction: fadeCrossHairs];
        }
        
    }
    else
    {
        crossHairs.opacity = 255;
    }
    
    crossHairs.position = atPoint;    
}

-(void)crossHairsSetHidden
{
    [crossHairs stopAllActions];
    crossHairs.opacity = 0;
}

#pragma mark --- Touch Events

- (void)ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if(self.isGamePaused){
        return;
    }
    CGPoint location;

       
    for( UITouch *touch in touches )
    {
        location = [touch locationInView: [touch view]];
		
        if(location.y < 60)
        {
            touchIsDown = NO;
            return;
        }
        
        
        leftSideTouchIsDown = NO;
        
        
        if(location.x < mrPants.position.x)
        {
            touchLeftSideStartPosY = [[CCDirector sharedDirector] convertToGL: location].y;
            touchLeftSideCurrentPosY = [[CCDirector sharedDirector] convertToGL: location].y;
            if(touchLeftSideCurrentPosY > levelObjects.maxAltitude)
            {
                touchLeftSideCurrentPosY = levelObjects.maxAltitude;
            }
            if(touchLeftSideCurrentPosY < levelObjects.minAltitude)
            {
                touchLeftSideCurrentPosY = levelObjects.minAltitude;
            }
            leftSideTouchIsDown = YES;
            return;
        }
        
        hasTakenAim = YES;
        
		location = [[CCDirector sharedDirector] convertToGL: location];
        
        if(duringTouchLocation.x == 0)
        {
            duringTouchLocation = location;
            startTouchLocation = location;
        }
        else
        {
            yAdjust = duringTouchLocation.y - location.y;
            xAdjust = duringTouchLocation.x - location.x;
            startTouchLocation = duringTouchLocation;
            
            duringTouchLocation = location;
        }

        [self moveCrosshairs];
        
        if (allowedToShoot)
        {
            [self unschedule:@selector(setAllowedToShoot:)];
            [self schedule: @selector(setAllowedToShoot:) interval:.5];
            
            allowedToShoot = NO;
            shouldFire = YES;
            touchIsDown = YES;
            
            //Gun Rotation
            [self rotateGun];
        }
        else
        {
            throwInProgress = NO;
            shouldFire = NO;
            touchIsDown = NO;
        }
        
        [self dotSetup];
        
    }
}

-(void)moveCrosshairs
{
    float posX = (duringTouchLocation.x) + CROSSHAIR_OFFSET_X;
    float posY = (duringTouchLocation.y * CROSSHAIR_MULTIPLYER) + CROSSHAIR_OFFSET_Y;
    currentCrosshairsLocation = ccp(posX, posY);
    [self dotSetup];

}

-(void)rotateGun
{
    CGPoint diff = ccpSub(currentCrosshairsLocation,startPosGun);
    float rads = atan2f( diff.y, diff.x);
    gunRoation = CC_RADIANS_TO_DEGREES(rads);
}
- (void)ccTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
    
    if(self.isGamePaused){
        return;
    }
    CGPoint location;

    for( UITouch *touch in touches ){

        location = [touch locationInView: [touch view]];
        location = [[CCDirector sharedDirector] convertToGL: location];
        
        touchLeftSideCurrentPosY = location.y;
        
        if(touchLeftSideCurrentPosY > levelObjects.maxAltitude){
            touchLeftSideCurrentPosY = levelObjects.maxAltitude;
        }

        if(touchLeftSideCurrentPosY < levelObjects.minAltitude){
            touchLeftSideCurrentPosY = levelObjects.minAltitude;
        }
        
        if(leftSideTouchIsDown){
            return;
        }

        if(location.x > mrPants.position.x){
            duringTouchLocation = ccp(location.x,location.y);
            [self moveCrosshairs];
        }

        if(shouldFire){
            [self rotateGun];
        }
    }
    
}

-(void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    if(self.isGamePaused){
        return;
    }
    
    for( UITouch *touch in touches ) {
        
        if(touchIsDown)
        {
            CGPoint location = [touch locationInView: [touch view]];
            
            location = [[CCDirector sharedDirector] convertToGL: location];
            touchIsDown = NO;
            leftSideTouchIsDown = NO;
            
            
            [self unschedule:@selector(setNextShotReady:)];
            [self schedule: @selector(setNextShotReady:) interval:.5];
            ///nextShotReady =
            shouldFire = NO;

            if(nextShotReady){
                nextShotReady = NO;
                shouldFire = YES;
                [self fireBullet];
                [self adjustBallsMenuCountsShouldFire];
            }
        }
	}
}


#pragma mark --- Predict the shot for the dots

-(void)dotSetup
{
    if(!hasTakenAim)
    {
        return;
    }
    
    double positionX = (mrPants.position.x + GUN_POS_X_OFFSET);
    double positionY = (mrPants.position.y + GUN_POS_Y_OFFSET);
    CGPoint endOfGunPoint = ccp(positionX,positionY);
    
    CGPoint shootVector = ccpSub(currentCrosshairsLocation, endOfGunPoint);
    CGFloat shootAngle = ccpToAngle(shootVector);
    
    float power = self.bulletPower;
    
    if(IS_IPAD){
        power *= 2;
    }
    double x1 = cos(shootAngle);
    double y1 = sin(shootAngle);
    
    b2Vec2 force = b2Vec2(x1* power,y1* power);
    [self drawPathForCurrentTurretState:force];
    return;
}

-(void)animateDrawPathState
{
    self.myXTween = [CCActionTween actionWithDuration:.20 key:@"currentPosXOfPath" from:lastPathForce.x to:currentPathForce.x];
    self.myYTween = [CCActionTween actionWithDuration:.20 key:@"currentPosYOfPath" from:lastPathForce.y to:currentPathForce.y];
    
    [self runAction:self.myXTween];
    [self runAction:self.myYTween];
}

#pragma mark --- fire bullet BOMBS AWAY!
-(void)animateSpringScale
{
    if(shouldAnimateSpringScale)
    {
        //id moveUp = [CCSequence actions:[CCMoveTo actionWithDuration:timeToLevelComplete position:ccp(180,180)], nil];
        //[sun runAction:[CCSequence actions:moveUp, nil]];
        
        float springScale = .1;
        [mrPants setSpringScale:springScale];
    }
}

-(void)reduceBallCount{
    currentBallTick = 0;
    currentBallNumber++;
}
-(void)fireBullet
{
    shouldAnimateSpringScale = YES;
    [mrPants setSpringScale:0];
    [mrPants shootGun];
    [self hideFollowPath];
    [self hidePath];
    double positionX = (mrPants.position.x + GUN_POS_X_OFFSET);
    double positionY = (mrPants.position.y + GUN_POS_Y_OFFSET);
    CGPoint endOfGunPoint = ccp(positionX,positionY);
    
    CGPoint shootVector = ccpSub(currentCrosshairsLocation, endOfGunPoint);
    CGFloat shootAngle = ccpToAngle(shootVector);
    
    float power = self.bulletPower;
    
    float x1 = cos(shootAngle);
    float y1 = sin(shootAngle);
    
    if(IS_IPAD){
        power *= 2;
    }

    b2Vec2 force = b2Vec2(x1* power,y1* power);
    [self createBullet:endOfGunPoint force:force];
    
    throwInProgress = YES;
    [self unschedule:@selector(timerAfterThrow:)];
    [self schedule:@selector(timerAfterThrow:) interval:.6f];
    
    [self unschedule:@selector(followBallTick:)];
    [self schedule: @selector(followBallTick:) interval:0.02];
    
    if(ENABLE_AUDIO && ![[GameData sharedData]areAmbientFXMuted]){
        [[SimpleAudioEngine sharedEngine] playEffect:AUDIO_SPRING_GUN];
    }
    
    crossHairs.opacity = 0;
    
    
    
    //[self dotSetup];
}

-(void) timerAfterThrow:(ccTime) delta {
    
    //throwInProgress = NO;
    [self unschedule:_cmd];
}

-(void)createBullet:(CGPoint)p force:(b2Vec2)force
{
    
    
    Bullet* bullet = [[Bullet alloc] initInParent:self At:p inWorld:world];
    [bullet setDensity:self.currentDensity];
    [bullet setBallImage:currentBallImage];
    [bullet setRestitution:self.bulletRestitution];
    [bullet setFriction:self.bulletFriction];
    [bullet createBullet:p];
    [bulletsArray addObject:bullet];
    currentBullet = bullet;
    [self addChild:bullet z:0];
    [bullet applyBulletForce:force];

    //starEmitter.plist
    /*
#if ENABLE_EMITTERS
    if(!bulletEmitter)
    {
        bulletEmitter = [[CCParticleSystemQuad alloc]initWithFile:BULLET_PLIST];
        bulletEmitter.position = p;
        [self addChild:bulletEmitter];
    }
    else
    {
        bulletEmitter.position = p;
    }
#endif
     */
    
    [self unschedule:@selector(duringTick:)];
    [self schedule: @selector(duringTick:) interval:0.01];
    
    [self reduceBallCount];
    
}

-(void)duringTick: (ccTime) dt
{
    [self unschedule:@selector(duringTick:)];
    if(throwInProgress && currentBullet)
    {
        bulletEmitter.scale = .6;
        [bulletEmitter resetSystem];
        bulletEmitter.position = CGPointMake( currentBullet.getPointedBody->GetPosition().x * PTM_RATIO, currentBullet.getPointedBody->GetPosition().y * PTM_RATIO);
    }   
    
}

-(void)followBallTick: (ccTime) dt
{
    if(throwInProgress && currentBullet)
    {
        currentBallTick++;
        
        CCSprite* dot = [CCSprite spriteWithSpriteFrameName:IMAGE_DOT];
        
        if(currentBallTick %2){
            dot.scale = 0.25f;
        }
        else{
            dot.scale = 0.15f;
        }
        
        dot.scale = 0.15f;
        dot.opacity = 200;
        
        [self addChild:dot z:0 tag:Z_DOTS];
        
        dot.position = CGPointMake( currentBullet.getPointedBody->GetPosition().x * PTM_RATIO, currentBullet.getPointedBody->GetPosition().y * PTM_RATIO);
        //TODO CS there is a bug here, need to unschedule followBallTick
    }
}



#pragma mark ---- Utils

-(BOOL) checkCircleCollision:(CGPoint)center1  :(float)radius1  :(CGPoint) center2  :(float) radius2
{
    float a = center2.x - center1.x;
    float b = center2.y - center1.y;
    float c = radius1 + radius2;
    float distanceSqrd = (a * a) + (b * b);
    
    if (distanceSqrd < (c * c) ) {
        
        return YES;
    } else {
        return NO;
    }
    
}

- (GLfloat) calculateAngle:(GLfloat)x1 :(GLfloat)y1 :(GLfloat)x2 :(GLfloat)y2
{
    // DX
    GLfloat x = x2 - x1;
    
    // DY
    GLfloat y = y2 - y1;
    GLfloat angle = (GLfloat) (180 + (atan2(-x, -y) * (180/M_PI)));
    return angle;  //degrees
}

- (void)accelerometer:(UIAccelerometer*)accelerometer didAccelerate:(UIAcceleration*)acceleration{
    //not used atm
}

// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
    [self destroyAllBullets];

    [self unschedule:@selector(timerAfterThrow:)];    
    [self unschedule:@selector(followBallTick:)];
    
    [emitter release];
    emitter = nil;
    // in case you have something to dealloc, do it in this method
    delete world;
    world = NULL;
    
    delete m_debugDraw;
    
    if(rotatingArrowWarp)
    {
        rotatingArrowWarp = nil;
    }
    
    [bulletEmitter release];
    bulletEmitter = nil;
    
    [levelObjects release];
    levelObjects = nil;
    
    [bulletsArray release];
    bulletsArray = nil;
    
    [ballsMenu release];
    ballsMenu = nil;
    
    [mrPants release];
    mrPants = nil;
    
    delete _contactListener;
    
    AppController *app = (AppController*) [[UIApplication sharedApplication] delegate];
    app.currentLayer = nil;
    
    // don't forget to call "super dealloc"
    [super dealloc];
}



-(void)createLevels
{
    [self unschedule:@selector(createOutOfBallsMenu:)];

    [self startLevelTimer];
    
    currentSection = 2;
    currentPlatformName = PLATFORM_GROUND;
    canChangePlatform = YES;
    
    int level = [[GameData sharedData] returnLevel];
    
    canPlayBounceSound = YES;
    
    SimpleAudioEngine *sharedEngine = [SimpleAudioEngine sharedEngine];
    
    if(ENABLE_MUSIC && ![[GameData sharedData]areSoundFXMuted]){
        
       
        [[GameData sharedData] startLevel_1_Loop];
        /*
        if(![sharedEngine isBackgroundMusicPlaying]){
            
            //[sharedEngine stopBackgroundMusic];
            
            [sharedEngine playBackgroundMusic:LOOP_SECTION_01 loop:YES];
            //[sharedEngine playBackgroundMusic:LOOP_CITY_TRAFFIC loop:YES];
            
            //[sharedEngine playEffect:LOOP_CITY_TRAFFIC];
        }
         */
        
    }
    
    [sharedEngine setBackgroundMusicVolume:0.6];
    [sharedEngine setEffectsVolume:0.5];
    
    switch (level) {
        case 0:
            
            break;
        case 1:
            //[self createTestLevel];
            [self createLevel1];
            break;
        case 2:
            [self createLevel2];
            break;
        case 3:
            [self createLevel3];
            break;
        case 4:
            [self createLevel4];
            break;
        case 5:
            [self createLevel5];
            break;
        case 6:
            [self createLevel6];
            break;
        case 7:
            [self createLevel7];
            break;
        case 8:
            [self createLevel8];
            break;
        case 9:
            [self createLevel9];
            break;
        case 10:
            [self createLevel10];
            break;
        case 11:
            [self createLevel11];
            break;
        case 12:
            [self createLevel12];
            break;
        case 13:
            [self createLevel13];
            break;
        case 14:
            [self createLevel14];
            break;
        case 15:
            [self createLevel15];
            break;
        case 16:
            [self createLevel16];
            break;
        case 17:
            [self createLevel17];
            break;
        case 18:
            [self createLevel18];
            break;
        case 19:
            [self createLevel19];
            break;
        case 20:
            [self createLevel20];
            break;
        case 21:
            [self createLevel21];
            break;
        case 22:
            [self createLevel22];
            break;
        case 23:
            [self createLevel23];
            break;
        case 24:
            [self createLevel24];
            break;
        default:
            [self createLevel1];
            break;
    }

    //Level 1
}



-(void)catExplodedAtPosition:(CGPoint)pos
{
    
    BreakableBlock *block2 = [[BreakableBlock alloc] initInParent:self At:pos rotateTo:0.0f withBlockType:WARP_02 inWorld:world];
    [self addChild:block2];
    [block2 release];
    
    if(isFirstLevel){
        CCSprite *wormhole = [CCSprite spriteWithSpriteFrameName:IMAGE_WORMHOLE];
        [self addChild:wormhole];
        
        CCSprite *ballExit = [CCSprite spriteWithSpriteFrameName:IMAGE_BALL_EXIT];
        [self addChild:ballExit];
        
        
        
        if(IS_IPAD){
            wormhole.position = dccp(190,195);
            ballExit.position = dccp(402,95);
        }else{
            wormhole.position = ccp(190,195);
            ballExit.position = ccp(402,95);
        }
        
        if(slidePaw){
            [self removeChild:slidePaw cleanup:YES];
        }
        if(easierShot){
            [self removeChild:easierShot cleanup:YES];
            easierShot = nil;
        }
    }
}


-(void)createLevel1
{
    currentSection = 1;
    if(currentSection == 1){
        isFirstLevel = YES;
        
        slidePaw = [CCSprite spriteWithSpriteFrameName:IMAGE_SLIDE_PAW];
        easierShot = [CCSprite spriteWithSpriteFrameName:IMAGE_EASIER_SHOT];
        [self addChild:slidePaw];
        [self addChild:easierShot];
        
        int xPos = 430;
        CGSize size = [[CCDirector sharedDirector] winSize];
        if(size.width > 480 || size.height > 480)
        {
            xPos = 480;
        }
        
        if(IS_IPAD){
            slidePaw.position = dccp(465, 130);
            easierShot.position = dccp(150,175);
        }else{
            slidePaw.position = dccp(xPos, 130);
            easierShot.position = dccp(150,175);
        }
        
        //IMAGE_SLIDE_PAW
        
        ExplodingCat1 *catNumber1 = [[ExplodingCat1 alloc] initInParent:self At:dccp(10.375, 1.5) rotateTo:0.0f inWorld:world catType:CAT_01];
        [self addChild:catNumber1];
        [catNumber1 release];
        
        ExplodingCat1 *catNumber2 = [[ExplodingCat1 alloc] initInParent:self At:dccp(8.875, 5.71875) rotateTo:0.0f inWorld:world catType:CAT_03];
        [self addChild:catNumber2];
        catNumber2.delegate = self;
        [catNumber2 release];
        
        rotatingArrowWarp2 = [CCSprite spriteWithSpriteFrameName:IMAGE_SELECT_BACK_BTN];
        [self addChild:rotatingArrowWarp2 z:200];
        rotatingArrowWarp2.position = dccp(334,106);
        rotatingArrowWarp2.rotation = -270;
        numberOfCatsThisLevel = 2;
    }else if(currentSection == 2){
        ExplodingCat1 *catNumber1 = [[ExplodingCat1 alloc] initInParent:self At:dccp(8.59375, 1.3125) rotateTo:0.0f inWorld:world catType:CAT_01];
        [self addChild:catNumber1];
        [catNumber1 release];
        
        BreakableBlock *block2 = [[BreakableBlock alloc] initInParent:self At:dccp(7.15625, 4.09375) rotateTo:0.0f withBlockType:BARREL inWorld:world];
        [self addChild:block2];
        [block2 release];
        
        BreakableBlock *block3 = [[BreakableBlock alloc] initInParent:self At:dccp(12.03125, 6.15625) rotateTo:0.0f withBlockType:BARREL inWorld:world];
        [self addChild:block3];
        [block3 release];
        
        ExplodingCat1 *catNumber4 = [[ExplodingCat1 alloc] initInParent:self At:dccp(12.03125, 6.875) rotateTo:0.0f inWorld:world catType:CAT_01];
        [self addChild:catNumber4];
        [catNumber4 release];
        
        ExplodingCat1 *catNumber5 = [[ExplodingCat1 alloc] initInParent:self At:dccp(4.8125, 2.375) rotateTo:0.0f inWorld:world catType:CAT_01];
        [self addChild:catNumber5];
        [catNumber5 release];
        
        numberOfCatsThisLevel = 3;
    }
}

-(void)createLevel2
{
    ExplodingCat1 *catNumber1 = [[ExplodingCat1 alloc] initInParent:self At:dccp(11.3125, 1.5) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber1];
    [catNumber1 release];
    
    ExplodingCat1 *catNumber2 = [[ExplodingCat1 alloc] initInParent:self At:dccp(8.5625, 1.625) rotateTo:0.0f inWorld:world catType:CAT_03];
    [self addChild:catNumber2];
    catNumber2.delegate = self;
    [catNumber2 release];
    
    rotatingArrowWarp2 = [CCSprite spriteWithSpriteFrameName:IMAGE_SELECT_BACK_BTN];
    [self addChild:rotatingArrowWarp2 z:200];
    rotatingArrowWarp2.position = dccp(363,106);
    rotatingArrowWarp2.rotation = -270;
    ExplodingCat1 *catNumber4 = [[ExplodingCat1 alloc] initInParent:self At:dccp(9.9375, 6.34375) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber4];
    [catNumber4 release];
    
    numberOfCatsThisLevel = 3;
    
    shouldRotateWarp2 = YES;
}

-(void)createLevel3{
    
    ExplodingCat1 *catNumber1 = [[ExplodingCat1 alloc] initInParent:self At:dccp(11.1875, 0.875) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber1];
    [catNumber1 release];
    
    ExplodingCat1 *catNumber2 = [[ExplodingCat1 alloc] initInParent:self At:dccp(8.9375, 1.625) rotateTo:0.0f inWorld:world catType:CAT_03];
    [self addChild:catNumber2];
    catNumber2.delegate = self;
    [catNumber2 release];
    
    rotatingArrowWarp2 = [CCSprite spriteWithSpriteFrameName:IMAGE_SELECT_BACK_BTN];
    [self addChild:rotatingArrowWarp2 z:200];
    rotatingArrowWarp2.position = dccp(352,127);
    rotatingArrowWarp2.rotation = -270;
    ExplodingCat1 *catNumber4 = [[ExplodingCat1 alloc] initInParent:self At:dccp(9.125, 4.875) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber4];
    [catNumber4 release];
    
    ExplodingCat1 *catNumber5 = [[ExplodingCat1 alloc] initInParent:self At:dccp(11.9375, 5.65625) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber5];
    [catNumber5 release];
    
    ExplodingCat1 *catNumber6 = [[ExplodingCat1 alloc] initInParent:self At:dccp(12.4375, 0.875) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber6];
    [catNumber6 release];
    
    numberOfCatsThisLevel = 5;
        
    shouldRotateWarp2 = YES;
    
    /*
    CCSprite *pipe = [CCSprite spriteWithSpriteFrameName:IMAGE_GROUND_LEVEL_3_PIPE];
    [self addChild:pipe z:Z_PIPE];
    pipe.anchorPoint = ccp(0,0);
    pipe.position = dccp(411/2, 193/2);
     */
}

-(void)createLevel4{
    
    ExplodingCat1 *catNumber1 = [[ExplodingCat1 alloc] initInParent:self At:dccp(11.125, 2.625) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber1];
    [catNumber1 release];
    
    ExplodingCat1 *catNumber2 = [[ExplodingCat1 alloc] initInParent:self At:dccp(8.5625, 1.34375) rotateTo:0.0f inWorld:world catType:CAT_03];
    [self addChild:catNumber2];
    catNumber2.delegate = self;
    [catNumber2 release];
    
    rotatingArrowWarp2 = [CCSprite spriteWithSpriteFrameName:IMAGE_SELECT_BACK_BTN];
    [self addChild:rotatingArrowWarp2 z:200];
    rotatingArrowWarp2.position = dccp(355,115);
    rotatingArrowWarp2.rotation = -270;
    ExplodingCat1 *catNumber4 = [[ExplodingCat1 alloc] initInParent:self At:dccp(10.3125, 4.9375) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber4];
    [catNumber4 release];
    
    NonBreakableCircle *block5 = [[NonBreakableCircle alloc] initInParent:self At:dccp(7.34375, 5.28125) rotateTo:0.0f withBlockType:WHEEL inWorld:world];
    [self addChild:block5];
    [block5 release];
    
    Plank01 *plank6 = [[Plank01 alloc] initInParent:self At:dccp(7.28125, 4.6875) rotateTo:0.0f plankType:2 inWorld:world];
    [self addChild:plank6];
    [plank6 release];
    
    ExplodingCat1 *catNumber7 = [[ExplodingCat1 alloc] initInParent:self At:dccp(11.4375, 7.0625) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber7];
    [catNumber7 release];
    
    ExplodingCat1 *catNumber8 = [[ExplodingCat1 alloc] initInParent:self At:dccp(5.125, 0.9375) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber8];
    [catNumber8 release];
    
    numberOfCatsThisLevel = 5;
    
    shouldRotateWarp2 = YES;
}

-(void)createLevel5{
    
    
    ExplodingCat1 *catNumber1 = [[ExplodingCat1 alloc] initInParent:self At:dccp(10.8125, 0.71875) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber1];
    [catNumber1 release];
    
    ExplodingCat1 *catNumber2 = [[ExplodingCat1 alloc] initInParent:self At:dccp(9.625, 6.84375) rotateTo:0.0f inWorld:world catType:CAT_03];
    [self addChild:catNumber2];
    catNumber2.delegate = self;
    [catNumber2 release];
    
    rotatingArrowWarp2 = [CCSprite spriteWithSpriteFrameName:IMAGE_SELECT_BACK_BTN];
    [self addChild:rotatingArrowWarp2 z:200];
    rotatingArrowWarp2.position = dccp(241,122);
    rotatingArrowWarp2.rotation = -180;
    ExplodingCat1 *catNumber4 = [[ExplodingCat1 alloc] initInParent:self At:dccp(6.71875, 1.15625) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber4];
    [catNumber4 release];
    
    ExplodingCat1 *catNumber5 = [[ExplodingCat1 alloc] initInParent:self At:dccp(8.34375, 1.15625) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber5];
    [catNumber5 release];
    
    BreakableBlock *block6 = [[BreakableBlock alloc] initInParent:self At:dccp(9.625, 5.34375) rotateTo:0.0f withBlockType:BARREL inWorld:world];
    [self addChild:block6];
    [block6 release];
    
    BreakableBlock *block7 = [[BreakableBlock alloc] initInParent:self At:dccp(9.625, 6.09375) rotateTo:0.0f withBlockType:BARREL inWorld:world];
    [self addChild:block7];
    [block7 release];
    
    BreakableBlock *block8 = [[BreakableBlock alloc] initInParent:self At:dccp(7.5, 5.375) rotateTo:0.0f withBlockType:TV inWorld:world];
    [self addChild:block8];
    [block8 release];
    
    BreakableBlock *block9 = [[BreakableBlock alloc] initInParent:self At:dccp(7.5, 6.21875) rotateTo:0.0f withBlockType:TV inWorld:world];
    [self addChild:block9];
    [block9 release];
    
    BreakableBlock *block10 = [[BreakableBlock alloc] initInParent:self At:dccp(7.5, 7.0625) rotateTo:0.0f withBlockType:TV inWorld:world];
    [self addChild:block10];
    [block10 release];
    
    numberOfCatsThisLevel = 4;
    
    shouldRotateWarp2 = YES;
}

-(void)createLevel6{
    
    ExplodingCat1 *catNumber1 = [[ExplodingCat1 alloc] initInParent:self At:dccp(11.28125, 4.5625) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber1];
    [catNumber1 release];
    
    ExplodingCat1 *catNumber2 = [[ExplodingCat1 alloc] initInParent:self At:dccp(5.5625, 4.5625) rotateTo:0.0f inWorld:world catType:CAT_03];
    [self addChild:catNumber2];
    catNumber2.delegate = self;
    [catNumber2 release];
    
    rotatingArrowWarp2 = [CCSprite spriteWithSpriteFrameName:IMAGE_SELECT_BACK_BTN];
    [self addChild:rotatingArrowWarp2 z:200];
    rotatingArrowWarp2.position = dccp(282,158);
    rotatingArrowWarp2.rotation = -180;
    ExplodingCat1 *catNumber4 = [[ExplodingCat1 alloc] initInParent:self At:dccp(7.1875, 6.6875) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber4];
    [catNumber4 release];
    
    ExplodingCat1 *catNumber5 = [[ExplodingCat1 alloc] initInParent:self At:dccp(8.65625, 1.21875) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber5];
    [catNumber5 release];
    
    ExplodingCat1 *catNumber6 = [[ExplodingCat1 alloc] initInParent:self At:dccp(7.65625, 1.21875) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber6];
    [catNumber6 release];
    
    ExplodingCat1 *catNumber7 = [[ExplodingCat1 alloc] initInParent:self At:dccp(10.9375, 0.90625) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber7];
    [catNumber7 release];
    
    ExplodingCat1 *catNumber8 = [[ExplodingCat1 alloc] initInParent:self At:dccp(9.0625, 6.6875) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber8];
    [catNumber8 release];
    
    rotatingArrowWarp = [CCSprite spriteWithSpriteFrameName:IMAGE_SELECT_BACK_BTN];
    [self addChild:rotatingArrowWarp z:200];
    rotatingArrowWarp.position = dccp(246,100);
    rotatingArrowWarp.rotation = -180;
    BreakableBlock *block10 = [[BreakableBlock alloc] initInParent:self At:dccp(7.59375, 4.9375) rotateTo:0.0f withBlockType:WARP_01 inWorld:world];
    [self addChild:block10];
    [block10 release];
    
    numberOfCatsThisLevel = 7;

    shouldRotateWarp2 = NO;
    shouldRotateWarp1 = YES;
}

-(void)createLevel7
{
    ExplodingCat1 *catNumber1 = [[ExplodingCat1 alloc] initInParent:self At:dccp(10.3125, 4.5625) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber1];
    [catNumber1 release];
    
    ExplodingCat1 *catNumber2 = [[ExplodingCat1 alloc] initInParent:self At:dccp(10.71875, 2.78125) rotateTo:0.0f inWorld:world catType:CAT_03];
    [self addChild:catNumber2];
    catNumber2.delegate = self;
    [catNumber2 release];
    
    rotatingArrowWarp2 = [CCSprite spriteWithSpriteFrameName:IMAGE_SELECT_BACK_BTN];
    [self addChild:rotatingArrowWarp2 z:200];
    rotatingArrowWarp2.position = dccp(301,99);
    rotatingArrowWarp2.rotation = -180;
    ExplodingCat1 *catNumber4 = [[ExplodingCat1 alloc] initInParent:self At:dccp(7.125, 4.5625) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber4];
    [catNumber4 release];
    
    ExplodingCat1 *catNumber5 = [[ExplodingCat1 alloc] initInParent:self At:dccp(7.9375, 2.34375) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber5];
    [catNumber5 release];
    
    numberOfCatsThisLevel = 4;
    shouldRotateWarp2 = YES;
}

-(void) createLevel8
{
    ExplodingCat1 *catNumber2 = [[ExplodingCat1 alloc] initInParent:self At:dccp(11, 2.125) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber2];
    [catNumber2 release];
    
    ExplodingCat1 *catNumber3 = [[ExplodingCat1 alloc] initInParent:self At:dccp(6.375, 1.21875) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber3];
    [catNumber3 release];
    
    ExplodingCat1 *catNumber4 = [[ExplodingCat1 alloc] initInParent:self At:dccp(8.5625, 1.71875) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber4];
    [catNumber4 release];
    
    Plank01 *plank5 = [[Plank01 alloc] initInParent:self At:dccp(7.375, 5.0625) rotateTo:1.5707963267948966 plankType:2 inWorld:world];
    [self addChild:plank5];
    [plank5 release];
    
    Plank01 *plank6 = [[Plank01 alloc] initInParent:self At:dccp(9.9375, 5.0625) rotateTo:1.5707963267948966 plankType:2 inWorld:world];
    [self addChild:plank6];
    [plank6 release];
    
    Plank01 *plank7 = [[Plank01 alloc] initInParent:self At:dccp(6.1875, 5.625) rotateTo:1.047211133318157 plankType:2 inWorld:world];
    [self addChild:plank7];
    [plank7 release];
    
    Plank01 *plank8 = [[Plank01 alloc] initInParent:self At:dccp(8.71875, 6.8125) rotateTo:0.0f plankType:2 inWorld:world];
    [self addChild:plank8];
    [plank8 release];
    
    Plank01 *plank9 = [[Plank01 alloc] initInParent:self At:dccp(11.21875, 5.625) rotateTo:2.094381520271636 plankType:2 inWorld:world];
    [self addChild:plank9];
    [plank9 release];
    
    numberOfCatsThisLevel = 3;
}

-(void)createLevel9
{
    ExplodingCat1 *catNumber1 = [[ExplodingCat1 alloc] initInParent:self At:dccp(10.03125, 3.40625) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber1];
    [catNumber1 release];
    
    ExplodingCat1 *catNumber2 = [[ExplodingCat1 alloc] initInParent:self At:dccp(4.71875, 4.3125) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber2];
    [catNumber2 release];
    
    ExplodingCat1 *catNumber3 = [[ExplodingCat1 alloc] initInParent:self At:dccp(10.75, 5.375) rotateTo:0.5235851934767395 inWorld:world catType:CAT_01];
    [self addChild:catNumber3];
    [catNumber3 release];
    
    Plank01 *plank4 = [[Plank01 alloc] initInParent:self At:dccp(10.875, 4.46875) rotateTo:2.094381520271636 plankType:2 inWorld:world];
    [self addChild:plank4];
    [plank4 release];
    
    ExplodingCat1 *catNumber5 = [[ExplodingCat1 alloc] initInParent:self At:dccp(11.375, 5.65625) rotateTo:0.5235851934767395 inWorld:world catType:CAT_03];
    [self addChild:catNumber5];
    catNumber5.delegate = self;
    [catNumber5 release];
    
    rotatingArrowWarp2 = [CCSprite spriteWithSpriteFrameName:IMAGE_SELECT_BACK_BTN];
    [self addChild:rotatingArrowWarp2 z:200];
    rotatingArrowWarp2.position = dccp(278,57);
    rotatingArrowWarp2.rotation = -14.99810791015625;
    BreakableBlock *block7 = [[BreakableBlock alloc] initInParent:self At:dccp(11.3125, 1.8125) rotateTo:0.0f withBlockType:WARP_01 inWorld:world];
    [self addChild:block7];
    [block7 release];
    
    rotatingArrowWarp = [CCSprite spriteWithSpriteFrameName:IMAGE_WARP_EXIT_1];
    [self addChild:rotatingArrowWarp z:200];
    rotatingArrowWarp.position = dccp(381,134);
    rotatingArrowWarp.rotation = -180;
    ExplodingCat1 *catNumber9 = [[ExplodingCat1 alloc] initInParent:self At:dccp(12.03125, 5.90625) rotateTo:0.5235851934767395 inWorld:world catType:CAT_01];
    [self addChild:catNumber9];
    [catNumber9 release];
    
    ExplodingCat1 *catNumber10 = [[ExplodingCat1 alloc] initInParent:self At:dccp(3.84375, 0.78125) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber10];
    [catNumber10 release];
    
    numberOfCatsThisLevel = 6;
    
    shouldRotateWarp1 = YES;
    shouldRotateWarp2 = NO;
}

-(void)createLevel10{
    ExplodingCat1 *catNumber1 = [[ExplodingCat1 alloc] initInParent:self At:dccp(11.125, 0.875) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber1];
    [catNumber1 release];
    
    ExplodingCat1 *catNumber2 = [[ExplodingCat1 alloc] initInParent:self At:dccp(10.34375, 5.34375) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber2];
    [catNumber2 release];
    
    Plank01 *plank3 = [[Plank01 alloc] initInParent:self At:dccp(8.875, 4.875) rotateTo:0.0f plankType:2 inWorld:world];
    [self addChild:plank3];
    [plank3 release];
    
    ExplodingCat1 *catNumber4 = [[ExplodingCat1 alloc] initInParent:self At:dccp(8.875, 1.28125) rotateTo:0.0f inWorld:world catType:CAT_03];
    [self addChild:catNumber4];
    catNumber4.delegate = self;
    [catNumber4 release];
    
    rotatingArrowWarp2 = [CCSprite spriteWithSpriteFrameName:IMAGE_SELECT_BACK_BTN];
    [self addChild:rotatingArrowWarp2 z:200];
    rotatingArrowWarp2.position = dccp(355,98);
    rotatingArrowWarp2.rotation = -14.99810791015625;
    ExplodingCat1 *catNumber6 = [[ExplodingCat1 alloc] initInParent:self At:dccp(7.53125, 5.34375) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber6];
    [catNumber6 release];
    
    
    NonBreakableCircle *block10 = [[NonBreakableCircle alloc] initInParent:self At:dccp(8.90625, 5.375) rotateTo:0.0f withBlockType:WHEEL inWorld:world];
    [self addChild:block10];
    [block10 release];
    
    numberOfCatsThisLevel = 4;
    
    shouldRotateWarp2 = YES;
}

-(void)createLevel11{
    ExplodingCat1 *catNumber1 = [[ExplodingCat1 alloc] initInParent:self At:dccp(12.0625, 4.03125) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber1];
    [catNumber1 release];
    
    ExplodingCat1 *catNumber2 = [[ExplodingCat1 alloc] initInParent:self At:dccp(7.6875, 1.28125) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber2];
    [catNumber2 release];
    
    ExplodingCat1 *catNumber3 = [[ExplodingCat1 alloc] initInParent:self At:dccp(12.03125, 1.28125) rotateTo:0.0f inWorld:world catType:CAT_03];
    [self addChild:catNumber3];
    catNumber3.delegate = self;
    [catNumber3 release];
    
    rotatingArrowWarp2 = [CCSprite spriteWithSpriteFrameName:IMAGE_SELECT_BACK_BTN];
    [self addChild:rotatingArrowWarp2 z:200];
    rotatingArrowWarp2.position = dccp(325,49);
    rotatingArrowWarp2.rotation = -14.99810791015625;
    ExplodingCat1 *catNumber5 = [[ExplodingCat1 alloc] initInParent:self At:dccp(6.3125, 1.25) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber5];
    [catNumber5 release];
    
    
    b2BodyDef bodyDef6;
    bodyDef6.type = b2_dynamicBody;
    CCSprite *boxSprite6 = [CCSprite spriteWithSpriteFrameName:@"large-wheel.png"];
    [self addChild:boxSprite6 z:0 tag:TAG_NON_BREAKABLE_OBJECT];
    float yPos6 = 4.03125;
    float xPos6 = 7.1875;
    if(IS_IPAD){
        yPos6 *= 2;
        xPos6 *= 2;
    }
    
    bodyDef6.userData = boxSprite6;
    b2Body *body6 = world->CreateBody(&bodyDef6);
    body6->SetTransform(b2Vec2(xPos6, yPos6),0);
    b2CircleShape shape6;
    float radiusInMeters = 0.4375f;
    if(IS_IPAD){
        radiusInMeters *= 2;
    }
    shape6.m_radius = radiusInMeters;
    b2FixtureDef fixtureDef6;
    fixtureDef6.shape = &shape6;
    fixtureDef6.filter.groupIndex = COLLIDE_GENERIC_ALWAYS_COLLIDE;
    fixtureDef6.density = 4.0f;
    fixtureDef6.friction = 1.0f;
    fixtureDef6.restitution = 0.6f;
    body6->CreateFixture(&fixtureDef6);
    
    ExplodingCat1 *catNumber7 = [[ExplodingCat1 alloc] initInParent:self At:dccp(8.96875, 1.28125) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber7];
    [catNumber7 release];
    
    numberOfCatsThisLevel = 5;
    
    shouldRotateWarp2 = YES;
}

-(void)createLevel12{
    ExplodingCat1 *catNumber1 = [[ExplodingCat1 alloc] initInParent:self At:dccp(12.0625, 4.03125) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber1];
    [catNumber1 release];
    
    ExplodingCat1 *catNumber2 = [[ExplodingCat1 alloc] initInParent:self At:dccp(7.59375, 4.09375) rotateTo:0.0f inWorld:world catType:CAT_03];
    [self addChild:catNumber2];
    catNumber2.delegate = self;
    [catNumber2 release];
    
    rotatingArrowWarp2 = [CCSprite spriteWithSpriteFrameName:IMAGE_SELECT_BACK_BTN];
    [self addChild:rotatingArrowWarp2 z:200];
    rotatingArrowWarp2.position = dccp(381,89);
    rotatingArrowWarp2.rotation = -179.99913024902344;
    ExplodingCat1 *catNumber4 = [[ExplodingCat1 alloc] initInParent:self At:dccp(12.625, 1.09375) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber4];
    [catNumber4 release];
    
    BreakableBlock *block5 = [[BreakableBlock alloc] initInParent:self At:dccp(8.09375, 2.8125) rotateTo:0.0f withBlockType:WARP_01 inWorld:world];
    [self addChild:block5];
    [block5 release];
    
    rotatingArrowWarp = [CCSprite spriteWithSpriteFrameName:IMAGE_WARP_EXIT_1];
    [self addChild:rotatingArrowWarp z:200];
    rotatingArrowWarp.position = dccp(259,42);
    rotatingArrowWarp.rotation = -180;
    numberOfCatsThisLevel = 3;
    
    shouldRotateWarp1 = YES;
}

-(void)createLevel13{
    ExplodingCat1 *catNumber1 = [[ExplodingCat1 alloc] initInParent:self At:dccp(7.8125, 1.53125) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber1];
    [catNumber1 release];
    
    ExplodingCat1 *catNumber2 = [[ExplodingCat1 alloc] initInParent:self At:dccp(9.65625, 1.125) rotateTo:0.0f inWorld:world catType:CAT_03];
    [self addChild:catNumber2];
    catNumber2.delegate = self;
    [catNumber2 release];
    
    rotatingArrowWarp2 = [CCSprite spriteWithSpriteFrameName:IMAGE_SELECT_BACK_BTN];
    [self addChild:rotatingArrowWarp2 z:200];
    rotatingArrowWarp2.position = dccp(409,66);
    rotatingArrowWarp2.rotation = -179.99913024902344;
    ExplodingCat1 *catNumber4 = [[ExplodingCat1 alloc] initInParent:self At:dccp(11.625, 1.09375) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber4];
    [catNumber4 release];
    
    ExplodingCat1 *catNumber5 = [[ExplodingCat1 alloc] initInParent:self At:dccp(11.84375, 4.84375) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber5];
    [catNumber5 release];
    
    NonBreakableCircle *block6 = [[NonBreakableCircle alloc] initInParent:self At:dccp(10.65625, 3.5) rotateTo:0.0f withBlockType:WHEEL inWorld:world];
    [self addChild:block6];
    [block6 release];
    
    NonBreakableCircle *block7 = [[NonBreakableCircle alloc] initInParent:self At:dccp(6.4375, 3.3125) rotateTo:0.0f withBlockType:WHEEL inWorld:world];
    [self addChild:block7];
    [block7 release];
    
    Plank01 *plank8 = [[Plank01 alloc] initInParent:self At:dccp(6.75, 2.1875) rotateTo:1.2814939417862807 plankType:2 inWorld:world];
    [self addChild:plank8];
    [plank8 release];
    
    ExplodingCat1 *catNumber9 = [[ExplodingCat1 alloc] initInParent:self At:dccp(5.28125, 1) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber9];
    [catNumber9 release];
    
    numberOfCatsThisLevel = 5;
    
    
    shouldRotateWarp2 = YES;
}

-(void)createLevel14{
    ExplodingCat1 *catNumber1 = [[ExplodingCat1 alloc] initInParent:self At:dccp(5.4375, 4.40625) rotateTo:0.0f inWorld:world catType:CAT_03];
    [self addChild:catNumber1];
    catNumber1.delegate = self;
    [catNumber1 release];
    
    rotatingArrowWarp2 = [CCSprite spriteWithSpriteFrameName:IMAGE_SELECT_BACK_BTN];
    [self addChild:rotatingArrowWarp2 z:200];
    rotatingArrowWarp2.position = dccp(223,54);
    rotatingArrowWarp2.rotation = -359.99913024902344;
    ExplodingCat1 *catNumber3 = [[ExplodingCat1 alloc] initInParent:self At:dccp(11.75, 1.375) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber3];
    [catNumber3 release];
    
    ExplodingCat1 *catNumber4 = [[ExplodingCat1 alloc] initInParent:self At:dccp(11.65625, 3.9375) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber4];
    [catNumber4 release];
    
    numberOfCatsThisLevel = 3;
    
    movingArrowWarp = YES;
}

-(void)createLevel15{
    ExplodingCat1 *catNumber1 = [[ExplodingCat1 alloc] initInParent:self At:dccp(7.375, 4.1875) rotateTo:0.0f inWorld:world catType:CAT_03];
    [self addChild:catNumber1];
    catNumber1.delegate = self;
    [catNumber1 release];
    
    rotatingArrowWarp2 = [CCSprite spriteWithSpriteFrameName:IMAGE_SELECT_BACK_BTN];
    [self addChild:rotatingArrowWarp2 z:200];
    rotatingArrowWarp2.position = dccp(404,54);
    rotatingArrowWarp2.rotation = -359.99913024902344;
    ExplodingCat1 *catNumber3 = [[ExplodingCat1 alloc] initInParent:self At:dccp(6.96875, 2.5) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber3];
    [catNumber3 release];
    
    ExplodingCat1 *catNumber4 = [[ExplodingCat1 alloc] initInParent:self At:dccp(8.8125, 1.375) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber4];
    [catNumber4 release];
    
    ExplodingCat1 *catNumber5 = [[ExplodingCat1 alloc] initInParent:self At:dccp(10.25, 4.09375) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber5];
    [catNumber5 release];
    
    numberOfCatsThisLevel = 4;
    
    movingArrowWarp = YES;
    shouldRotateWarp2 = YES;
}

-(void)createLevel16{
    ExplodingCat1 *catNumber1 = [[ExplodingCat1 alloc] initInParent:self At:dccp(6.15625, 2.5625) rotateTo:0.0f inWorld:world catType:CAT_03];
    [self addChild:catNumber1];
    catNumber1.delegate = self;
    [catNumber1 release];
    
    rotatingArrowWarp2 = [CCSprite spriteWithSpriteFrameName:IMAGE_SELECT_BACK_BTN];
    [self addChild:rotatingArrowWarp2 z:200];
    rotatingArrowWarp2.position = dccp(214,164);
    rotatingArrowWarp2.rotation = -314.9982452392578;
    ExplodingCat1 *catNumber3 = [[ExplodingCat1 alloc] initInParent:self At:dccp(8.3125, 1.40625) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber3];
    [catNumber3 release];
    
    ExplodingCat1 *catNumber4 = [[ExplodingCat1 alloc] initInParent:self At:dccp(9.875, 1.40625) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber4];
    [catNumber4 release];
    
    numberOfCatsThisLevel = 3;
    
    movingArrowWarpX = YES;
    //shouldRotateWarp2 = YES;
}

-(void)createLevel17{
    ExplodingCat1 *catNumber1 = [[ExplodingCat1 alloc] initInParent:self At:dccp(6.625, 1.25) rotateTo:0.0f inWorld:world catType:CAT_03];
    [self addChild:catNumber1];
    catNumber1.delegate = self;
    [catNumber1 release];
    
    rotatingArrowWarp2 = [CCSprite spriteWithSpriteFrameName:IMAGE_SELECT_BACK_BTN];
    [self addChild:rotatingArrowWarp2 z:200];
    rotatingArrowWarp2.position = dccp(292,173);
    rotatingArrowWarp2.rotation = -269.9982452392578;
    ExplodingCat1 *catNumber3 = [[ExplodingCat1 alloc] initInParent:self At:dccp(9.6875, 6.875) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber3];
    [catNumber3 release];
    
    ExplodingCat1 *catNumber4 = [[ExplodingCat1 alloc] initInParent:self At:dccp(11.25, 6.875) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber4];
    [catNumber4 release];
    
    BreakableBlock *block5 = [[BreakableBlock alloc] initInParent:self At:dccp(9.1875, 1.25) rotateTo:0.0f withBlockType:WARP_01 inWorld:world];
    [self addChild:block5];
    [block5 release];
    
    rotatingArrowWarp = [CCSprite spriteWithSpriteFrameName:IMAGE_WARP_EXIT_1];
    [self addChild:rotatingArrowWarp z:200];
    rotatingArrowWarp.position = dccp(351,174);
    rotatingArrowWarp.rotation = -180;
    ExplodingCat1 *catNumber7 = [[ExplodingCat1 alloc] initInParent:self At:dccp(11.0625, 1.09375) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber7];
    [catNumber7 release];
    
    Plank01 *plank8 = [[Plank01 alloc] initInParent:self At:dccp(6.21875, 4.6875) rotateTo:0.0f plankType:2 inWorld:world];
    [self addChild:plank8];
    [plank8 release];
    
    ExplodingCat1 *catNumber9 = [[ExplodingCat1 alloc] initInParent:self At:dccp(5.21875, 5.125) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber9];
    [catNumber9 release];
    
    NonBreakableCircle *block10 = [[NonBreakableCircle alloc] initInParent:self At:dccp(6.1875, 5.03125) rotateTo:0.0f withBlockType:ROCK inWorld:world];
    [self addChild:block10];
    [block10 release];
    
    Plank01 *plank11 = [[Plank01 alloc] initInParent:self At:dccp(7.625, 3.71875) rotateTo:-1.5707963267948966 plankType:3 inWorld:world];
    [self addChild:plank11];
    [plank11 release];
    
    Plank01 *plank12 = [[Plank01 alloc] initInParent:self At:dccp(5.8125, 3.71875) rotateTo:-1.5707963267948966 plankType:3 inWorld:world];
    [self addChild:plank12];
    [plank12 release];
    
    Plank01 *plank13 = [[Plank01 alloc] initInParent:self At:dccp(6.21875, 2.78125) rotateTo:0.0f plankType:2 inWorld:world];
    [self addChild:plank13];
    [plank13 release];
    
    ExplodingCat1 *catNumber14 = [[ExplodingCat1 alloc] initInParent:self At:dccp(6.65625, 3.28125) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber14];
    [catNumber14 release];
    
    NonBreakableCircle *block15 = [[NonBreakableCircle alloc] initInParent:self At:dccp(6.1875, 5.34375) rotateTo:0.0f withBlockType:ROCK inWorld:world];
    [self addChild:block15];
    [block15 release];
    
    NonBreakableCircle *block16 = [[NonBreakableCircle alloc] initInParent:self At:dccp(6.1875, 5.65625) rotateTo:0.0f withBlockType:ROCK inWorld:world];
    [self addChild:block16];
    [block16 release];
    
    NonBreakableCircle *block17 = [[NonBreakableCircle alloc] initInParent:self At:dccp(6.1875, 6) rotateTo:0.0f withBlockType:ROCK inWorld:world];
    [self addChild:block17];
    [block17 release];
    
    NonBreakableCircle *block18 = [[NonBreakableCircle alloc] initInParent:self At:dccp(6.1875, 6.34375) rotateTo:0.0f withBlockType:ROCK inWorld:world];
    [self addChild:block18];
    [block18 release];
    
    NonBreakableCircle *block19 = [[NonBreakableCircle alloc] initInParent:self At:dccp(6.5, 5.03125) rotateTo:0.0f withBlockType:ROCK inWorld:world];
    [self addChild:block19];
    [block19 release];
    
    NonBreakableCircle *block20 = [[NonBreakableCircle alloc] initInParent:self At:dccp(6.5, 5.34375) rotateTo:0.0f withBlockType:ROCK inWorld:world];
    [self addChild:block20];
    [block20 release];
    
    NonBreakableCircle *block21 = [[NonBreakableCircle alloc] initInParent:self At:dccp(6.5, 5.65625) rotateTo:0.0f withBlockType:ROCK inWorld:world];
    [self addChild:block21];
    [block21 release];
    
    NonBreakableCircle *block22 = [[NonBreakableCircle alloc] initInParent:self At:dccp(6.5, 6) rotateTo:0.0f withBlockType:ROCK inWorld:world];
    [self addChild:block22];
    [block22 release];
    
    NonBreakableCircle *block23 = [[NonBreakableCircle alloc] initInParent:self At:dccp(6.5, 6.34375) rotateTo:0.0f withBlockType:ROCK inWorld:world];
    [self addChild:block23];
    [block23 release];
    
    NonBreakableCircle *block24 = [[NonBreakableCircle alloc] initInParent:self At:dccp(6.8125, 5.03125) rotateTo:0.0f withBlockType:ROCK inWorld:world];
    [self addChild:block24];
    [block24 release];
    
    NonBreakableCircle *block25 = [[NonBreakableCircle alloc] initInParent:self At:dccp(6.8125, 5.34375) rotateTo:0.0f withBlockType:ROCK inWorld:world];
    [self addChild:block25];
    [block25 release];
    
    NonBreakableCircle *block26 = [[NonBreakableCircle alloc] initInParent:self At:dccp(6.8125, 5.65625) rotateTo:0.0f withBlockType:ROCK inWorld:world];
    [self addChild:block26];
    [block26 release];
    
    NonBreakableCircle *block27 = [[NonBreakableCircle alloc] initInParent:self At:dccp(6.8125, 6) rotateTo:0.0f withBlockType:ROCK inWorld:world];
    [self addChild:block27];
    [block27 release];
    
    NonBreakableCircle *block28 = [[NonBreakableCircle alloc] initInParent:self At:dccp(6.8125, 6.34375) rotateTo:0.0f withBlockType:ROCK inWorld:world];
    [self addChild:block28];
    [block28 release];
    
    NonBreakableCircle *block29 = [[NonBreakableCircle alloc] initInParent:self At:dccp(7.15625, 5.03125) rotateTo:0.0f withBlockType:ROCK inWorld:world];
    [self addChild:block29];
    [block29 release];
    
    NonBreakableCircle *block30 = [[NonBreakableCircle alloc] initInParent:self At:dccp(7.15625, 5.34375) rotateTo:0.0f withBlockType:ROCK inWorld:world];
    [self addChild:block30];
    [block30 release];
    
    NonBreakableCircle *block31 = [[NonBreakableCircle alloc] initInParent:self At:dccp(7.15625, 5.65625) rotateTo:0.0f withBlockType:ROCK inWorld:world];
    [self addChild:block31];
    [block31 release];
    
    NonBreakableCircle *block32 = [[NonBreakableCircle alloc] initInParent:self At:dccp(7.15625, 6) rotateTo:0.0f withBlockType:ROCK inWorld:world];
    [self addChild:block32];
    [block32 release];
    
    NonBreakableCircle *block33 = [[NonBreakableCircle alloc] initInParent:self At:dccp(7.15625, 6.34375) rotateTo:0.0f withBlockType:ROCK inWorld:world];
    [self addChild:block33];
    [block33 release];
    
    numberOfCatsThisLevel = 6;
    
    shouldRotateWarp1 = YES;
}

-(void)createLevel18{
    ExplodingCat1 *catNumber1 = [[ExplodingCat1 alloc] initInParent:self At:dccp(7.09375, 1.25) rotateTo:0.0f inWorld:world catType:CAT_03];
    [self addChild:catNumber1];
    catNumber1.delegate = self;
    [catNumber1 release];
    
    rotatingArrowWarp2 = [CCSprite spriteWithSpriteFrameName:IMAGE_SELECT_BACK_BTN];
    [self addChild:rotatingArrowWarp2 z:200];
    rotatingArrowWarp2.position = dccp(391,129);
    rotatingArrowWarp2.rotation = -269.9982452392578;
    ExplodingCat1 *catNumber3 = [[ExplodingCat1 alloc] initInParent:self At:dccp(6.875, 6.875) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber3];
    [catNumber3 release];
    
    ExplodingCat1 *catNumber4 = [[ExplodingCat1 alloc] initInParent:self At:dccp(12.34375, 1.15625) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber4];
    [catNumber4 release];
    
    ExplodingCat1 *catNumber5 = [[ExplodingCat1 alloc] initInParent:self At:dccp(10.28125, 1.40625) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber5];
    [catNumber5 release];
    
    Plank01 *plank6 = [[Plank01 alloc] initInParent:self At:dccp(7.96875, 4.15625) rotateTo:-1.5707963267948966 plankType:2 inWorld:world];
    [self addChild:plank6];
    [plank6 release];
    
    ExplodingCat1 *catNumber7 = [[ExplodingCat1 alloc] initInParent:self At:dccp(10.25, 5.25) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber7];
    [catNumber7 release];
    
    NonBreakableCircle *block8 = [[NonBreakableCircle alloc] initInParent:self At:dccp(6.625, 5.125) rotateTo:0.0f withBlockType:ROCK inWorld:world];
    [self addChild:block8];
    [block8 release];
    
    ExplodingCat1 *catNumber9 = [[ExplodingCat1 alloc] initInParent:self At:dccp(8.6875, 1.1875) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber9];
    [catNumber9 release];
    
    NonBreakableCircle *block10 = [[NonBreakableCircle alloc] initInParent:self At:dccp(6.625, 5.4375) rotateTo:0.0f withBlockType:ROCK inWorld:world];
    [self addChild:block10];
    [block10 release];
    
    NonBreakableCircle *block11 = [[NonBreakableCircle alloc] initInParent:self At:dccp(6.625, 5.75) rotateTo:0.0f withBlockType:ROCK inWorld:world];
    [self addChild:block11];
    [block11 release];
    
    NonBreakableCircle *block12 = [[NonBreakableCircle alloc] initInParent:self At:dccp(6.625, 6.09375) rotateTo:0.0f withBlockType:ROCK inWorld:world];
    [self addChild:block12];
    [block12 release];
    
    NonBreakableCircle *block13 = [[NonBreakableCircle alloc] initInParent:self At:dccp(6.625, 6.4375) rotateTo:0.0f withBlockType:ROCK inWorld:world];
    [self addChild:block13];
    [block13 release];
    
    NonBreakableCircle *block14 = [[NonBreakableCircle alloc] initInParent:self At:dccp(6.9375, 4.9375) rotateTo:0.0f withBlockType:ROCK inWorld:world];
    [self addChild:block14];
    [block14 release];
    
    NonBreakableCircle *block15 = [[NonBreakableCircle alloc] initInParent:self At:dccp(6.9375, 5.25) rotateTo:0.0f withBlockType:ROCK inWorld:world];
    [self addChild:block15];
    [block15 release];
    
    NonBreakableCircle *block16 = [[NonBreakableCircle alloc] initInParent:self At:dccp(6.9375, 5.5625) rotateTo:0.0f withBlockType:ROCK inWorld:world];
    [self addChild:block16];
    [block16 release];
    
    NonBreakableCircle *block17 = [[NonBreakableCircle alloc] initInParent:self At:dccp(6.9375, 5.90625) rotateTo:0.0f withBlockType:ROCK inWorld:world];
    [self addChild:block17];
    [block17 release];
    
    NonBreakableCircle *block18 = [[NonBreakableCircle alloc] initInParent:self At:dccp(6.9375, 6.25) rotateTo:0.0f withBlockType:ROCK inWorld:world];
    [self addChild:block18];
    [block18 release];
    
    NonBreakableCircle *block19 = [[NonBreakableCircle alloc] initInParent:self At:dccp(7.28125, 4.75) rotateTo:0.0f withBlockType:ROCK inWorld:world];
    [self addChild:block19];
    [block19 release];
    
    NonBreakableCircle *block20 = [[NonBreakableCircle alloc] initInParent:self At:dccp(7.28125, 5.0625) rotateTo:0.0f withBlockType:ROCK inWorld:world];
    [self addChild:block20];
    [block20 release];
    
    NonBreakableCircle *block21 = [[NonBreakableCircle alloc] initInParent:self At:dccp(7.28125, 5.375) rotateTo:0.0f withBlockType:ROCK inWorld:world];
    [self addChild:block21];
    [block21 release];
    
    NonBreakableCircle *block22 = [[NonBreakableCircle alloc] initInParent:self At:dccp(7.28125, 5.71875) rotateTo:0.0f withBlockType:ROCK inWorld:world];
    [self addChild:block22];
    [block22 release];
    
    NonBreakableCircle *block23 = [[NonBreakableCircle alloc] initInParent:self At:dccp(7.28125, 6.0625) rotateTo:0.0f withBlockType:ROCK inWorld:world];
    [self addChild:block23];
    [block23 release];
    
    NonBreakableCircle *block24 = [[NonBreakableCircle alloc] initInParent:self At:dccp(7.59375, 4.5) rotateTo:0.0f withBlockType:ROCK inWorld:world];
    [self addChild:block24];
    [block24 release];
    
    NonBreakableCircle *block25 = [[NonBreakableCircle alloc] initInParent:self At:dccp(7.59375, 4.8125) rotateTo:0.0f withBlockType:ROCK inWorld:world];
    [self addChild:block25];
    [block25 release];
    
    NonBreakableCircle *block26 = [[NonBreakableCircle alloc] initInParent:self At:dccp(7.59375, 5.125) rotateTo:0.0f withBlockType:ROCK inWorld:world];
    [self addChild:block26];
    [block26 release];
    
    NonBreakableCircle *block27 = [[NonBreakableCircle alloc] initInParent:self At:dccp(7.59375, 5.46875) rotateTo:0.0f withBlockType:ROCK inWorld:world];
    [self addChild:block27];
    [block27 release];
    
    NonBreakableCircle *block28 = [[NonBreakableCircle alloc] initInParent:self At:dccp(7.59375, 5.8125) rotateTo:0.0f withBlockType:ROCK inWorld:world];
    [self addChild:block28];
    [block28 release];
    
    Plank01 *plank29 = [[Plank01 alloc] initInParent:self At:dccp(7.9375, 1.84375) rotateTo:-1.5707963267948966 plankType:3 inWorld:world];
    [self addChild:plank29];
    [plank29 release];
    
    NonBreakableCircle *block30 = [[NonBreakableCircle alloc] initInParent:self At:dccp(6.25, 5.4375) rotateTo:0.0f withBlockType:ROCK inWorld:world];
    [self addChild:block30];
    [block30 release];
    
    NonBreakableCircle *block31 = [[NonBreakableCircle alloc] initInParent:self At:dccp(6.25, 5.75) rotateTo:0.0f withBlockType:ROCK inWorld:world];
    [self addChild:block31];
    [block31 release];
    
    NonBreakableCircle *block32 = [[NonBreakableCircle alloc] initInParent:self At:dccp(6.25, 6.09375) rotateTo:0.0f withBlockType:ROCK inWorld:world];
    [self addChild:block32];
    [block32 release];
    
    NonBreakableCircle *block33 = [[NonBreakableCircle alloc] initInParent:self At:dccp(6.25, 6.40625) rotateTo:0.0f withBlockType:ROCK inWorld:world];
    [self addChild:block33];
    [block33 release];
    
    NonBreakableCircle *block34 = [[NonBreakableCircle alloc] initInParent:self At:dccp(7.59375, 6.125) rotateTo:0.0f withBlockType:ROCK inWorld:world];
    [self addChild:block34];
    [block34 release];
    
    NonBreakableCircle *block35 = [[NonBreakableCircle alloc] initInParent:self At:dccp(7.28125, 6.40625) rotateTo:0.0f withBlockType:ROCK inWorld:world];
    [self addChild:block35];
    [block35 release];
    
    NonBreakableCircle *block36 = [[NonBreakableCircle alloc] initInParent:self At:dccp(7.625, 6.46875) rotateTo:0.0f withBlockType:ROCK inWorld:world];
    [self addChild:block36];
    [block36 release];
    
    Plank01 *plank37 = [[Plank01 alloc] initInParent:self At:dccp(10.65625, 4.75) rotateTo:3.141592653589793 plankType:2 inWorld:world];
    [self addChild:plank37];
    [plank37 release];
    
    numberOfCatsThisLevel = 6;
    
    shouldRotateWarp2 = YES;
}

-(void)createLevel19{
    ExplodingCat1 *catNumber1 = [[ExplodingCat1 alloc] initInParent:self At:dccp(7.84375, 4.75) rotateTo:0.0f inWorld:world catType:CAT_03];
    [self addChild:catNumber1];
    catNumber1.delegate = self;
    [catNumber1 release];
    
    rotatingArrowWarp2 = [CCSprite spriteWithSpriteFrameName:IMAGE_SELECT_BACK_BTN];
    [self addChild:rotatingArrowWarp2 z:200];
    rotatingArrowWarp2.position = dccp(231,217);
    rotatingArrowWarp2.rotation = -269.9982452392578;
    ExplodingCat1 *catNumber3 = [[ExplodingCat1 alloc] initInParent:self At:dccp(10.53125, 1.71875) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber3];
    [catNumber3 release];
    
    ExplodingCat1 *catNumber4 = [[ExplodingCat1 alloc] initInParent:self At:dccp(8.84375, 1.71875) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber4];
    [catNumber4 release];
    
    ExplodingCat1 *catNumber5 = [[ExplodingCat1 alloc] initInParent:self At:dccp(3.28125, 0.75) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber5];
    [catNumber5 release];
    
    ExplodingCat1 *catNumber6 = [[ExplodingCat1 alloc] initInParent:self At:dccp(6.84375, 1.71875) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber6];
    [catNumber6 release];
    
    NonBreakableCircle *block7 = [[NonBreakableCircle alloc] initInParent:self At:dccp(5.96875, 4.78125) rotateTo:0.0f withBlockType:WHEEL inWorld:world];
    [self addChild:block7];
    [block7 release];
    
    NonBreakableCircle *block8 = [[NonBreakableCircle alloc] initInParent:self At:dccp(11.5, 4.78125) rotateTo:0.0f withBlockType:WHEEL inWorld:world];
    [self addChild:block8];
    [block8 release];
    
    NonBreakableCircle *block9 = [[NonBreakableCircle alloc] initInParent:self At:dccp(9.65625, 4.78125) rotateTo:0.0f withBlockType:WHEEL inWorld:world];
    [self addChild:block9];
    [block9 release];
    
    ExplodingCat1 *catNumber10 = [[ExplodingCat1 alloc] initInParent:self At:dccp(5.25, 2.96875) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber10];
    [catNumber10 release];
    
    numberOfCatsThisLevel = 6;
    
    movingArrowWarpX = YES;
}

-(void)createLevel20{
    ExplodingCat1 *catNumber1 = [[ExplodingCat1 alloc] initInParent:self At:dccp(8.46875, 1.4375) rotateTo:0.0f inWorld:world catType:CAT_03];
    [self addChild:catNumber1];
    catNumber1.delegate = self;
    [catNumber1 release];
    
    rotatingArrowWarp2 = [CCSprite spriteWithSpriteFrameName:IMAGE_SELECT_BACK_BTN];
    [self addChild:rotatingArrowWarp2 z:200];
    rotatingArrowWarp2.position = dccp(380,189);
    rotatingArrowWarp2.rotation = -269.9982452392578;
    ExplodingCat1 *catNumber3 = [[ExplodingCat1 alloc] initInParent:self At:dccp(8.46875, 4.90625) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber3];
    [catNumber3 release];
    
    ExplodingCat1 *catNumber4 = [[ExplodingCat1 alloc] initInParent:self At:dccp(7.03125, 4.9375) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber4];
    [catNumber4 release];
    
    ExplodingCat1 *catNumber5 = [[ExplodingCat1 alloc] initInParent:self At:dccp(10.25, 5.21875) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber5];
    [catNumber5 release];
    
    Plank01 *plank6 = [[Plank01 alloc] initInParent:self At:dccp(8, 4.40625) rotateTo:0.0f plankType:2 inWorld:world];
    [self addChild:plank6];
    [plank6 release];
    
    NonBreakableCircle *block7 = [[NonBreakableCircle alloc] initInParent:self At:dccp(8.15625, 5.375) rotateTo:0.0f withBlockType:ROCK inWorld:world];
    [self addChild:block7];
    [block7 release];
    
    NonBreakableCircle *block8 = [[NonBreakableCircle alloc] initInParent:self At:dccp(8.46875, 5.375) rotateTo:0.0f withBlockType:ROCK inWorld:world];
    [self addChild:block8];
    [block8 release];
    
    NonBreakableCircle *block9 = [[NonBreakableCircle alloc] initInParent:self At:dccp(8.78125, 5.375) rotateTo:0.0f withBlockType:ROCK inWorld:world];
    [self addChild:block9];
    [block9 release];
    
    NonBreakableCircle *block10 = [[NonBreakableCircle alloc] initInParent:self At:dccp(7.9375, 5.71875) rotateTo:0.0f withBlockType:ROCK inWorld:world];
    [self addChild:block10];
    [block10 release];
    
    NonBreakableCircle *block11 = [[NonBreakableCircle alloc] initInParent:self At:dccp(8.25, 5.71875) rotateTo:0.0f withBlockType:ROCK inWorld:world];
    [self addChild:block11];
    [block11 release];
    
    NonBreakableCircle *block12 = [[NonBreakableCircle alloc] initInParent:self At:dccp(8.5625, 5.71875) rotateTo:0.0f withBlockType:ROCK inWorld:world];
    [self addChild:block12];
    [block12 release];
    
    NonBreakableCircle *block13 = [[NonBreakableCircle alloc] initInParent:self At:dccp(8.84375, 5.71875) rotateTo:0.0f withBlockType:ROCK inWorld:world];
    [self addChild:block13];
    [block13 release];
    
    NonBreakableCircle *block14 = [[NonBreakableCircle alloc] initInParent:self At:dccp(9.15625, 5.71875) rotateTo:0.0f withBlockType:ROCK inWorld:world];
    [self addChild:block14];
    [block14 release];
    
    NonBreakableCircle *block15 = [[NonBreakableCircle alloc] initInParent:self At:dccp(7.59375, 6) rotateTo:0.0f withBlockType:ROCK inWorld:world];
    [self addChild:block15];
    [block15 release];
    
    NonBreakableCircle *block16 = [[NonBreakableCircle alloc] initInParent:self At:dccp(7.90625, 6) rotateTo:0.0f withBlockType:ROCK inWorld:world];
    [self addChild:block16];
    [block16 release];
    
    NonBreakableCircle *block17 = [[NonBreakableCircle alloc] initInParent:self At:dccp(8.21875, 6) rotateTo:0.0f withBlockType:ROCK inWorld:world];
    [self addChild:block17];
    [block17 release];
    
    NonBreakableCircle *block18 = [[NonBreakableCircle alloc] initInParent:self At:dccp(8.5, 6) rotateTo:0.0f withBlockType:ROCK inWorld:world];
    [self addChild:block18];
    [block18 release];
    
    NonBreakableCircle *block19 = [[NonBreakableCircle alloc] initInParent:self At:dccp(8.8125, 6) rotateTo:0.0f withBlockType:ROCK inWorld:world];
    [self addChild:block19];
    [block19 release];
    
    NonBreakableCircle *block20 = [[NonBreakableCircle alloc] initInParent:self At:dccp(9.125, 6) rotateTo:0.0f withBlockType:ROCK inWorld:world];
    [self addChild:block20];
    [block20 release];
    
    NonBreakableCircle *block21 = [[NonBreakableCircle alloc] initInParent:self At:dccp(9.4375, 6) rotateTo:0.0f withBlockType:ROCK inWorld:world];
    [self addChild:block21];
    [block21 release];
    
    NonBreakableCircle *block22 = [[NonBreakableCircle alloc] initInParent:self At:dccp(7.25, 6.28125) rotateTo:0.0f withBlockType:ROCK inWorld:world];
    [self addChild:block22];
    [block22 release];
    
    NonBreakableCircle *block23 = [[NonBreakableCircle alloc] initInParent:self At:dccp(7.65625, 6.34375) rotateTo:0.0f withBlockType:ROCK inWorld:world];
    [self addChild:block23];
    [block23 release];
    
    NonBreakableCircle *block24 = [[NonBreakableCircle alloc] initInParent:self At:dccp(8.875, 6.34375) rotateTo:0.0f withBlockType:ROCK inWorld:world];
    [self addChild:block24];
    [block24 release];
    
    NonBreakableCircle *block25 = [[NonBreakableCircle alloc] initInParent:self At:dccp(8.0625, 6.34375) rotateTo:0.0f withBlockType:ROCK inWorld:world];
    [self addChild:block25];
    [block25 release];
    
    NonBreakableCircle *block26 = [[NonBreakableCircle alloc] initInParent:self At:dccp(8.46875, 6.34375) rotateTo:0.0f withBlockType:ROCK inWorld:world];
    [self addChild:block26];
    [block26 release];
    
    NonBreakableCircle *block27 = [[NonBreakableCircle alloc] initInParent:self At:dccp(9.3125, 6.34375) rotateTo:0.0f withBlockType:ROCK inWorld:world];
    [self addChild:block27];
    [block27 release];
    
    NonBreakableCircle *block28 = [[NonBreakableCircle alloc] initInParent:self At:dccp(9.71875, 6.28125) rotateTo:0.0f withBlockType:ROCK inWorld:world];
    [self addChild:block28];
    [block28 release];
    
    numberOfCatsThisLevel = 4;
    shouldRotateWarp2 = YES;
}

-(void)createLevel21{
    ExplodingCat1 *catNumber1 = [[ExplodingCat1 alloc] initInParent:self At:dccp(7.28125, 3.4375) rotateTo:0.0f inWorld:world catType:CAT_03];
    [self addChild:catNumber1];
    catNumber1.delegate = self;
    [catNumber1 release];
    
    rotatingArrowWarp2 = [CCSprite spriteWithSpriteFrameName:IMAGE_SELECT_BACK_BTN];
    [self addChild:rotatingArrowWarp2 z:200];
    rotatingArrowWarp2.position = dccp(334,109);
    rotatingArrowWarp2.rotation = -269.9982452392578;
    ExplodingCat1 *catNumber3 = [[ExplodingCat1 alloc] initInParent:self At:dccp(5.75, 5.40625) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber3];
    [catNumber3 release];
    
    ExplodingCat1 *catNumber4 = [[ExplodingCat1 alloc] initInParent:self At:dccp(10.40625, 0.78125) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber4];
    [catNumber4 release];
    
    BreakableBlock *block5 = [[BreakableBlock alloc] initInParent:self At:dccp(5.53125, 3.375) rotateTo:0.0f withBlockType:CINDER inWorld:world];
    [self addChild:block5];
    [block5 release];
    
    BreakableBlock *block6 = [[BreakableBlock alloc] initInParent:self At:dccp(7.5, 6.0625) rotateTo:0.0f withBlockType:CINDER inWorld:world];
    [self addChild:block6];
    [block6 release];
    
    BreakableBlock *block7 = [[BreakableBlock alloc] initInParent:self At:dccp(7.5, 5.28125) rotateTo:0.0f withBlockType:CINDER inWorld:world];
    [self addChild:block7];
    [block7 release];
    
    numberOfCatsThisLevel = 3;


    
    shouldRotateWarp2 = YES;
    movingArrowWarp = YES;

}

-(void)createLevel22{
    
    ExplodingCat1 *catNumber1 = [[ExplodingCat1 alloc] initInParent:self At:dccp(8.875, 1.25) rotateTo:0.0f inWorld:world catType:CAT_03];
    [self addChild:catNumber1];
    catNumber1.delegate = self;
    [catNumber1 release];
    
    rotatingArrowWarp2 = [CCSprite spriteWithSpriteFrameName:IMAGE_SELECT_BACK_BTN];
    [self addChild:rotatingArrowWarp2 z:200];
    rotatingArrowWarp2.position = dccp(389,64);
    rotatingArrowWarp2.rotation = -209.9992218017578;
    ExplodingCat1 *catNumber3 = [[ExplodingCat1 alloc] initInParent:self At:dccp(4.59375, 1.34375) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber3];
    [catNumber3 release];
    
    ExplodingCat1 *catNumber4 = [[ExplodingCat1 alloc] initInParent:self At:dccp(10.65625, 1.375) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber4];
    [catNumber4 release];
    
    rotatingArrowWarp = [CCSprite spriteWithSpriteFrameName:IMAGE_WARP_EXIT_1];
    [self addChild:rotatingArrowWarp z:200];
    rotatingArrowWarp.position = dccp(381,162);
    rotatingArrowWarp.rotation = -180;
    BreakableBlock *block6 = [[BreakableBlock alloc] initInParent:self At:dccp(7.59375, 1.28125) rotateTo:0.0f withBlockType:WARP_01 inWorld:world];
    [self addChild:block6];
    [block6 release];
    
    NonBreakableCircle *block7 = [[NonBreakableCircle alloc] initInParent:self At:dccp(4.5625, 3.5) rotateTo:0.0f withBlockType:WHEEL inWorld:world];
    [self addChild:block7];
    [block7 release];
    
    NonBreakableCircle *block8 = [[NonBreakableCircle alloc] initInParent:self At:dccp(11.875, 3.53125) rotateTo:0.0f withBlockType:WHEEL inWorld:world];
    [self addChild:block8];
    [block8 release];
    
    numberOfCatsThisLevel = 3;

    
    shouldRotateWarp1 = YES;
}

-(void)createLevel23{
    ExplodingCat1 *catNumber1 = [[ExplodingCat1 alloc] initInParent:self At:dccp(9.15625, 2.625) rotateTo:0.0f inWorld:world catType:CAT_03];
    [self addChild:catNumber1];
    catNumber1.delegate = self;
    [catNumber1 release];
    
    rotatingArrowWarp2 = [CCSprite spriteWithSpriteFrameName:IMAGE_SELECT_BACK_BTN];
    [self addChild:rotatingArrowWarp2 z:200];
    rotatingArrowWarp2.position = dccp(334,201);
    rotatingArrowWarp2.rotation = -209.99856567382813;
    ExplodingCat1 *catNumber3 = [[ExplodingCat1 alloc] initInParent:self At:dccp(6.34375, 5.5) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber3];
    [catNumber3 release];
    
    ExplodingCat1 *catNumber4 = [[ExplodingCat1 alloc] initInParent:self At:dccp(11.8125, 0.8125) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber4];
    [catNumber4 release];
    
    Plank01 *plank5 = [[Plank01 alloc] initInParent:self At:dccp(6.84375, 5) rotateTo:0.0f plankType:2 inWorld:world];
    [self addChild:plank5];
    [plank5 release];
    
    ExplodingCat1 *catNumber6 = [[ExplodingCat1 alloc] initInParent:self At:dccp(6.15625, 0.8125) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber6];
    [catNumber6 release];
    
    BreakableBlock *block7 = [[BreakableBlock alloc] initInParent:self At:dccp(7.6875, 5.53125) rotateTo:0.0f withBlockType:CINDER inWorld:world];
    [self addChild:block7];
    [block7 release];
    
    BreakableBlock *block8 = [[BreakableBlock alloc] initInParent:self At:dccp(7.6875, 6.28125) rotateTo:0.0f withBlockType:CINDER inWorld:world];
    [self addChild:block8];
    [block8 release];
    
    numberOfCatsThisLevel = 4;
    
    shouldRotateWarp2 = YES;
}

-(void)createLevel24{
    
    ExplodingCat1 *catNumber1 = [[ExplodingCat1 alloc] initInParent:self At:dccp(9.03125, 5.21875) rotateTo:0.0f inWorld:world catType:CAT_03];
    [self addChild:catNumber1];
    catNumber1.delegate = self;
    [catNumber1 release];
    
    rotatingArrowWarp2 = [CCSprite spriteWithSpriteFrameName:IMAGE_SELECT_BACK_BTN];
    [self addChild:rotatingArrowWarp2 z:200];
    rotatingArrowWarp2.position = dccp(326,201);
    rotatingArrowWarp2.rotation = -209.99856567382813;
    ExplodingCat1 *catNumber3 = [[ExplodingCat1 alloc] initInParent:self At:dccp(11.125, 0.8125) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber3];
    [catNumber3 release];
    
    ExplodingCat1 *catNumber4 = [[ExplodingCat1 alloc] initInParent:self At:dccp(8.46875, 2.15625) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber4];
    [catNumber4 release];
    
    ExplodingCat1 *catNumber5 = [[ExplodingCat1 alloc] initInParent:self At:dccp(6.5625, 6.71875) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber5];
    [catNumber5 release];
    
    BreakableBlock *block6 = [[BreakableBlock alloc] initInParent:self At:dccp(7.6875, 6.78125) rotateTo:0.0f withBlockType:CINDER inWorld:world];
    [self addChild:block6];
    [block6 release];
    
    Plank01 *plank7 = [[Plank01 alloc] initInParent:self At:dccp(7.71875, 6.21875) rotateTo:0.0f plankType:2 inWorld:world];
    [self addChild:plank7];
    [plank7 release];
    
    ExplodingCat1 *catNumber8 = [[ExplodingCat1 alloc] initInParent:self At:dccp(8.875, 6.71875) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber8];
    [catNumber8 release];
    
    BreakableBlock *block9 = [[BreakableBlock alloc] initInParent:self At:dccp(7.6875, 7.5625) rotateTo:0.0f withBlockType:CINDER inWorld:world];
    [self addChild:block9];
    [block9 release];
    
    ExplodingCat1 *catNumber10 = [[ExplodingCat1 alloc] initInParent:self At:dccp(6.53125, 4.8125) rotateTo:0.0f inWorld:world catType:CAT_01];
    [self addChild:catNumber10];
    [catNumber10 release];
    
    numberOfCatsThisLevel = 6;
    
    shouldRotateWarp2 = YES;
}




-(void)createTestLevel
{
    ExplodingCat1 *catNumber1 = [[ExplodingCat1 alloc] initInParent:self At:dccp(7.03125, 4.4375) rotateTo:0.0f inWorld:world];
    [self addChild:catNumber1];
    [catNumber1 release];
    
    Plank01 *plank2 = [[Plank01 alloc] initInParent:self At:dccp(6.34375, 4.875) rotateTo:1.5707963267948966 plankType:3 inWorld:world];
    [self addChild:plank2];
    [plank2 release];
    
    Plank01 *plank3 = [[Plank01 alloc] initInParent:self At:dccp(7.59375, 4.875) rotateTo:1.5707963267948966 plankType:3 inWorld:world];
    [self addChild:plank3];
    [plank3 release];
    
    Plank01 *plank4 = [[Plank01 alloc] initInParent:self At:dccp(6.96875, 5.84375) rotateTo:0.0f plankType:3 inWorld:world];
    [self addChild:plank4];
    [plank4 release];
    
    ExplodingCat1 *catNumber5 = [[ExplodingCat1 alloc] initInParent:self At:dccp(7.03125, 6.3125) rotateTo:0.0f inWorld:world];
    [self addChild:catNumber5];
    [catNumber5 release];
    
    BreakableBlock *block6 = [[BreakableBlock alloc] initInParent:self At:dccp(7.84375, 1.46875) rotateTo:0.0f withBlockType:WARP_02 inWorld:world];
    [self addChild:block6];
    [block6 release];
    
    BreakableBlock *block7 = [[BreakableBlock alloc] initInParent:self At:dccp(6.4375, 3.9375) rotateTo:3.141592653589793 withBlockType:PLATFORM2 inWorld:world];
    [self addChild:block7];
    [block7 release];
    
    rotatingArrowWarp2 = [CCSprite spriteWithSpriteFrameName:IMAGE_SELECT_BACK_BTN];
    [self addChild:rotatingArrowWarp2 z:200];
    rotatingArrowWarp2.position = dccp(363,211);
    rotatingArrowWarp2.rotation = -180;
    
    BreakableBlock *block9 = [[BreakableBlock alloc] initInParent:self At:dccp(3.71875, 4.6875) rotateTo:2.6096368784908255 withBlockType:PLATFORM2 inWorld:world];
    [self addChild:block9];
    [block9 release];
    
    BreakableBlock *block10 = [[BreakableBlock alloc] initInParent:self At:dccp(9.21875, 6.28125) rotateTo:-1.5707963267948966 withBlockType:PLATFORM2 inWorld:world];
    [self addChild:block10];
    [block10 release];
    
    BreakableBlock *block11 = [[BreakableBlock alloc] initInParent:self At:dccp(5.21875, 8.09375) rotateTo:0.0f withBlockType:WARP_01 inWorld:world];
    [self addChild:block11];
    [block11 release];
    
    BreakableBlock *block12 = [[BreakableBlock alloc] initInParent:self At:dccp(1.03125, 5.4375) rotateTo:0.0f withBlockType:PLATFORM2 inWorld:world];
    [self addChild:block12];
    [block12 release];
    
    rotatingArrowWarp = [CCSprite spriteWithSpriteFrameName:IMAGE_WARP_EXIT_1];
    [self addChild:rotatingArrowWarp z:200];
    rotatingArrowWarp.position = dccp(139,201);
    rotatingArrowWarp.rotation = -330.0001220703125;
    BreakableBlock *block14 = [[BreakableBlock alloc] initInParent:self At:dccp(7.46875, 7.625) rotateTo:3.141592653589793 withBlockType:PLATFORM2 inWorld:world];
    [self addChild:block14];
    [block14 release];
    
    numberOfCatsThisLevel = 2;
    
    
    /*
     ExplodingCat1 *catNumber1 = [[ExplodingCat1 alloc] initInParent:self At:dccp(11.5625, 3.96875) rotateTo:0.0f inWorld:world];
     [self addChild:catNumber1];
     [catNumber1 release];
     
     BreakableBlock *block2 = [[BreakableBlock alloc] initInParent:self At:dccp(10.40625, 2.03125) rotateTo:-1.5707963267948966 withBlockType:PLATFORM2 inWorld:world];
     [self addChild:block2];
     [block2 release];
     
     ExplodingCat1 *catNumber3 = [[ExplodingCat1 alloc] initInParent:self At:dccp(8.59375, 3.9375) rotateTo:0.0f inWorld:world];
     [self addChild:catNumber3];
     [catNumber3 release];
     
     
     b2BodyDef bodyDef4;
     bodyDef4.type = b2_staticBody;
     CCSprite *boxSprite4 = [CCSprite spriteWithSpriteFrameName:@"particleTexture.png"];
     boxSprite4.opacity = 0;
     [self addChild:boxSprite4 z:0 tag:TAG_WARP_HOLE];
     float yPos4 = 5.84375;
     float xPos4 = 8.5625;
     bodyDef4.userData = boxSprite4;
     b2Body *body4 = world->CreateBody(&bodyDef4);
     body4->SetTransform(b2Vec2(xPos4, yPos4),0);
     b2PolygonShape shape4;
     shape4.SetAsBox(0.46875,0.46875);
     b2FixtureDef fixtureDef4;
     fixtureDef4.shape = &shape4;
     fixtureDef4.filter.groupIndex = COLLIDE_WARP_HOLE;
     fixtureDef4.density = 4.0f;
     fixtureDef4.friction = 1.0f;
     fixtureDef4.restitution = 0.1f;
     body4->CreateFixture(&fixtureDef4);
     CCParticleSystemQuad *warpEmitter4= [[CCParticleSystemQuad alloc]initWithFile:WARP_EMITTER];
     warpEmitter4.position = ccp(274,187);
     [self addChild:warpEmitter4];
     
     rotatingArrowWarp = [CCSprite spriteWithSpriteFrameName:IMAGE_WARP_EXIT_1];
     [self addChild:rotatingArrowWarp z:200];
     rotatingArrowWarp.position = ccp(395,184);
     rotatingArrowWarp.rotation = -239.9988250732422;
     BreakableBlock *block6 = [[BreakableBlock alloc] initInParent:self At:dccp(10.40625, 5.125) rotateTo:-1.5707963267948966 withBlockType:PLATFORM2 inWorld:world];
     [self addChild:block6];
     [block6 release];
     
     BreakableBlock *block7 = [[BreakableBlock alloc] initInParent:self At:dccp(14.90625, 6.53125) rotateTo:0.0f withBlockType:PLATFORM2 inWorld:world];
     [self addChild:block7];
     [block7 release];
     
     BreakableBlock *block8 = [[BreakableBlock alloc] initInParent:self At:dccp(12, 3.4375) rotateTo:0.0f withBlockType:PLATFORM2 inWorld:world];
     [self addChild:block8];
     [block8 release];
     
     BreakableBlock *block9 = [[BreakableBlock alloc] initInParent:self At:dccp(8.90625, 3.4375) rotateTo:0.0f withBlockType:PLATFORM2 inWorld:world];
     [self addChild:block9];
     [block9 release];
     
     BreakableBlock *block10 = [[BreakableBlock alloc] initInParent:self At:dccp(11.78125, 6.53125) rotateTo:0.0f withBlockType:PLATFORM2 inWorld:world];
     [self addChild:block10];
     [block10 release];
     
     numberOfCatsThisLevel = 2;
     */
    
}







- (void)bombExplodedAtPosition:(CGPoint)pos
{
    
	BOOL doSuction = NO; // Very cool looking implosion effect instead of explosion.
    b2World *_world = world;
    //In Box2D the bodies are a linked list, so keep getting the next one until it doesn't exist.
	for (b2Body* b = _world->GetBodyList(); b; b = b->GetNext())
	{
        //Box2D uses meters, there's 32 pixels in one meter. PTM_RATIO is defined somewhere in the class.
		//b2Vec2 b2TouchPosition = b2Vec2(pos.x/PTM_RATIO, pos.y/PTM_RATIO);
		b2Vec2 b2TouchPosition = b2Vec2(pos.x/PTM_RATIO, pos.y/PTM_RATIO);
        b2Vec2 b2BodyPosition = b2Vec2(b->GetPosition().x, b->GetPosition().y);
        
        //Don't forget any measurements always need to take PTM_RATIO into account
		float maxDistance = 8; // In your head don't forget this number is low because we're multiplying it by 32 pixels;
		int maxForce = 4;
		CGFloat distance; // Why do i want to use CGFloat vs float - I'm not sure, but this mixing seems to work fine for this little test.
		CGFloat strength;
		float force;
		CGFloat angle;
        
		if(doSuction) // To go towards the press, all we really change is the atanf function, and swap which goes first to reverse the angle
		{
			// Get the distance, and cap it
			distance = b2Distance(b2BodyPosition, b2TouchPosition);
			if(distance > maxDistance) distance = maxDistance - 0.01;
			// Get the strength
			//strength = distance / maxDistance; // Uncomment and reverse these two. and ones further away will get more force instead of less
			strength = (maxDistance - distance) / maxDistance; // This makes it so that the closer something is - the stronger, instead of further
			force  = strength * maxForce;
            
			// Get the angle
			angle = atan2f(b2TouchPosition.y - b2BodyPosition.y, b2TouchPosition.x - b2BodyPosition.x);
			// Apply an impulse to the body, using the angle
			b->ApplyLinearImpulse(b2Vec2(cosf(angle) * force, sinf(angle) * force), b->GetPosition());
		}
		else
		{
			distance = b2Distance(b2BodyPosition, b2TouchPosition);
			if(distance > maxDistance) distance = maxDistance - 0.01;
            
			// Normally if distance is max distance, it'll have the most strength, this makes it so the opposite is true - closer = stronger
			strength = (maxDistance - distance) / maxDistance; // This makes it so that the closer something is - the stronger, instead of further
			force = strength * maxForce;
			angle = atan2f(b2BodyPosition.y - b2TouchPosition.y, b2BodyPosition.x - b2TouchPosition.x);
			// Apply an impulse to the body, using the angle
			b->ApplyLinearImpulse(b2Vec2(cosf(angle) * force, sinf(angle) * force), b->GetPosition());
		}
	}
}




@end





