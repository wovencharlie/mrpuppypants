//
//  MenuBetweenLevels.h
//  MrPants
//
//  Created by Charles Schulze on 9/15/12.
//  Copyright 2012 CS54 INC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import <MediaPlayer/MediaPlayer.h>

@interface MenuSelectLevel : CCLayer
{
    CCMenuItemImage *menuButton;
    CCMenuItemImage *replayButton;
    CCMenuItemImage *nextButton;
    
    int pointsScoreAnimation;
    int pointTotalThisRound;
    
    NSString *lab;
    NSString *scoreLabelString;
    
    CCParticleSystemQuad *emitter;
    MPMoviePlayerController *player;
    UIButton *playButton;

}
+(CCScene *) scene;
@property (nonatomic,retain) CCLabelTTF *scoreLabel;
@property (nonatomic,retain) CCLabelTTF *scoreLabelStroke;
@end