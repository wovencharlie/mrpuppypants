//
//  IntroLayer.m
//  Squigglebottoms
//
//  Created by Charles Schulze on 9/29/12.
//  Copyright Charles Schulze 2012. All rights reserved.
//


// Import the interfaces
#import "IntroLayer.h"
#import "BaseLevel.h"
#import "SelectLevelScrollLayer.h"
#import "MenuSelectLevel.h"
#import "DDGameKitHelper.h"
#import "MenuPreferences.h"
#import "EndGoodBoy.h"
#import "MenuCredits.h"
#import "Appirater.h"
#import "DDGameKitHelper.h"
#import "DialogAreYouSure.h"
#import "TapForTap.h"
#import "AppDelegate.h"

#pragma mark - IntroLayer

// HelloWorldLayer implementation
@implementation IntroLayer

// Helper class method that creates a Scene with the HelloWorldLayer as the only child.
+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	IntroLayer *layer = [IntroLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

//
-(id) init
{
	if( (self=[super init])) {
		
        
        //[self addChild:vvController];
        
        [Flurry logEvent:@"Home Page Loaded"];
        
        		// ask director for the window size
		CGSize size = [[CCDirector sharedDirector] winSize];
		
		CCSprite *background;
		//[[CCDirector sharedDirector] setDisplayStats:NO];
        
        [[GameData sharedData] startMainScreenLoops];
        
        background = [CCSprite spriteWithFile:@"mrPantsTitleScreen.png"];
        background.anchorPoint = dccp(0,0);

        /*
		if( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
			background = [CCSprite spriteWithFile:@"mrPantsTitleScreen.png"];
            background.anchorPoint = ccp(0,0);
			//background.rotation = 90;
		} else {
			background = [CCSprite spriteWithFile:@"Default-Landscape~ipad.png"];
		}
         */
		//background.position = ccp(size.width/2, size.height/2);
		
		// add the label as a child to this Layer
		[self addChild: background];
        
        
        CCSpriteFrameCache *frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameCache addSpriteFramesWithFile:GENERIC_GRAPHICS_PLIST];
        [frameCache addSpriteFramesWithFile:SELECT_GAME_GRAPHICS_PLIST];
        [frameCache addSpriteFramesWithFile:BUTT_DANCE_PLIST];
        
        
//#define IMAGE_CREDITS_BUTTON @"credits-button.png"
//#define IMAGE_GAME_CENTER_BUTTON @"game-center-button.png"
        
        
        CCMenuItemImage *playButton = [CCMenuItemImage itemWithNormalSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_HOME_PLAY_GAME_BUTTON]]
                                            selectedSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_HOME_PLAY_GAME_BUTTON]]
                                                    target:self selector:@selector(menuButtonTapped:)];
        
        CCMenuItemImage *moreGames = [CCMenuItemImage itemWithNormalSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_HOME_MORE_GAMES_BUTTON]]
                                                             selectedSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_HOME_MORE_GAMES_BUTTON]]
                                                                     target:self selector:@selector(showAd:)];
        
        CCMenuItemImage *preferencesButton = [CCMenuItemImage itemWithNormalSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_PREFERENCES_BUTTON]]
                                                             selectedSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_PREFERENCES_BUTTON]]
                                                                     target:self selector:@selector(prefButtonTapped:)];
        
        CCMenuItemImage *gameCenter = [CCMenuItemImage itemWithNormalSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_GAME_CENTER_BUTTON]]
                                                             selectedSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_GAME_CENTER_BUTTON]]
                                                                     target:self selector:@selector(showGameCenter:)];
        
        CCMenuItemImage *creditsButton = [CCMenuItemImage itemWithNormalSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_CREDITS_BUTTON]]
                                                                selectedSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_CREDITS_BUTTON]]
                                                                        target:self selector:@selector(creditsTapped:)];

        
        //450 - 190
        
        
        /*
        NSString* ver60SysVer = @"6.0";
        NSString* currSysVer = [[UIDevice currentDevice] systemVersion];
        bool isOSVer60 = NO;
        if ([currSysVer rangeOfString:ver60SysVer].location == NSNotFound) {
            //isOSVer60 = NO;
        }
        */
        CCMenu *menu = nil;
        
        GameData *gameData = [GameData sharedData];
        
        //DDGameKitHelper *gameHelper = [DDGameKitHelper sharedGameKitHelper];
        
        menu = [CCMenu menuWithItems:playButton,gameCenter, nil];
        if(![gameData isGameCenterAvailable]){
            //menu = [CCMenu menuWithItems:playButton, moreGames, nil];
        }else{
            //menu = [CCMenu menuWithItems:playButton, moreGames,gameCenter, nil];
        }
        menu.position = dccp(225, 95);
        [menu alignItemsVertically];
        [self addChild:menu];
        
        //800 550
        int xPosModifier = 0;
        int yPosModifier = 0;
        
        xPosModifier = 0;
        if(size.width > 480 || size.height > 480)
        {
            xPosModifier = 35;
        }
        
        if(IS_IPAD){
            xPosModifier = 200;
            yPosModifier = 200;
        }
        CCMenu *utilityMenu = [CCMenu menuWithItems:creditsButton, preferencesButton, nil];
        
        if(size.width > 480 || size.height > 480){
            [utilityMenu alignItemsHorizontally];
        }else{
            [utilityMenu alignItemsVertically];
            yPosModifier = -30;
        }
        
        utilityMenu.position = dccp(400 + xPosModifier, 275 + yPosModifier);
        
        
        
        [self addChild:utilityMenu];
        
        
        if(!gameData.hasSeenCS54){
            gameData.hasSeenCS54 = YES;
            
            int width = size.width;
            int height = size.height;
            if(size.height > size.width){
                width = size.height;
                height = size.width;
            }
            cs54BG = [CCSprite spriteWithFile:IMAGE_CS54];
            cs54BG.position = ccp(width / 2, height / 2);
            [self addChild:cs54BG];
            [self performSelector:@selector(faseOutCS54) withObject:nil afterDelay:3];
        }
	}
	
	return self;
}
- (void)faseOutCS54{
    [cs54BG runAction:[CCFadeOut actionWithDuration:0.2f]];
    [self performSelector:@selector(removeCS54) withObject:nil afterDelay:0.2f];
}
- (void)removeCS54{
    [cs54BG removeFromParentAndCleanup:YES];
}
- (void)prefButtonTapped:(id)sender{
    //MenuPreferences
    CCTransitionFade *transition=[CCTransitionFade transitionWithDuration:.7 scene:[MenuPreferences scene]];
    [[CCDirector sharedDirector] replaceScene:transition];
    //[[GameData sharedData] tweetAboutGame];
}

- (void)creditsTapped:(id)sender{
    CCTransitionFade *transition=[CCTransitionFade transitionWithDuration:.7 scene:[MenuCredits scene]];
    [[CCDirector sharedDirector] replaceScene:transition];
    
}
- (void)showGameCenter:(id)sender{
    DDGameKitHelper *gameHelper = [DDGameKitHelper sharedGameKitHelper];
    [gameHelper showLeaderboard];

}

- (void)showAd:(id)sender{
//#define kPrimaryCurrencyID = @"f140e798-78fb-4d82-bb05-365a24d59547"	// Respect Points
//#define kSecondaryCurrencyID
    //RevMobFullscreen *fs = [[RevMobAds session] fullscreen];
    //fs.delegate = self.;
    //[fs showAd];
    //[[RevMobAds session] showFullscreen];
    
    AppController *app = (AppController*) [[UIApplication sharedApplication] delegate];
    [TapForTapInterstitial showWithRootViewController:[app navController]];
    //[TapForTapAppWall showWithRootViewController:[app navController]];
    //TapForTapAdView* adView = [[TapForTapAdView alloc] initWithFrame:CGRectMake(0, 0, 320,50)];
    //[[CCDirector sharedDirector].view addSubview:adView];
    
    //UIViewController *hsv = [[UIViewController alloc] init];
	//[[[CCDirector sharedDirector] view] addSubview:[hsv view]];
    
    //[TapForTapAppWall showWithRootViewController: hsv.parentViewController];
    
    
    //[[RevMobAds session] showFullscreen];
    //UIViewController *hsv = [[UIViewController alloc] init];
	//[[[CCDirector sharedDirector] view] addSubview:[hsv view]];
    
    //[TapjoyConnect showOffersWithCurrencyID:@"f140e798-78fb-4d82-bb05-365a24d59547" withViewController:hsv withCurrencySelector:NO];
}
- (void)menuButtonTapped:(id)sender
{
    CCTransitionFade *transition=[CCTransitionFade transitionWithDuration:.7 scene:[SelectLevelScrollLayer scene]];
    //MenuSelectLevel
    [[CCDirector sharedDirector] replaceScene:transition];
    
    //[self actionNextLevel];
}

-(void) onEnter
{
	[super onEnter];
	//[[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0 scene:[BaseLevel scene] ]];
}
@end
