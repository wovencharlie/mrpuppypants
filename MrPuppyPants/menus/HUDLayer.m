//
//  HUDLayer.m
//  MrPants
//
//  Created by Charles Schulze on 7/5/12.
//  Copyright 2012 CS54 INC. All rights reserved.
//

#import "HUDLayer.h"
#import "Constants.h"
#import "GameData.h"
@implementation HUDLayer

-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self=[super init])) 
    {
        CGSize size = [CCDirector sharedDirector].winSize;

        GameData *gameData = [GameData sharedData];
        
        CCSprite *menuButton = [CCSprite spriteWithSpriteFrameName:IMAGE_MENU_BUTTON];
        CCSprite *pauseButton = [CCSprite spriteWithSpriteFrameName:IMAGE_MENU_PAUSE_BUTTON];
        CCSprite *scoreTest = [CCSprite spriteWithSpriteFrameName:IMAGE_SCORE_TEST];

        int yOffset = 10;
        
        CGPoint coinPosition = ccp(22, 280);
        CGPoint timeToCompleteLevelPosition = ccp(305, 285 - yOffset);
        CGPoint besttimeToCompleteLevelPosition = ccp(305, 270 - yOffset);
        
        
        
        CGPoint moneyLabelPosition = ccp(0, 259);
        
        CGFloat largeFontSize = 18;
        CGFloat smallFontSize = 14;
        
        
        //[self addChild:pauseButton];

        int hudXMod = 0;
        int xPos = 455;
        int yPos = 295;
        if(IS_IPAD){
            yPos = 680 / 2;
            
            largeFontSize = 35;
            smallFontSize = 28;
            
            coinPosition = ccp(22 * 2, 670);
            moneyLabelPosition = ccp(30,640);
            timeToCompleteLevelPosition = ccp(700, 685 - yOffset);
            besttimeToCompleteLevelPosition = ccp(700, 655 - yOffset);
            //moneyLabelPosition = ccp(0, 259);
        }else if(size.width > 480)
        {
            hudXMod = 80;
            
            timeToCompleteLevelPosition = ccp(305 + hudXMod, 285 - yOffset);
            besttimeToCompleteLevelPosition = ccp(305 + hudXMod, 270 - yOffset);
        }
        
        menuButton.position = dccp(384 + hudXMod, 295);
        pauseButton.position = dccp((xPos + hudXMod), yPos);
        
        yPos = 252;
        if(IS_IPAD){
            yPos = 656 / 2;
        }
        
        scoreTest.position = dccp(395 + hudXMod, yPos);

        int labelWidth = 200;
        if(IS_IPAD){
            labelWidth = 250;
        }
		self.timeToCompleteLevel = [CCLabelTTF labelWithString:@"" dimensions:CGSizeMake(labelWidth, 40) hAlignment:kCCTextAlignmentRight fontName:@"Marker Felt" fontSize:smallFontSize];
        [self addChild:self.timeToCompleteLevel z:0];
		[self.timeToCompleteLevel setColor:ccc3(255,255,255)];
		self.timeToCompleteLevel.position = timeToCompleteLevelPosition;
        
        self.bestTimeToCompleteLevel = [CCLabelTTF labelWithString:@"" dimensions:CGSizeMake(labelWidth, 40) hAlignment:kCCTextAlignmentRight fontName:@"Marker Felt" fontSize:smallFontSize];
        [self addChild:self.bestTimeToCompleteLevel z:0];
		[self.bestTimeToCompleteLevel setColor:ccc3(255,255,255)];
		self.bestTimeToCompleteLevel.position = besttimeToCompleteLevelPosition;
        
        
        
        
        
        [self.bestTimeToCompleteLevel setString:[gameData returnBestTimeForLevel:[gameData returnLevel]]];
        
        
        
        
        

        int moneyLabelWidth = 76;
        if(IS_IPAD){
            moneyLabelWidth = 86;
        }
        
        self.moneyLabel = [CCLabelTTF labelWithString:@"$0" dimensions:CGSizeMake(moneyLabelWidth, 40) hAlignment:kCCTextAlignmentCenter fontName:@"Marker Felt" fontSize:largeFontSize];
		[self addChild:self.moneyLabel z:0];
		[self.moneyLabel setColor:ccc3(255,255,255)];
		self.moneyLabel.position = moneyLabelPosition;
        self.moneyLabel.anchorPoint = ccp(0,.5);
        
        
        CCSprite *coin = [CCSprite spriteWithSpriteFrameName:HUD_COIN];
        coin.position = coinPosition;
        coin.anchorPoint = ccp(0,0);
        [self addChild:coin];
        
        
        
        
        
        
        
        
        //rainEmitter.scale = 2.2;
        //364 295
        //431 295
        //CCSprite *mySprite = [CCSprite spriteWithFile:@"mr-pants-owner.png"];
        //[self addChild:mySprite];
        
       // mySprite.position = ccp(mySprite.contentSize.width / 2, screenSize.height - mySprite.contentSize.height / 2);
    }
    return self;
}

-(void)dealloc
{
    self.timeToCompleteLevel = nil;
    self.moneyLabel = nil;
    [super dealloc];
}
@end
