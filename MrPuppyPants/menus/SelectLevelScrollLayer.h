//
//  SelectLevelScrollLayer.h
//  MrPuppyPants
//
//  Created by Charlie Schulze on 12/28/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import <MediaPlayer/MediaPlayer.h>


@interface SelectLevelScrollLayer : CCLayer {
    NSUserDefaults *defaults;
    BOOL hasSeenIntroVideo;
    MPMoviePlayerController *player;
}
+(CCScene *) scene;
@end
