//
//  SelectLevelScrollLayer.m
//  MrPuppyPants
//
//  Created by Charlie Schulze on 12/28/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "SelectLevelScrollLayer.h"
#import "CCScrollLayer.h"
#import "Constants.h"
#import "MenuSelectLevel.h"
#import "IntroLayer.h"
#import "GameData.h"

#define HAS_SEEN_INTRO_VIDEO @"hasSeenIntroVideo_test"

@implementation SelectLevelScrollLayer
+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	SelectLevelScrollLayer *layer = [SelectLevelScrollLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
    
	// return the scene
	return scene;
}

-(id) init
{
    if((self = [super init]))
    {
        
        [Flurry logEvent:@"Load Sections page"];
        CGSize size = [CCDirector sharedDirector].winSize;
        
        int yPosition = 180;
        
        if(IS_IPAD){
            yPosition *= 2;
        }
        int xOffset = 180;
        if(size.height > 480 || size.width > 480)
        {
            xOffset = 320;
        }
        CCSprite *imageSprite = [CCSprite spriteWithFile:IMAGE_LEVEL_SELECT_MENU];
        imageSprite.anchorPoint = ccp(0,0);
        [self addChild:imageSprite];
        
        /////////////////////////////////////////////////
        // PAGE 1
        ////////////////////////////////////////////////
        // create a blank layer for page 1
        CCLayer *pageOne = [[CCLayer alloc] init];
        
        CCSpriteFrameCache *frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
        
        CCMenuItemImage *playButton = [CCMenuItemImage itemWithNormalSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_SELECT_GETTING_STARTED]]
                                                             selectedSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_SELECT_GETTING_STARTED]]
                                                                     target:self selector:@selector(goToFirstSection:)];
        CCMenu *menu = [CCMenu menuWithItems:playButton, nil];
        menu.position = ccp(size.width/2, yPosition);
        [menu alignItemsHorizontally];
        [pageOne addChild:menu];

        CCLayer *pageTwo = [[CCLayer alloc] init];

        CCMenuItemImage *playButton2 = [CCMenuItemImage itemWithNormalSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_SELECT_CITY_STREETS]]
                                                             selectedSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_SELECT_CITY_STREETS]]
                                                                     target:self selector:@selector(goToSecondSection:)];
        CCMenu *menu2 = [CCMenu menuWithItems:playButton2, nil];
        menu2.position = ccp(size.width/2, yPosition);
        [menu2 alignItemsHorizontally];
        
        [pageTwo addChild:menu2];
        
        
        
        CCLayer *pageThree = [[CCLayer alloc] init];
        
        CCMenuItemImage *playButton3 = [CCMenuItemImage itemWithNormalSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_SELECT_THE_HEIGHTS]]
                                                             selectedSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_SELECT_THE_HEIGHTS]]
                                                                     target:self selector:@selector(goToThirdSection:)];
        CCMenu *menu3 = [CCMenu menuWithItems:playButton3, nil];
        menu3.position = ccp(size.width/2, yPosition);
        [menu3 alignItemsHorizontally];
        
        [pageThree addChild:menu3];
        
        
        
        
        
        CCLayer *pageFour = [[CCLayer alloc] init];
        
        CCMenuItemImage *playButton4 = [CCMenuItemImage itemWithNormalSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_SELECT_BROKEN_CITY]]
                                                             selectedSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_SELECT_BROKEN_CITY]]
                                                                     target:self selector:@selector(goToFourthSection:)];
        CCMenu *menu4 = [CCMenu menuWithItems:playButton4, nil];
        menu4.position = ccp(size.width/2, yPosition);
        [menu4 alignItemsHorizontally];
        
        [pageFour addChild:menu4];
        
        
        CCMenuItemImage *backButton = [CCMenuItemImage itemWithNormalSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_BACK_ARROW_BUTTON]]
                                                             selectedSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_BACK_ARROW_BUTTON]]
                                                                     target:self selector:@selector(backButtonTapped:)];
        CCMenu *backButtonMenu = [CCMenu menuWithItems:backButton, nil];
        backButtonMenu.position = dccp(60, 60);
        [backButtonMenu alignItemsHorizontally];
        [self addChild:backButtonMenu];
        
        // now create the scroller and pass-in the pages (set widthOffset to 0 for fullscreen pages)
        //CCScrollLayer *scroller = [[CCScrollLayer alloc] initWithLayers:[NSMutableArray arrayWithObjects: pageOne,pageTwo,pageThree,pageFour,nil] widthOffset: xOffset];
        CCScrollLayer *scroller = [[CCScrollLayer alloc] initWithLayers:[NSMutableArray arrayWithObjects: pageOne,nil] widthOffset: xOffset];
        // finally add the scroller to your scene
        [self addChild:scroller];
        
        
        defaults = [NSUserDefaults standardUserDefaults];
        hasSeenIntroVideo = NO;//[defaults boolForKey:HAS_SEEN_INTRO_VIDEO];
        
	}
    
   	return self;
}


- (bool)canGotoSection:(int)level
{
    return [[GameData sharedData] canYouGoToTheFirstLevelOfThisSection:level];
}

- (void)goToFirstSection:(id)sender
{
    CCTransitionFade *transition=[CCTransitionFade transitionWithDuration:.7 scene:[MenuSelectLevel scene]];
    [[CCDirector sharedDirector] replaceScene:transition];
    
}
- (void)goToSecondSection:(id)sender
{
    return;
    CCTransitionFade *transition=[CCTransitionFade transitionWithDuration:.7 scene:[MenuSelectLevel scene]];
    [[CCDirector sharedDirector] replaceScene:transition];
    return;
    //[TestFlight passCheckpoint:@"Go to section 2"];
    [Flurry logEvent:@"Go to section 2"];
    if([self canGotoSection:2])
    {
        CCTransitionFade *transition=[CCTransitionFade transitionWithDuration:.7 scene:[MenuSelectLevel scene]];
        [[CCDirector sharedDirector] replaceScene:transition];
        return;
    }
    NSLog(@"Could not load level 2");
}
- (void)goToThirdSection:(id)sender
{
    return;
    //[TestFlight passCheckpoint:@"Go to section 3"];
    [Flurry logEvent:@"Go to section 3"];
    if([self canGotoSection:3])
    {
        CCTransitionFade *transition=[CCTransitionFade transitionWithDuration:.7 scene:[MenuSelectLevel scene]];
        [[CCDirector sharedDirector] replaceScene:transition];
        return;
    }
    NSLog(@"Could not load level 3");
}
- (void)goToFourthSection:(id)sender
{
    return;
    //[TestFlight passCheckpoint:@"Go to section 4"];
    [Flurry logEvent:@"Go to section 4"];
    if([self canGotoSection:4])
    {
        CCTransitionFade *transition=[CCTransitionFade transitionWithDuration:.7 scene:[MenuSelectLevel scene]];
        [[CCDirector sharedDirector] replaceScene:transition];
        return;
    }
    NSLog(@"Could not load level 4");
}

- (void)backButtonTapped:(id)sender
{
    CCTransitionFade *transition=[CCTransitionFade transitionWithDuration:.7 scene:[IntroLayer scene]];
    [[CCDirector sharedDirector] replaceScene:transition];
    //[TestFlight passCheckpoint:@"GO BACK to Home Page"];
    [Flurry logEvent:@"SelectLevelScrollLayer : GO BACK to Home Page"];
}


- (void)menuButtonTapped:(id)sender
{
    CCTransitionFade *transition=[CCTransitionFade transitionWithDuration:.7 scene:[MenuSelectLevel scene]];
    //MenuSelectLevel
    [[CCDirector sharedDirector] replaceScene:transition];
    //[self actionNextLevel];
}


@end
