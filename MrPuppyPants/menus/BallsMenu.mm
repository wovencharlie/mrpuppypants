//
//  BallsMenu.m
//  MrPants
//
//  Created by Charles Schulze on 9/2/12.
//  Copyright 2012 CS54 INC. All rights reserved.
//

#import "BallsMenu.h"
#import "CCRadioMenu.h"
#import "Constants.h"
#import "GameData.h"

@implementation BallsMenu
@synthesize delegate,orangeLabel,blackLabel,redLabel,blueLabel,radioMenu;
@synthesize orangeBallButton,blackBallButton,redBallButton,blueBallButton;

- (CGPoint)convertPoint:(CGPoint)point {
    return point;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        return ccp(32 + point.x*2, 64 + point.y*2);
    } else {
        return point;
    }
}
-(void)disableBlackBall{
    
}
- (id) initInPosition:(CGPoint)pos
{
	if((self = [super init]))
    {

        
        CCSpriteFrameCache *shared = [CCSpriteFrameCache sharedSpriteFrameCache];
 
        
        self.orangeBallButton = [CCMenuItemImage
                                 itemWithNormalSprite:[CCSprite spriteWithSpriteFrame:[shared spriteFrameByName:IMAGE_BALL_PLATE]]
                                 selectedSprite:[CCSprite spriteWithSpriteFrame:[shared spriteFrameByName:IMAGE_BALL_PLATE_SELECTED]]
                                 disabledSprite:[CCSprite spriteWithSpriteFrame:[shared spriteFrameByName:IMAGE_BALL_PLATE_DISABLED]]
                                 target:self selector:@selector(ball1Selected:)];
        
        self.blackBallButton = [CCMenuItemImage
                                 itemWithNormalSprite:[CCSprite spriteWithSpriteFrame:[shared spriteFrameByName:IMAGE_BALL_PLATE]]
                                 selectedSprite:[CCSprite spriteWithSpriteFrame:[shared spriteFrameByName:IMAGE_BALL_PLATE_SELECTED]]
                                 disabledSprite:[CCSprite spriteWithSpriteFrame:[shared spriteFrameByName:IMAGE_BALL_PLATE_DISABLED]]
                                 target:self selector:@selector(ball2Selected:)];
        
        self.redBallButton = [CCMenuItemImage
                                 itemWithNormalSprite:[CCSprite spriteWithSpriteFrame:[shared spriteFrameByName:IMAGE_BALL_PLATE]]
                                 selectedSprite:[CCSprite spriteWithSpriteFrame:[shared spriteFrameByName:IMAGE_BALL_PLATE_SELECTED]]
                                 disabledSprite:[CCSprite spriteWithSpriteFrame:[shared spriteFrameByName:IMAGE_BALL_PLATE_DISABLED]]
                                 target:self selector:@selector(ball3Selected:)];
        
        self.blueBallButton = [CCMenuItemImage
                                 itemWithNormalSprite:[CCSprite spriteWithSpriteFrame:[shared spriteFrameByName:IMAGE_BALL_PLATE]]
                                 selectedSprite:[CCSprite spriteWithSpriteFrame:[shared spriteFrameByName:IMAGE_BALL_PLATE_SELECTED]]
                                 disabledSprite:[CCSprite spriteWithSpriteFrame:[shared spriteFrameByName:IMAGE_BALL_PLATE_DISABLED]]
                                 target:self selector:@selector(ball4Selected:)];

        
       
        self.orangeBall = [CCSprite spriteWithSpriteFrameName:HUD_ORANGE_BALL];
        [self.orangeBallButton addChild:self.orangeBall];
        self.orangeBall.position = dccp(25,40);
        
        self.blackBall = [CCSprite spriteWithSpriteFrameName:HUD_BLACK_BALL];
        [self.blackBallButton addChild:self.blackBall];
        self.blackBall.position = dccp(25,40);
        
        self.redBall = [CCSprite spriteWithSpriteFrameName:HUD_RED_BALL];
        [self.redBallButton addChild:self.redBall];
        self.redBall.position = dccp(25,40);
        
        self.blueBall = [CCSprite spriteWithSpriteFrameName:HUD_BLUE_BALL];
        [self.blueBallButton addChild:self.blueBall];
        self.blueBall.position = dccp(25,40);
        
        int fontSize = 20;
        CGPoint labelPosition;
        labelPosition = dccp(25,6);
        if(IS_IPAD)
        {
            fontSize *= 2;
            labelPosition = dccp(25,16);
        }
        
        
        self.orangeLabel = [CCLabelTTF labelWithString:@"2" dimensions:CGSizeMake(49, 40) hAlignment:kCCTextAlignmentCenter fontName:@"Marker Felt" fontSize:fontSize];

        self.orangeLabel.color = ccc3(255,255,255);
        self.orangeLabel.position = labelPosition;
        self.orangeLabel.tag = 1;
        [self.orangeBallButton addChild:self.orangeLabel];
        
        self.blackLabel = [CCLabelTTF labelWithString:@"2" dimensions:CGSizeMake(49, 40) hAlignment:kCCTextAlignmentCenter fontName:@"Marker Felt" fontSize:fontSize];
        self.blackLabel.color = ccc3(255,255,255);
        self.blackLabel.position = labelPosition;
        self.blackLabel.tag = 1;
        [self.blackBallButton addChild:self.blackLabel];
        
        self.redLabel = [CCLabelTTF labelWithString:@"2" dimensions:CGSizeMake(49, 40) hAlignment:kCCTextAlignmentCenter fontName:@"Marker Felt" fontSize:fontSize];
        
        self.redLabel.color = ccc3(255,255,255);
        self.redLabel.position = labelPosition;
        self.redLabel.tag = 1;
        [self.redBallButton addChild:self.redLabel];
        
        self.blueLabel = [CCLabelTTF labelWithString:@"2" dimensions:CGSizeMake(49, 40) hAlignment:kCCTextAlignmentCenter fontName:@"Marker Felt" fontSize:fontSize];
        self.blueLabel.color = ccc3(255,255,255);
        self.blueLabel.position = labelPosition;
        self.blueLabel.tag = 1;
        [self.blueBallButton addChild:self.blueLabel];

        
        
        int xPos = 180;
        int yPos = 280;
        if(IS_IPAD){
            xPos = 360;
            yPos = 670;
        }
        
        self.radioMenu = [CCRadioMenu menuWithItems:self.orangeBallButton, self.blackBallButton,self.redBallButton,self.blueBallButton, nil];
        self.radioMenu.position = ccp(xPos, yPos);//[self convertPoint:];
        [self.radioMenu alignItemsHorizontally];
        self.radioMenu.selectedItem_ = self.orangeBallButton;
        [self.orangeBallButton selected];
        [self addChild:self.radioMenu];
	}
    
   	return self;
}

-(void)ball1Selected:(id)sender
{
    [self.delegate ball1Selected];
    NSMutableString* ballSelected = [NSMutableString stringWithFormat:@"Ball %d Selected", 1];
    [Flurry logEvent:ballSelected];
}

-(void)ball2Selected:(id)sender
{
    [self.delegate ball2Selected];
    NSMutableString* ballSelected = [NSMutableString stringWithFormat:@"Ball %d Selected", 2];
    [Flurry logEvent:ballSelected];

}
-(void)ball3Selected:(id)sender
{
    [self.delegate ball3Selected];
    NSMutableString* ballSelected = [NSMutableString stringWithFormat:@"Ball %d Selected", 3];
    [Flurry logEvent:ballSelected];

}

-(void)ball4Selected:(id)sender
{
    [self.delegate ball4Selected];
    NSMutableString* ballSelected = [NSMutableString stringWithFormat:@"Ball %d Selected", 4];
    [Flurry logEvent:ballSelected];

}

-(void)dealloc
{
    self.orangeLabel = nil;
    self.blackLabel = nil;
    self.redLabel = nil;
    self.blueLabel = nil;
    self.radioMenu = nil;
    
    self.orangeBall = nil;
    self.blackBall = nil;
    self.redBall = nil;
    self.blueBall = nil;
    
    self.orangeBallButton = nil;
    self.blackBallButton = nil;
    self.redBallButton = nil;
    self.blueBallButton = nil;

    [super dealloc];
}

@end
