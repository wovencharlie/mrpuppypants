//
//  HUDLayer.h
//  MrPants
//
//  Created by Charles Schulze on 7/5/12.
//  Copyright 2012 CS54 INC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
//#import "SnoozeMeter.h"
@interface HUDLayer : CCLayer {
    //SnoozeMeter *snoozeMeter;
    
}
@property (nonatomic,retain) CCLabelTTF *moneyLabel;
@property (nonatomic,retain) CCLabelTTF *timeToCompleteLevel;
@property (nonatomic,retain) CCLabelTTF *bestTimeToCompleteLevel;

@end
