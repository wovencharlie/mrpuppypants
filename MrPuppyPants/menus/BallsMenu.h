//
//  BallsMenu.h
//  MrPants
//
//  Created by Charles Schulze on 9/2/12.
//  Copyright 2012 CS54 INC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "CCRadioMenu.h"
@protocol BallsMenuDelegate <NSObject>

- (void)ball1Selected;
- (void)ball2Selected;
- (void)ball3Selected;
- (void)ball4Selected;
@end



@interface BallsMenu : CCNode
{
    CCMenuItem *orangeBallButton;
    CCMenuItem *blackBallButton;
    CCMenuItem *redBallButton;
    CCMenuItem *blueBallButton;
}
@property (nonatomic,retain) CCLabelTTF *orangeLabel;
@property (nonatomic,retain) CCLabelTTF *blackLabel;
@property (nonatomic,retain) CCLabelTTF *redLabel;
@property (nonatomic,retain) CCLabelTTF *blueLabel;
@property (nonatomic,retain) CCRadioMenu *radioMenu;


@property (nonatomic,retain) CCMenuItem *orangeBallButton;
@property (nonatomic,retain) CCMenuItem *blackBallButton;
@property (nonatomic,retain) CCMenuItem *redBallButton;
@property (nonatomic,retain) CCMenuItem *blueBallButton;

@property (nonatomic,retain) CCSprite *orangeBall;
@property (nonatomic,retain) CCSprite *blackBall;
@property (nonatomic,retain) CCSprite *redBall;
@property (nonatomic,retain) CCSprite *blueBall;

@property (nonatomic, assign) id  <BallsMenuDelegate> delegate;


- (id) initInPosition:(CGPoint)pos;
@end
