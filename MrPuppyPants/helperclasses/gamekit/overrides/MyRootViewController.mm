//
//  MyRootViewController.m
//  MrPuppyPants
//
//  Created by Charlie Schulze on 3/3/13.
//
//

#import "MyRootViewController.h"
#import "GameData.h"
@implementation MyRootViewController

-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskLandscape;
}

- (BOOL)shouldAutorotate {
    return YES;
}

@end
