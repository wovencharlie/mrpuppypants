//
//  GKMatchmakerViewController-LandscapeOnly.m
//  CatRace
//
//  Created by Ray Wenderlich on 4/23/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "GKMatchmakerViewController-LandscapeOnly.h"

@implementation GKMatchmakerViewController (LandscapeOnly)

-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskLandscape;
}

- (BOOL)shouldAutorotate {
    return YES;
}

@end
