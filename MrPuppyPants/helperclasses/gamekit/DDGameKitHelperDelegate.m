//
//  DDGameKitHelperDelegate.h
//  Version 1.0

#import "DDGameKitHelperDelegate.h"
#import "GKAchievementHandler.h"
#import "GameData.h"
@implementation DDGameKitHelperDelegate

// return true if score1 is greater than score2
// modify this if your scoreboard is reversed (low scores on top)
-(bool) compare:(int64_t)score1 to:(int64_t)score2
{
    return score1 > score2;
}

// display new high score using GKAchievement class
-(void) onSubmitScore:(int64_t)score;
{
    [[GKAchievementHandler defaultHandler] notifyAchievementTitle:@"New High Score!!!" andMessage:[NSString stringWithFormat:@"%d", score]];
}

-(void) onSubmitBestTime:(int64_t)score;
{
    int64_t a = score;
    int totalSeconds = (int) a;
    
    int seconds = totalSeconds % 60;
    int minutes = (totalSeconds / 60) % 60;
    int hours = totalSeconds / 3600;
    /*
    return ;
    
    int mins = (int) (currentTime / 60.0);
    currentTime -= mins * 60;
    int secs = (int) (currentTime);
    currentTime -= secs;
    //int fraction = currentTime * 10.0;
    */
    //return ;
    
    NSString *puppyTime = [NSString stringWithFormat:@"%02d:%02d:%02d",hours, minutes, seconds];//[NSString stringWithFormat:@"%u:%02u",mins,secs];//[[GameData sharedData] formattedBestTimeForLevel:score];
    [[GKAchievementHandler defaultHandler] notifyAchievementTitle:@"New Best Puppy Time!!!" andMessage:[NSString stringWithFormat:@"%@", puppyTime]];
}

// display the achievement using GKAchievement class
-(void) onReportAchievement:(GKAchievement*)achievement
{
    DDGameKitHelper* gkHelper = [DDGameKitHelper sharedGameKitHelper];
    [[GKAchievementHandler defaultHandler] notifyAchievement:[gkHelper getAchievementDescription:achievement.identifier]];
}

@end
