//
//  MenuBetweenLevels.h
//  MrPants
//
//  Created by Charles Schulze on 9/15/12.
//  Copyright 2012 CS54 INC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "GameData.h"
@interface MenuBuyGuns : CCLayer
{
    CCMenuItemImage *gun1Button;
    CCMenuItemImage *gun2Button;
    CCMenuItemImage *gun3Button;
    CCMenuItemImage *backButton;
    
    CCMenuItemImage *buyMore;
    
    CCMenu *buyMoreMenu;

    GameData *gameData;
    
    BOOL purchaseGun1;
    BOOL purchaseGun2;
    BOOL purchaseGun3;
}
+(CCScene *) scene;
@property (nonatomic,retain) CCLabelTTF *coinLabel;

-(void)completePurchase;
-(void)cancelPurchase;
@end
