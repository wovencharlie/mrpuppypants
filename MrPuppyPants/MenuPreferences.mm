//
//  MenuBetweenLevels.m
//  MrPants
//
//  Created by Charles Schulze on 9/15/12.
//  Copyright 2012 CS54 INC. All rights reserved.
//

#import "MenuPreferences.h"
#import "GameData.h"
#import "MenuSelectLevel.h"
#import "Constants.h"
#import "ButtDance.h"
#import "BaseLevel.h"
#import "MenuBuyGuns.h"
#import "IntroLayer.h"

@implementation MenuPreferences

+(CCScene *) scene
{
	CCScene *scene = [CCScene node];
    MenuPreferences *layer = [MenuPreferences node];
    [scene addChild: layer];
    return scene;
}

-(id) init
{
    if((self = [super init]))
    {
        //175
        CGSize size = [[CCDirector sharedDirector] winSize];
        
        gameData = [GameData sharedData];
        //int currentLevel = [gameData returnLevel];
        CGPoint menuButtonPosition = ccp(105, 280);
        CGPoint mainMenusPosition = ccp(size.width/2,(size.height - 40)/2);
        
        if(IS_IPAD){
            menuButtonPosition = ccp(210, 680);
        }
        
        CCSprite *imageSprite = [CCSprite spriteWithFile:IMAGE_BG_BUY_GUNS_SCREEN];
        imageSprite.position = ccp(size.width / 2,size.height/2);
        [self addChild:imageSprite];
        
        CCSpriteFrameCache *frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
       
        musicOnButton = [CCMenuItemImage itemWithNormalSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_MUSIC_ON_BUTTON]]
                                                                               selectedSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_MUSIC_ON_BUTTON]]];
                                                               
        musicOffButton = [CCMenuItemImage itemWithNormalSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_MUSIC_OFF_BUTTON]]
                                                                                selectedSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_MUSIC_OFF_BUTTON]]];
        
        effectsOnButton = [CCMenuItemImage itemWithNormalSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_EFFECTS_ON_BUTTON]]
                                                                                 selectedSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_EFFECTS_ON_BUTTON]]];
        
        effectsOffButton = [CCMenuItemImage itemWithNormalSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_EFFECTS_OFF_BUTTON]]
                                                                                  selectedSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_EFFECTS_OFF_BUTTON] ]];
        
        musicToggleButton = [CCMenuItemToggle itemWithTarget:self selector:@selector(musicOnOffTapped:) items:musicOnButton,musicOffButton, nil];
        effectsToggleButton = [CCMenuItemToggle itemWithTarget:self selector:@selector(effectsOnOffTapped:) items:effectsOnButton,effectsOffButton, nil];

        [musicToggleButton setSelectedIndex:0];
        [effectsToggleButton setSelectedIndex:0];
        
        if([gameData areSoundFXMuted]){
            [musicToggleButton setSelectedIndex:1];
        }
        
        if([gameData areAmbientFXMuted]){
            [effectsToggleButton setSelectedIndex:1];
        }
        
        CCMenuItemImage *backButton = [CCMenuItemImage itemWithNormalSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_BL_BACK_BUTTON]]
                                                             selectedSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_BL_BACK_BUTTON]]
                                                                     target:self selector:@selector(backButtonTapped:)];
        
        
        CCMenu *menuLower = [CCMenu menuWithItems:backButton, nil];
        menuLower.position = menuButtonPosition;
        [menuLower alignItemsHorizontally];
        [self addChild:menuLower];
        
        CCMenu *menu = [CCMenu menuWithItems:musicToggleButton, effectsToggleButton, nil];
        menu.position = mainMenusPosition;
        [menu alignItemsVertically];
        [self addChild:menu];
        menu.anchorPoint = ccp(0,0);
        
    }
    
   	return self;
}


- (void)musicOnOffTapped:(id)sender{

    //CCTransitionSplitRows *transition=[CCTransitionFade transitionWithDuration:.2 scene:[MenuBuyGuns scene]];
    //[[CCDirector sharedDirector] replaceScene:transition];
    if([gameData areSoundFXMuted]){
        [gameData turnSoundFXOn];
    }else{
        [gameData turnSoundFXOff];
    }

}

- (void)effectsOnOffTapped:(id)sender
{
    //CCTransitionSplitRows *transition=[CCTransitionSplitRows transitionWithDuration:.7 scene:[MenuSelectLevel scene]];
    //[[CCDirector sharedDirector] replaceScene:transition];
    if([gameData areAmbientFXMuted]){
        [gameData turnAmbientFXOn];
    }else{
        [gameData turnAmbientFXOff];
    }
}

- (void)backButtonTapped:(id)sender
{
    [[CCDirector sharedDirector] replaceScene:[IntroLayer scene]];
}

-(void)dealloc
{
    [super dealloc];
}

@end
