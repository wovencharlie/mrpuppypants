//
//  AdViewController.m
//  MrPuppyPants
//
//  Created by Charlie Schulze on 3/3/13.
//
//

#import "AdViewController.h"

@interface AdViewController ()

@end

@implementation AdViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    [[RevMobAds session] showFullscreen];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[RevMobAds session] showFullscreen];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
