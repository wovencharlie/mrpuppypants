//
//  AdViewController.h
//  MrPuppyPants
//
//  Created by Charlie Schulze on 3/3/13.
//
//

#import <UIKit/UIKit.h>
#import <RevMobAds/RevMobAds.h>

@interface AdViewController : UIViewController <UIApplicationDelegate, RevMobAdsDelegate>

@end
