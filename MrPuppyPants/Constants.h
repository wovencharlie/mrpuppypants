#define PTM_RATIO 32

#define kPrimaryCurrencyID = @"f140e798-78fb-4d82-bb05-365a24d59547"	// Respect Points
#define kSecondaryCurrencyID = @"4b4e99d4-c485-4f5d-a096-4f8cb8854205"	// Disrespect Points

#define useShapeOfSourceImageButSlightlySmaller 9


#define ENABLE_EMITTERS 1
#define IS_DEV_ONLY 0

#if IS_DEV_ONLY
#define ENABLE_AUDIO 0
#define ENABLE_MUSIC 0
#else
#define ENABLE_AUDIO 1
#define ENABLE_MUSIC 1
#endif

#define GC_LEADERBOARD @"com.cs54.mrpuppypantslite.mainleaderboard"
#define GC_TIME_LEADER @"com.cs54.mrpuppypantslite.timeleaderboard"

#define GC_USE_WORMHOLE @"com.cs54.mrpuppypantslite.usewormhole"
#define GC_USE_TWO_WORMHOLES @"com.cs54.mrpuppypantslite.usetwowormholes"

#define GC_HALF_WAY @"com.cs54.mrpuppypantslite.halfway"
#define GC_WIN_HOUNDS @"com.cs54.mrpuppypantslite.winhounds"
/*
#define GC_LEADERBOARD @"com.cs54.mrpuppypants.mainleaderboard"
#define GC_TIME_LEADER @"time.leaderboard"

#define GC_USE_WORMHOLE @"com.cs54.usewormhole"
#define GC_USE_TWO_WORMHOLES @"com.cs54.usetwowormholes"
*/

#define Z_CROSSHAIR 600
#define Z_PIPE 554
#define Z_DOTS 555

#define BULLET_POWER_ORANGE 12
#define BULLET_DENSETY_ORANGE 20.0
#define BULLET_RESTITUTION_ORANGE 0.8
#define BULLET_FRICTION_ORANGE 1.0
#define BULLET_DRAW_AMOUNT_ORANGE 17
#define BULLET_ALT_DRAW_STEPS_ORANGE 2
#define BULLET_DRAW_DOT_AFTER_ORANGE 1

#define BULLET_POWER_BLACK 10
#define BULLET_DENSETY_BLACK 50
#define BULLET_RESTITUTION_BLACK 0.5
#define BULLET_FRICTION_BLACK 1.0
#define BULLET_DRAW_AMOUNT_BLACK 16
#define BULLET_ALT_DRAW_STEPS_BLACK 2
#define BULLET_DRAW_DOT_AFTER_BLACK 1

#define BULLET_POWER_RED 14
#define BULLET_DENSETY_RED 19
#define BULLET_RESTITUTION_RED 0.8
#define BULLET_FRICTION_RED 1.0
#define BULLET_DRAW_AMOUNT_RED 17
#define BULLET_ALT_DRAW_STEPS_RED 2
#define BULLET_DRAW_DOT_AFTER_RED 1

#define BULLET_POWER_BLUE 17
#define BULLET_DENSETY_BLUE 100
#define BULLET_RESTITUTION_BLUE 0.3
#define BULLET_FRICTION_BLUE 1.0
#define BULLET_DRAW_AMOUNT_BLUE 16
#define BULLET_ALT_DRAW_STEPS_BLUE 2
#define BULLET_DRAW_DOT_AFTER_BLUE 1


#define DRAW_AMOUNT 18
#define ALT_DRAW_STEPS 2
#define DRAW_DOT_AFTER 1

//#define START_WITH_LEVEL 4;

#define TWEET_KEY @"tweetMe_prod_"
#define SHARE_KEY @"shareMe_prod_"
#define USER_COIN_COUNT @"userCoinCount_TEST"

#define CROSSHAIR_OFFSET_X 20
#define CROSSHAIR_OFFSET_Y 50
#define CROSSHAIR_MULTIPLYER 3

#define TAG_DEATH_FLOOR 333

#define BALLS_POINT_WORTH 2500


//#define breakEffectNone 0
//#define breakEffectSmokePuffs 1
//#define breakEffectExplosion 2

#define FIRST_TIME_STAR_RATING @"first_time_star_rating_7"
#define STAR_KEY @"starKey7"

#define IMAGE_STAR_3 @"win-3-star.png"
#define IMAGE_STAR_2 @"win-2-star.png"
#define IMAGE_STAR_1 @"win-1-star.png"

#define WARP_1_ANIMATION_IDLE_FRAME @"warp-enter-1.swf/0000"
#define WARP_1_ANIMATION_BASE_FRAME @"warp-enter-1.swf/0"

#define WARP_2_ANIMATION_IDLE_FRAME @"warp-enter.swf/0000"
#define WARP_2_ANIMATION_BASE_FRAME @"warp-enter.swf/0"

#define CAT_ANIMATION_IDLE_FRAME @"cat_v1.swf/0000"
#define CAT_ANIMATION_BASE_FRAME @"cat_v1.swf/0"


#define CAT_2_ANIMATION_IDLE_FRAME @"cat_v2.swf/0000"
#define CAT_2_ANIMATION_BASE_FRAME @"cat_v2.swf/0"

#define CAT_3_ANIMATION_IDLE_FRAME @"cat_v3.swf/0000"
#define CAT_3_ANIMATION_BASE_FRAME @"cat_v3.swf/0"

#define MRPANTS_BUTT_DANCE_FRAME @"win-butt-dance.swf/0"


#define MR_PANTS_WALK_IDLE_FRAME @"mrpants-walking.swf/0004"
#define MR_PANTS_WALK_BASE_FRAME @"mrpants-walking.swf/0"

#define MR_PANTS_IDLE_FRAME @"mr-pants-idle.swf/0000"
#define MR_PANTS_BASE_FRAME @"mr-pants-idle.swf/0"
#define MRPANTS_BUTT_DANCE @"win-butt-dance.swf/0000"
#define JETPACK_BASE_FRAME @"jetpack.swf/0"
#define JETPACK_IDLE_FRAME @"jetpack.swf/0000"

#define GUN_EXPLOSION_BASE_FRAME @"gunExplosion.swf/0"
#define GUN_EXPLOSION_IDLE_FRAME @"gunExplosion.swf/0011"

#define TAG_BREAKABLE_ITEM 202
#define TAG_WARP_HOLE 205
#define TAG_WARP_HOLE_02 206
#define TAG_BOMB 2002


#define TAG_BREAKABLE_ENEMY 204
#define TAG_BREAKABLE_ENEMY_NON_HURTING 2041

#define TAG_NON_BREAKABLE_OBJECT 203
#define TAG_NON_BREAKABLE__NOT_HURTING_OBJECT 207
#define TAG_WORLD_LEVEL 20945

#define TAG_BULLET 201
#define TAG_WREAKING_BALL 201

#define COLLIDE_GROUP_BULLET 445
#define COLLIDE_GROUP_PLANK 446
#define COLLID_GROUP_PLANK_HIT 445
#define COLLIDE_GENERIC_ALWAYS_COLLIDE 447
#define COLLIDE_DEATH_FLOOR 666
#define COLLIDE_WARP_HOLE 777
#define COLLIDE_WARP_HOLE_02 778

//Audio
#define AUDIO_CAT_MEOW_01 @"catMeow01.mp3"
#define LOOP_HOME_SCREEN @"loop_home_screen.mp3"
#define LOOP_SECTION_01 @"Variation 3 - Loop.aif"
#define LOOP_SECTION_02 @"Variation 2 - Loop.aif"
#define LOOP_SECTION_03 @"Variation 1 - Loop.aif"

#define LOOP_CITY_TRAFFIC @"ambient_traffic.mp3"

#define AUDIO_WIN @"win-audio.mp3"
#define AUDIO_LOSE @"you-lose.mp3"

#define AUDIO_SNORING @"snoring.mp3"
#define AUDIO_SPRING_GUN @"springGun.mp3"
#define AUDIO_CAT_EXPLODE @"catExplode.mp3"
#define AUDIO_CAT_BURP @"one-cat-sound.mp3"
#define AUDIO_BALL_BOUNCE @"ballBounce01.mp3"
#define AUDIO_GLASS_BREAK @"bulb-break.mp3"

#define SHARED_SECRET @"1912279bfa4140efb1dac40c7dbbb757"

//Menu Overlays
#define MENU_IMAGE_YOU_DONT_HAVE_ENOUGH @"youDontHaveEnough.png"
//Images
#define IMAGE_CROSSHAIRS @"cross-hairs.png"

#define IMAGE_CS54 @"cs54.png"
#define IMAGE_BACKGROUND @"main-bg-02.png"
#define IMAGE_SUN @"sun.png"
#define IMAGE_MID_BACKGROUND @"mid-plane.png"
#define IMAGE_CIRCLE_SKY_BACKGROUND @"circle-sky.png"

#define IMAGE_SLIDE_PAW @"slide-paw.png"
#define IMAGE_WORMHOLE @"wormhole.png"
#define IMAGE_EASIER_SHOT @"easier-shot.png"
#define IMAGE_BALL_EXIT @"ball-exit.png"




#define IMAGE_MR_PANTS_PAWS_GUN @"paws-and-gun.png"
#define IMAGE_MR_PANTS_PAWS_GUN_02 @"paws-and-gun-02.png"
#define IMAGE_MR_PANTS_PAWS_GUN_03 @"paws-and-gun-03.png"
#define IMAGE_MR_PANTS_PAWS_GUN_04 @"paws-and-gun-04.png"


#define IMAGE_DOT @"explode5.png"
#define IMAGE_SPRING @"spring.png"
#define IMAGE_ORANGE_BALL @"orange-spike-ball.png"
#define IMAGE_WARP_EXIT_1 @"warp-exit-1.png"


#define IMAGE_FIRE_BUTTON_SPRITE @"fireButton.png"
#define IMAGE_FIRE_BUTTON_ACTIVE_SPRITE @"fireButton.png"
#define IMAGE_FIRE_BUTTON_PRESS_SPRITE @"fireButton.png"


#define IMAGE_JETPACK_SLIDER_BG @"jetpack-slider-bg.png"
#define IMAGE_JETPACK_SLIDER_THUMB @"jetpack-slider-button.png"

#define IMAGE_SKIP_BUTTON @"skip-button.png"
#define IMAGE_RESET_BUTTON @"reset-button.png"
#define IMAGE_PLAY_GAME_BUTTON @"play-game-button.png"
#define IMAGE_HOME_PLAY_GAME_BUTTON @"home-play-button.png"
#define IMAGE_HOME_MORE_GAMES_BUTTON @"home-more-games-button.png"
#define IMAGE_PREFERENCES_BUTTON @"preferences-button.png"

#define IMAGE_CREDITS_BUTTON @"credits-button.png"
#define IMAGE_GAME_CENTER_BUTTON @"game-center-button.png"
#define IMAGE_ARE_YOU_SURE_DIALOG @"are-you-sure.png"

#define IMAGE_BUY_BUY_BUTTON @"buy-buy-button.png"
#define IMAGE_BUY_CLOSE_BUTTON @"buy-close-button.png"
#define IMAGE_DOWN_ARROW_BUTTON @"down-arrow-button.png"
#define IMAGE_BACK_ARROW_BUTTON @"back-arrow-button.png"


//plists
#define WARP_2_PLIST @"warp-enter.plist"
#define MR_PANTS_PLIST @"testPants.plist"
#define MR_PANTS_WALK_PLIST @"mrPantsWalking.plist"
#define CAT_ANIMATION_PLIST @"cat_v1.plist"
#define CAT_2_ANIMATION_PLIST @"cat_v2.plist"
#define CAT_3_ANIMATION_PLIST @"cat_v3.plist"

#define JETPACK_PLIST @"jetpack.plist"
#define GUN_EXPLOSION_PLIST @"gunExplosion.plist"

#define BULLET_PLIST @"MagicFire.plist"
#define WARP_EMITTER @"warpEmitter.plist"

#define ROCKET_PLIST @"rocket2.plist"
#define BLOCK_BREAK_EMITTER_PLIST @"block_break_emitter.plist"
#define GENERIC_GRAPHICS_PLIST @"genericGraphics.plist"
#define SELECT_GAME_GRAPHICS_PLIST @"selectGameCategory.plist"
#define BUTT_DANCE_PLIST @"win-butt-dance.plist"

#define SIZE_CAT_WIDTH 22
#define SIZE_CAT_HEIGHT 22


#define GUN_POS_Y_OFFSET -5
#define GUN_POS_X_OFFSET -5
#define ROCKET_Y_OFFSET -10
//--------

#define IMAGE_SELECT_GETTING_STARTED @"select-gettingStarted.png"
#define IMAGE_SELECT_CITY_STREETS @"select-cityStreets.png"
#define IMAGE_SELECT_THE_HEIGHTS @"select-theHeights.png"
#define IMAGE_SELECT_BROKEN_CITY @"select-brokenCity.png"


#define IMAGE_BALL_PLATE @"ball-tile-ns.png"
#define IMAGE_BALL_PLATE_SELECTED @"ball-tile-s.png"
#define IMAGE_BALL_PLATE_DISABLED @"ball-tile-disabled.png"

#define HUD_ORANGE_BALL @"hud-orange-ball.png"
#define HUD_BLACK_BALL @"hud-black-ball.png"
#define HUD_RED_BALL @"hud-red-ball.png"
#define HUD_BLUE_BALL @"hud-blue-ball.png"

#define HUD_COIN @"coin.png"

#define IMAGE_1000 @"1000.png"
#define IMAGE_5000 @"5000.png"

#define IMAGE_BLACK_BALL @"black-ball.png"
#define IMAGE_BLUE_BALL @"blue-ball.png"
#define IMAGE_RED_BALL @"red-ball.png"
#define IMAGE_SPIRAL @"spiral.png"


#define IMAGE_PRESS_HERE @"instruct-press-here.png"
#define IMAGE_PRESS_FIRE @"instruct-press-to-fire.png"
#define IMAGE_CHANGE_BALL @"change-ball.png"

#define IMAGE_TRY_AGAIN @"try-again.png"
#define IMAGE_NEXT_LEVEL_BUTTON @"next-level-btn.png"

#define IMAGE_GREEN_BUTTON @"green-button.png"
#define IMAGE_PLANK_01 @"long-plank-01.png"

#define IMAGE_TRASH_CAN @"trashCan.png"
#define IMAGE_TRASH_CAN_BROKEN @"trashCan.png"
#define PLIST_TRASH_CAN_FIRE @"trashcan-fire.plist"

#define IMAGE_CRANE_HOOK @"crane-hook.png"
#define IMAGE_WREAKING_BALL @"wreaking-ball.png"

#define LEVELS_PLIST @"levels_physEdit_v1.plist"

#define IMAGE_LEVEL @"bld_level_"
#define IMAGE_GROUND_LEVEL @"gr_bld_level_"

#define IMAGE_GROUND_LEVEL_3_PIPE @"gr_bld_level_03-pipe.png"

#define IMAGE_BACKGROUND_01 @"background-01.png"

#define IMAGE_MENU_BUTTON @"menu-button.png"
#define IMAGE_MENU_PAUSE_BUTTON @"pause-button.png"
#define IMAGE_SCORE_TEST @"score-test.png"

#define IMAGE_BL_TWEET @"tweet-for-1000.png"

#define IMAGE_BL_TWEET_5000 @"tweet-5000.png"
#define IMAGE_BL_TWEET_NORMAL @"tweet-normal.png"

#define IMAGE_BL_FB_NORMAL @"share-normal.png"
#define IMAGE_BL_FB_5000 @"share-5000.png"

#define IMAGE_ALERT_ONE @"one-alert.png"

#define IMAGE_BL_WEAPONS_BUTTON @"button_bl_weapons.png"
#define IMAGE_BL_REPLAY_BUTTON @"button_bl_replay.png"
#define IMAGE_BL_MENU_BUTTON @"button_bl_menu.png"
#define IMAGE_BL_NEXT_BUTTON @"next-v2.png"
#define IMAGE_BL_BACK_BUTTON @"button_bl_back.png"

#define IMAGE_CONTINUE_BUTTON @"continue.png"


#define IMAGE_RESTART_BUTTON @"restart-button.png"
#define IMAGE_RESUME_BUTTON @"resume-button.png"
#define IMAGE_EXIT_BUTTON @"exit-menu-button.png"

#define IMAGE_MUSIC_ON_BUTTON @"button_music_on.png"
#define IMAGE_MUSIC_OFF_BUTTON @"button_music_off.png"
#define IMAGE_EFFECTS_ON_BUTTON @"button_effects_on.png"
#define IMAGE_EFFECTS_OFF_BUTTON @"button_effects_off.png"

#define IMAGE_GUN_1_BUTTON @"gun-bowling.png"
#define IMAGE_GUN_2_BUTTON @"gun-red.png"
#define IMAGE_GUN_3_BUTTON @"gun-blue.png"

#define IMAGE_BS_MENU_BUTTON @"bs_menuButton.png"
#define IMAGE_REPLAY_BUTTON @"bs_replayButton.png"
#define IMAGE_NEXT_BUTTON @"bs_nextButton.png"


#define IMAGE_BETWEEN_LEVELS_SCREEN @"betweenLevelsScreen.png"
#define IMAGE_BG_BUY_GUNS_SCREEN @"backgroundBuyGuns.png"
#define IMAGE_BG_THANKS_SCREEN @"background-thanks.png"
#define IMAGE_BG_PAUSE_SCREEN @"grey-bg-overlay.png"
#define IMAGE_BG_BLACK @"bg-black.png"
#define IMAGE_BG_GOOD_BOY @"good-boy.png"

#define IMAGE_NEW_HIGH_SCORE @"new-high-score.png"

#define IMAGE_SELECT_LEVEL_BTN @"level-select-menu-button.png"
#define IMAGE_SELECT_LEVEL_BTN_1S @"level-select-menu-button-1s.png"
#define IMAGE_SELECT_LEVEL_BTN_2S @"level-select-menu-button-2s.png"
#define IMAGE_SELECT_LEVEL_BTN_3S @"level-select-menu-button-3s.png"
#define IMAGE_SELECT_BACK_BTN @"cannon.png"

#define IMAGE_SELECT_LEVEL_BTN_SELECTED @"level-select-menu-button-selected.png"
#define IMAGE_LOCKED_LEVEL @"level-select-menu-locked-button.png"
#define LEVEL_COMPLETED_ALL_TIME @"levelsCompletedAllTime_dev02"

#define IMAGE_LEVEL_SELECT_MENU @"level-select-menu.png"
#define PLIST_SELECT_LEVEL_SMOKE @"menuSelectLevelSmoke.plist"

#define IMAGE_PLANK_01 @"long-plank-01.png"
#define IMAGE_PLANK_02 @"long-plank-02.png"
#define IMAGE_PLANK_03 @"long-plank-03.png"


#define IMAGE_TV @"tv.png"
#define IMAGE_TV_BROKEN @"tv.png"

#define IMAGE_BBQ @"bbq.png"
#define IMAGE_BBQ_BROKEN @"bbq.png"

#define IMAGE_CRATE_01 @"crate01.png"
#define IMAGE_CRATE_01_BROKEN @"crate01.png"

#define IMAGE_CRATE_BOMB @"crateBomb.png"
#define IMAGE_CRATE_BOMB_BROKEN @"crateBomb.png"

#define IMAGE_FISH @"fishy.png"
#define IMAGE_FISH_BROKEN @"fishy.png.png"

#define IMAGE_CINDERBLOCK_01 @"cinderBlock.png"
#define IMAGE_CINDERBLOCK_01_BROKEN @"cinderBlock.png"

#define IMAGE_BARREL_01 @"barrel.png"
#define IMAGE_BARREL_01_BROKEN @"barrel.png"

#define IMAGE_PLATFORM_01 @"platform_01.png"

#define IMAGE_PLATFORM_02 @"platform_02.png"



#define TO_BREAK_CAT 1
#define TO_BREAK_PLANK_1 12
#define TO_BREAK_PLANK_2 12
#define TO_BREAK_PLANK_3 12
#define TO_BREAK_CRATE 12
#define TO_BREAK_TV 3
#define TO_BREAK_BARREL 13
#define TO_BREAK_CINDER 10
#define TO_BREAK_FISH 2
#define TO_BREAK_BBQ 3

#define SIZE_TV_WIDTH 30
#define SIZE_TV_HEIGHT 24

#define SIZE_CRATE_01_WIDTH 23
#define SIZE_CRATE_01_HEIGHT 23

#define SIZE_PLANK_01_WIDTH 144
#define SIZE_PLANK_01_HEIGHT 11

#define SIZE_PLANK_02_WIDTH 96
#define SIZE_PLANK_02_HEIGHT 11

#define SIZE_PLANK_03_WIDTH 48
#define SIZE_PLANK_03_HEIGHT 11

#define SIZE_BARREL_WIDTH 23
#define SIZE_BARREL_HEIGHT 23

#define SIZE_CINDER_WIDTH 11
#define SIZE_CINDER_HEIGHT 23

#define SIZE_FISH_WIDTH 28
#define SIZE_FISH_HEIGHT 21

#define SIZE_BBQ_WIDTH 26
#define SIZE_BBQ_HEIGHT 21

#define SIZE_PLATFORM_01_WIDTH 48
#define SIZE_PLATFORM_01_HEIGHT 11

#define SIZE_PLATFORM_02_WIDTH 96
#define SIZE_PLATFORM_02_HEIGHT 11

#define SIZE_WARP_SPIRAL_WIDTH 30
#define SIZE_WARP_SPIRAL_HEIGHT 30

#define LOCKED_TAG 10987

#define EMITTER_TAG 10

