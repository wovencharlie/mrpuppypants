//
//  MenuBetweenLevels.m
//  MrPants
//
//  Created by Charles Schulze on 9/15/12.
//  Copyright 2012 CS54 INC. All rights reserved.
//

#import "MenuSelectLevel.h"
#import "GameData.h"
#import "Constants.h"
#import "SelectLevelScrollLayer.h"
#import "BaseLevel.h"
#import "CCScrollLayer.h"
#import "SimpleAudioEngine.h"

@implementation MenuSelectLevel

#define HAS_SEEN_INTRO_VIDEO @"hasSeenIntroVideo_test"

+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	MenuSelectLevel *layer = [MenuSelectLevel node];
	
	// add layer as a child to scene
	[scene addChild: layer];
    
	// return the scene
	return scene;
}

-(NSString *)getButtonSpriteForLevel:(int)level withScore:(int)score {
    
    
    NSMutableArray *scoreArray = [NSMutableArray arrayWithObjects:@"2",@"3",@"5",@"5",@"5",@"7",@"4",@"3",@"6",@"4",@"5",@"3",@"5",@"3",@"4",@"3",@"6",@"6",@"6",@"4",@"3",@"3",@"4",@"6",nil];
    GameData *gameData = [GameData sharedData];
    
    if(!score){
        return IMAGE_SELECT_LEVEL_BTN;
    }
    
    int catCount = [[scoreArray objectAtIndex:level-1] integerValue];
    int maxCatScore = catCount * 5000;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    int ballCountToUseInCalc = 10;
    int maxBallScore = 9;
    int maxScore;

    if(![defaults boolForKey:FIRST_TIME_STAR_RATING]){
        if(level >  8){
            ballCountToUseInCalc = 13;
        }
        
        maxBallScore = (ballCountToUseInCalc - catCount) * 2500;
        
        maxScore = maxCatScore + maxBallScore;
        
        maxScore += 5000;
        
        int threeStarScore = maxScore;
        int twoStarScore = (threeStarScore - 3500);
        
        if(score >= maxScore){
            [gameData setStarRating:3 forLevel:level];
            return IMAGE_SELECT_LEVEL_BTN_3S;
        }else if(score >= twoStarScore){
            [gameData setStarRating:2 forLevel:level];
            return IMAGE_SELECT_LEVEL_BTN_2S;
        }
        
        [gameData setStarRating:1 forLevel:level];
        NSLog(@"STARS : THIS IS THE FIRST TIME WE CALC STAR RATING"); 
    }else
    {
        NSLog(@"STARS : GETTING HISTORICAL DATA"); 
        int starRating = [gameData getStarRatingForLevel:level];
        if(starRating == 3){
            return IMAGE_SELECT_LEVEL_BTN_3S;
        }else if(starRating == 2){
            return IMAGE_SELECT_LEVEL_BTN_2S;
        }
    }
    
    return IMAGE_SELECT_LEVEL_BTN_1S;
}




-(id) init
{
    if((self = [super init]))
    {
        CCLayer *pageOne = [[CCLayer alloc] init];
        CCLayer *pageTwo = [[CCLayer alloc] init];
        CCLayer *pageThree = [[CCLayer alloc] init];
        
        [Flurry logEvent:@"Select Level page loaded"];
        GameData *gameData = [GameData sharedData];
        
        [gameData startMainScreenLoops];
        
        CCSprite *imageSprite = [CCSprite spriteWithFile:IMAGE_LEVEL_SELECT_MENU];
        imageSprite.anchorPoint = ccp(0,0);
        [self addChild:imageSprite];
        
        emitter = [[CCParticleSystemQuad alloc]initWithFile:PLIST_SELECT_LEVEL_SMOKE];
        emitter.position = dccp(230,14);
        //[self addChild:emitter];
        
        CGFloat largeFont = 28;
        CGFloat smallFont = 16;
        
        int labelHeight = 40;
        int scoreLabelY = 7;
        int levelPosYAdjust = 0;
        if(IS_IPAD){
            largeFont = 50;
            smallFont = 28;
            labelHeight = 60;
            scoreLabelY = 10;
            levelPosYAdjust = 4;
        }
       
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        int levelsCompleted = [defaults integerForKey:LEVEL_COMPLETED_ALL_TIME];
        levelsCompleted ++;
                
        CCSpriteFrameCache *frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
        
        for(int outer = 0;outer<3;outer++)
        {
            CCMenu *menu = [CCMenu menuWithItems:nil];
            CCMenu *menuLower = [CCMenu menuWithItems: nil];
            
            for(int i = 0;i<8;i++)
            {
                CCMenuItem *menuItem;
                int sectionMultiplier = (8 * outer);
                
                if((i + sectionMultiplier) < levelsCompleted)
                {
                    
                    int scoreForLevel = [gameData returnHighScoreForLevel:(i + 1) + sectionMultiplier];
                    int levelInLoop = (i + 1);
                    NSString *buttonSprite = [self getButtonSpriteForLevel:levelInLoop + sectionMultiplier withScore:scoreForLevel];
                    
                    menuItem = [CCMenuItemImage itemWithNormalSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:buttonSprite]]
                                                      selectedSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:buttonSprite]]
                                                              target:self selector:@selector(menuButtonTapped:)];
                    
                    menuItem.tag = i + 1 + sectionMultiplier;
                    
                    NSString *level = [NSString stringWithFormat:@"%d", (i + 1) + sectionMultiplier];
                    
                    CCLabelTTF *levelLabel = [CCLabelTTF labelWithString:level dimensions:CGSizeMake(200, labelHeight) hAlignment:kCCTextAlignmentCenter fontName:@"Marker Felt" fontSize:largeFont];
                    
                    [menuItem addChild:levelLabel z:0];
                    [levelLabel setColor:ccc3(255,255,255)];
                    levelLabel.position = dccp(48, 60);
                    
                    levelLabel = [CCLabelTTF labelWithString:level dimensions:CGSizeMake(200, labelHeight) hAlignment:kCCTextAlignmentCenter fontName:@"Marker Felt" fontSize:largeFont];
                    
                    [menuItem addChild:levelLabel z:0];
                    [levelLabel setColor:ccc3(0,0,0)];
                    levelLabel.position = dccp(49, 61);
                    
                    level = @"play";
                    if(scoreForLevel > 0)
                    {
                        level = [NSString stringWithFormat:@"%d", scoreForLevel];
                    }
                    
                    
                    CCLabelTTF *scoreLabel= [CCLabelTTF labelWithString:level dimensions:CGSizeMake(200, labelHeight) hAlignment:kCCTextAlignmentCenter fontName:@"Marker Felt" fontSize:smallFont];
                    [menuItem addChild:scoreLabel z:0];
                    [scoreLabel setColor:ccc3(0,0,0)];
                    scoreLabel.position = dccp(48, scoreLabelY);
                    
                }
                else
                {
                    menuItem = [CCMenuItemImage itemWithNormalSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_LOCKED_LEVEL]]
                                                      selectedSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_LOCKED_LEVEL]]
                                                              target:self selector:@selector(menuButtonTapped:)];
                    
                    menuItem.tag = LOCKED_TAG;
                }
                
                if(i < 4)
                {
                    [menu addChild:menuItem];
                }
                else
                {
                    [menuLower addChild:menuItem];
                }
                
            }
            
            CGSize size = [[CCDirector sharedDirector] winSize];
            int yPosMultiplier = 1;
            if(IS_IPAD)
            {
                yPosMultiplier = 2;
            }
            
            if(outer == 0)
            {
                menu.position = ccp(size.width/2, 240 * yPosMultiplier);
                [menu alignItemsHorizontally];
                [pageOne addChild:menu];
                
                menuLower.position = ccp(size.width/2, 120 * yPosMultiplier);
                [menuLower alignItemsHorizontally];
                [pageOne addChild:menuLower];
            }
            else if(outer == 1)
            {
                menu.position = ccp(size.width/2, 240 * yPosMultiplier);
                [menu alignItemsHorizontally];
                [pageTwo addChild:menu];
                
                menuLower.position = ccp(size.width/2, 120 * yPosMultiplier);
                [menuLower alignItemsHorizontally];
                [pageTwo addChild:menuLower];
            }
            else
            {
                menu.position = ccp(size.width/2, 240 * yPosMultiplier);
                [menu alignItemsHorizontally];
                [pageThree addChild:menu];
                
                menuLower.position = ccp(size.width/2, 120 * yPosMultiplier);
                [menuLower alignItemsHorizontally];
                [pageThree addChild:menuLower];
            }
        }
        
        [defaults setBool:YES forKey:FIRST_TIME_STAR_RATING];
        [defaults synchronize];
        
        // now create the scroller and pass-in the pages (set widthOffset to 0 for fullscreen pages)
        CCScrollLayer *scroller = [[CCScrollLayer alloc] initWithLayers:[NSMutableArray arrayWithObjects: pageOne,pageTwo,pageThree,nil] widthOffset: 0];
        // finally add the scroller to your scene
        [self addChild:scroller];
        
        NSLog(@"game return level %i",gameData.returnLevel);
        if(gameData.returnLevel > 8 && gameData.returnLevel<17){
            [scroller moveToPage:2];
        }else if(gameData.returnLevel > 16){
            [scroller moveToPage:3];
        }else{
            [scroller moveToPage:1];
        }
        
        
        
        CCMenuItemImage *backButton = [CCMenuItemImage itemWithNormalSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_BACK_ARROW_BUTTON]]
                                                             selectedSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_BACK_ARROW_BUTTON]]
                                                                     target:self selector:@selector(backButtonTapped:)];
        
        CCMenu *backButtonMenu = [CCMenu menuWithItems:backButton, nil];
        backButtonMenu.position = dccp(35, 35);
        [backButtonMenu alignItemsHorizontally];
        [self addChild:backButtonMenu];
        
	}
    
   	return self;
}

- (void)backButtonTapped:(id)sender
{
    CCTransitionFade *transition=[CCTransitionFade transitionWithDuration:.7 scene:[SelectLevelScrollLayer scene]];
    [[CCDirector sharedDirector] replaceScene:transition];
    
    [Flurry logEvent:@"GO BACK to Select Level Page"];
    
}


- (void)menuButtonTapped:(id)sender
{
    CCMenuItem *menuItem = sender;
    if(menuItem.tag == 1){
        //play video
        CCSprite *black = [CCSprite spriteWithFile:IMAGE_BG_BLACK];
        [self addChild:black];
        CGSize size = [[CCDirector sharedDirector] winSize];
        black.position = ccp(size.width / 2,size.height / 2);
        //IMAGE_BG_BLACK
        [self playIntroVideo];
        return;
    }
    if(menuItem.tag != LOCKED_TAG)
    {
        [[GameData sharedData] loadThisLevel:menuItem.tag];
        
        int currentLevel = [[GameData sharedData] returnLevel];
        NSMutableString* ballSelected = [NSMutableString stringWithFormat:@"Menu Select : Load Level %d",currentLevel];
        [Flurry logEvent:ballSelected];
        //NSLog(@"%@",ballSelected);
        
        CCTransitionFade *transition=[CCTransitionFade transitionWithDuration:1 scene:[BaseLevel scene]];
        [[CCDirector sharedDirector] replaceScene:transition];
        [[GameData sharedData] loadThisLevel:menuItem.tag];
    }
}

- (void)playIntroVideo
{
    UIView *view = [[CCDirector sharedDirector] view];
    [view setUserInteractionEnabled:YES];
    
    playButton = [[UIButton buttonWithType:UIButtonTypeRoundedRect] retain];
    UIImage *skipImage = [UIImage imageNamed:@"video-skip.png"];

    //[skipImage drawInRect:CGRectMake(0, 0, skipImage.size.width, skipImage.size.height) blendMode:kCGBlendModeClear alpha:1.0];
    
    [playButton setBackgroundImage:skipImage forState:UIControlStateNormal];
    
    //[playButton setTitle:@"" forState:UIControlStateNormal];
    playButton.backgroundColor = [UIColor clearColor];
    [playButton addTarget:self action:@selector(playbackFinished:) forControlEvents:UIControlEventTouchUpInside];
    [playButton setUserInteractionEnabled:YES];
    
    [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playbackFinished:) name:MPMoviePlayerPlaybackDidFinishNotification object:nil];
    
    NSURL* movieURL =  [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"mr_puppy_pants_video_lg" ofType:@"m4v"]];
    player = [[MPMoviePlayerController alloc] initWithContentURL:movieURL];
    [player setFullscreen:YES];

    CGSize size = [CCDirector sharedDirector].winSize;
    int vidWidth = 480;
    if(size.height > 480 || size.width > 480)
    {
        vidWidth = 568;
    }
    
    if(IS_IPAD){
        player.view.frame = CGRectMake(0, 0, 1024, 768);
        playButton.frame = CGRectMake(1024 - 100, 768 - 50, skipImage.size.width, skipImage.size.height);
    }else{
        player.view.frame = CGRectMake(0, 0, vidWidth, 320);
        playButton.frame = CGRectMake(vidWidth - 100, 320 - 50, skipImage.size.width, skipImage.size.height);
    }

    player.controlStyle = MPMovieControlStyleNone;
    [player play];

    
    [[[CCDirector sharedDirector] view] addSubview:player.view];
    [[[CCDirector sharedDirector] view] addSubview:playButton];
    //playButton

}

- (void)playbackFinished:(id)sender
{
    [playButton removeFromSuperview];
    [playButton release];
    playButton = nil;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:nil];
    //[[NSNotificationCenter defaultCenter removeObserver:self forKeyPath:MPMoviePlayerPlaybackDidFinishNotification];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:YES forKey:HAS_SEEN_INTRO_VIDEO];
    [defaults synchronize];
    
    UIView *view = [[CCDirector sharedDirector] view];
    [view setUserInteractionEnabled:YES];
    [player.view removeFromSuperview];
    //[[[CCDirector sharedDirector] view] removeFromSuperview:player.view];
    
    [player stop];
    [player release];
    player = nil;
    
    [Flurry logEvent:@"Go to section 1 after video play"];
    [[GameData sharedData] loadThisLevel:1];
    
    int currentLevel = [[GameData sharedData] returnLevel];
    NSMutableString* ballSelected = [NSMutableString stringWithFormat:@"Menu Select : Load Level %d",currentLevel];
    //[TestFlight passCheckpoint:ballSelected];
    NSLog(@"%@",ballSelected);
    
    CCTransitionFade *transition=[CCTransitionFade transitionWithDuration:1 scene:[BaseLevel scene]];
    [[CCDirector sharedDirector] replaceScene:transition];
    [[GameData sharedData] loadThisLevel:1];
}


-(void) dealloc
{
    [emitter release];
    emitter = nil;
    [super dealloc];
}

@end
