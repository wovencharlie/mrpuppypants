//
//  MenuBetweenLevels.m
//  MrPants
//
//  Created by Charles Schulze on 9/15/12.
//  Copyright 2012 CS54 INC. All rights reserved.
//

#import "MenuBetweenLevels.h"
#import "GameData.h"
#import "MenuSelectLevel.h"
#import "Constants.h"
#import "ButtDance.h"
#import "BaseLevel.h"
#import "MenuBuyGuns.h"
#import "Appirater.h"
#import "SocialHelper.h"
#import <SystemConfiguration/SCNetworkReachability.h>
#include <netinet/in.h>
#import "AppDelegate.h"
#import "TapForTap.h"

@implementation MenuBetweenLevels

+(CCScene *) scene
{
	CCScene *scene = [CCScene node];
    MenuBetweenLevels *layer = [MenuBetweenLevels node];
    [scene addChild: layer];
    return scene;
}
- (void) resumeBackgroundMusic{
    [[SimpleAudioEngine sharedEngine] resumeBackgroundMusic];
}


-(id) init
{
    if((self = [super init]))
    {
        //175
        CGSize size = [[CCDirector sharedDirector] winSize];
        
        //insert butt dance
        GameData *gameData = [GameData sharedData];
        currentLevel = [gameData returnLevel];
        
        if(currentLevel == 1){
            [gameData submitAchievementName:GC_USE_WORMHOLE percent:100];
        }
        
        if(currentLevel == 6){
            [gameData submitAchievementName:GC_USE_TWO_WORMHOLES percent:100];
            [Appirater userDidSignificantEvent:YES];
        }
        
        if(currentLevel == 10){
            //[Appirater userDidSignificantEvent:YES];
        }
        
        if(currentLevel != 1 && currentLevel !=2 && currentLevel != 24){
            if([[GameData sharedData] shouldShowAd]){
                AppController *app = (AppController*) [[UIApplication sharedApplication] delegate];
                [TapForTapInterstitial showWithRootViewController:[app navController]];
                //[TapForTapAppWall showWithRootViewController:[app navController]];
            }
        }
        if(currentLevel == 12){
            [gameData submitAchievementName:GC_HALF_WAY percent:100];
            [Appirater userDidSignificantEvent:YES];
        }
        if(currentLevel == 22){
            [Appirater userDidSignificantEvent:YES];
        }
        if(currentLevel == 24){
            [gameData submitAchievementName:GC_WIN_HOUNDS percent:100];
        }
        
        //
        
        [[GameData sharedData] levelUp];
        
        if(ENABLE_MUSIC && ![[GameData sharedData]areSoundFXMuted]){
            //[[SimpleAudioEngine sharedEngine] playEffect:AUDIO_WIN];
            [[SimpleAudioEngine sharedEngine] pauseBackgroundMusic];
            [self performSelector:@selector(resumeBackgroundMusic) withObject:nil afterDelay:4.0];
        }
        
        //dccp(442 + xPosModifier,114);
        
        CGPoint mainMenusPosition = ccp(125,(size.height/2) + 30);
        CGPoint nextMenuPosition = ccp(size.width/2,50);
        CGPoint buttonDancePosition = ccp(418,112);
        
        int labelYPos = 178;
        int xOffset = 150;
        
        
        CGPoint scoreLabelPosition = ccp((xOffset + size.width + 60)/2, labelYPos - 115);
        CGPoint highScoreLabelPosition = ccp((xOffset + size.width + 60)/2, labelYPos - 95);
        CGPoint timeLabelPosition = ccp((xOffset + size.width)/2, labelYPos + 50);
        CGPoint bestTimeLabelPosition = ccp((xOffset + size.width)/2, labelYPos + 25);
        starRatingPosition = ccp((xOffset + size.width)/2, labelYPos + 100);
        
        CGPoint coinBonusLabelPosition = ccp(50 + ((xOffset + size.width)/2)+70, labelYPos - 5);
        CGPoint coinThisLevelLabelPosition = ccp(((xOffset + size.width)/2)+70, labelYPos - 25);
        CGPoint coinLabelPosition = ccp(((xOffset + size.width)/2)+70, labelYPos - 45);
        
        CGPoint coinPosition = ccp(((xOffset + size.width)/2)-67, labelYPos -20);
        
        CGFloat largeFontSize = 22;
        CGFloat mediumFontSize = 18;
        
        if(size.width > 480){
            buttonDancePosition = ccp(460,114);
        }
        
        if(IS_IPAD){
            labelYPos += 240;
            xOffset *= 2;
            
            scoreLabelPosition = ccp((xOffset + size.width)/2, labelYPos - 180);
            highScoreLabelPosition = ccp((xOffset + size.width)/2, labelYPos - 215);
            timeLabelPosition = ccp((xOffset + size.width)/2, labelYPos + 100);
            bestTimeLabelPosition = ccp((xOffset + size.width)/2, labelYPos + 50);
            starRatingPosition = ccp((xOffset + size.width)/2, labelYPos + 200);
            
            
            coinBonusLabelPosition = ccp(50 + ((xOffset + size.width)/2)+70, labelYPos - 10);
            coinThisLevelLabelPosition = ccp(((xOffset + size.width)/2)+70, labelYPos - 50);
            coinLabelPosition = ccp(((xOffset + size.width)/2)+70, labelYPos - 90);
            
            coinPosition = ccp(((xOffset + size.width)/2)-100, labelYPos -60);
            
            
            largeFontSize = 42;
            mediumFontSize = 30;
            
            mainMenusPosition = ccp(250,(size.height/2) + 60);
            nextMenuPosition = ccp(size.width /2,100);
            
            buttonDancePosition = ccp(880,222);
        }
        
        CCSprite *imageSprite = [CCSprite spriteWithFile:IMAGE_BETWEEN_LEVELS_SCREEN];
        imageSprite.position = ccp(size.width / 2,size.height/2);
        [self addChild:imageSprite];
        
        tweetImageName = IMAGE_BL_TWEET_5000;
        fbImageName = IMAGE_BL_FB_5000;
        
        if(gameData.hasTweeted){
            tweetImageName = IMAGE_BL_TWEET_NORMAL;
        }
        if(gameData.hasFacebooked){
            fbImageName = IMAGE_BL_FB_NORMAL;
            
        }
        
        
        CCSpriteFrameCache *frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
        
        weaponsButton = [CCMenuItemImage itemWithNormalSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_BL_WEAPONS_BUTTON]]
                                               selectedSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_BL_WEAPONS_BUTTON]]
                                                       target:self selector:@selector(weaponsButtonTapped:)];
        
        
        
        menuButton = [CCMenuItemImage itemWithNormalSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_BL_MENU_BUTTON]]
                                            selectedSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_BL_MENU_BUTTON]]
                                                    target:self selector:@selector(menuButtonTapped:)];
        
        replayButton = [CCMenuItemImage itemWithNormalSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_BL_REPLAY_BUTTON]]
                                              selectedSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_BL_REPLAY_BUTTON]]
                                                      target:self selector:@selector(replayButtonTapped:)];
        
        
        tweetButton = [CCMenuItemImage itemWithNormalSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:tweetImageName]]
                                             selectedSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:tweetImageName]]
                                                     target:self selector:@selector(tweetAboutGame:)];
        
        nextButton = [CCMenuItemImage itemWithNormalSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_BL_NEXT_BUTTON]]
                                            selectedSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_BL_NEXT_BUTTON]]
                                                    target:self selector:@selector(nextButtonTapped:)];
        
        if(gameData.isRunningiOS6){
            fbButton = [CCMenuItemImage itemWithNormalSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:fbImageName]]
                                              selectedSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:fbImageName]]
                                                      target:self selector:@selector(facebookClicked:)];
        }else{
            fbButton = [CCMenuItemImage itemWithNormalSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:tweetImageName]]
                                              selectedSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:tweetImageName]]
                                                      target:self selector:@selector(tweetAboutGame:)];
        }
        
        
        
        CCMenu *menu = [CCMenu menuWithItems:weaponsButton, menuButton, replayButton, nil];
        
        menu.position = mainMenusPosition;
        [menu alignItemsVertically];
        [self addChild:menu];
        menu.anchorPoint = ccp(0,0);
        
        if(currentLevel != 24){
            CCMenu *menuLower = [CCMenu menuWithItems:tweetButton, nextButton, fbButton, nil];
            menuLower.position = nextMenuPosition;
            [menuLower alignItemsHorizontally];
            [self addChild:menuLower];
        }else{
            CCMenu *menuLower = [CCMenu menuWithItems:tweetButton, fbButton, nil];
            menuLower.position = nextMenuPosition;
            [menuLower alignItemsHorizontally];
            [self addChild:menuLower];
        }
        
        
        
        
        self.bestTimeLabel = [CCLabelTTF labelWithString:@"" dimensions:CGSizeMake(300, 40) hAlignment:kCCTextAlignmentCenter fontName:@"Marker Felt" fontSize:mediumFontSize];
		[self addChild:self.bestTimeLabel z:0];
		self.bestTimeLabel.position = bestTimeLabelPosition;
        if(gameData.isNewBestTime){
            [self.bestTimeLabel setString:@"NEW BEST TIME!"];
        }else{
            [self.bestTimeLabel setString:[gameData returnBestTimeForLevel:currentLevel]];
        }
        
        self.timeLabel = [CCLabelTTF labelWithString:@"" dimensions:CGSizeMake(300, 40) hAlignment:kCCTextAlignmentCenter fontName:@"Marker Felt" fontSize:largeFontSize];
        [self addChild:self.timeLabel z:0];
        self.timeLabel.position = timeLabelPosition;
        [self.timeLabel setString:[gameData returnCurrentTimeForLevel]];
        
        
        self.coinBonusLabel = [CCLabelTTF labelWithString:@"" dimensions:CGSizeMake(300, 40) hAlignment:kCCTextAlignmentLeft fontName:@"Marker Felt" fontSize:mediumFontSize];
        [self addChild:self.coinBonusLabel z:0];
        self.coinBonusLabel.position = coinBonusLabelPosition;
        self.coinBonusLabel.color = ccc3(255,207,8);
        [self.coinBonusLabel setString:[NSString stringWithFormat:@"Time Coin Bonus +%i",[gameData returnCoinMultiplier]]];
        if([gameData returnCoinMultiplier] == 1){
            [self.coinBonusLabel setString:@"Too Slow. No Coin Bonus"];
        }
        
        self.coinLabel = [CCLabelTTF labelWithString:@"" dimensions:CGSizeMake(200, 40) hAlignment:kCCTextAlignmentLeft fontName:@"Marker Felt" fontSize:mediumFontSize];
        [self addChild:self.coinLabel z:0];
        self.coinLabel.position = coinLabelPosition;
        NSNumber *myDoubleNumber = [NSNumber numberWithDouble:[gameData returnTotalCoinsAvailable]];
        [self.coinLabel setString:[NSString stringWithFormat:@"Total  $%@",[myDoubleNumber stringValue]]];
        
        
        self.coinThisLevelLabel = [CCLabelTTF labelWithString:@"" dimensions:CGSizeMake(200, 40) hAlignment:kCCTextAlignmentLeft fontName:@"Marker Felt" fontSize:mediumFontSize];
        [self addChild:self.coinThisLevelLabel z:0];
        self.coinThisLevelLabel.position = coinThisLevelLabelPosition;
        NSNumber *myCoinDoubleNumber = [NSNumber numberWithDouble:[gameData returnCoinsThisLevel]];
        [self.coinThisLevelLabel setString:[NSString stringWithFormat:@"This Level $%@",[myCoinDoubleNumber stringValue]]];
        
        NSString *levelScoreName = [NSString stringWithFormat:@"Score For Level #%i",currentLevel];
        NSString *timeScoreName = [NSString stringWithFormat:@"Time For Level #%i",currentLevel];
        NSString *coinsScoreName = [NSString stringWithFormat:@"Coins For Level #%i",currentLevel];
        
        NSString *levelScoreString = [NSString stringWithFormat:@"%d",gameData.pointsScoredCurrentLevel];
        NSString *coinsThisLevel = [NSString stringWithFormat:@"%@",[myCoinDoubleNumber stringValue]];
        
        NSDictionary *scoreParams =
        [NSDictionary dictionaryWithObjectsAndKeys:
         levelScoreString, levelScoreName,
         [gameData returnCurrentTimeForLevel], timeScoreName,
         coinsThisLevel, coinsScoreName,
         nil];
        
        [Flurry logEvent:@"Level_Stats" withParameters:scoreParams];
        
        
        //80 and 5
        CCSprite *coin = [CCSprite spriteWithSpriteFrameName:HUD_COIN];
        coin.position = coinPosition;
        coin.anchorPoint = ccp(0,0);
        [self addChild:coin];
        
        
        self.scoreLabel = [CCLabelTTF labelWithString:@"" dimensions:CGSizeMake(200, 80) hAlignment:kCCTextAlignmentLeft fontName:@"Marker Felt" fontSize:mediumFontSize];
		[self addChild:self.scoreLabel z:0];
		self.scoreLabel.position = scoreLabelPosition;
        
        
        
        
        lab = @"";
        scoreLabelString = @"";
        
        pointsScoreAnimation = 0;
        BOOL isHighScore = gameData.isHighScore;
        
        
        NSLog(@"pointsScoredCurrentLevel %d",gameData.pointsScoredCurrentLevel);
        
        pointTotalThisRound = gameData.pointsScoredCurrentLevel;
        
        if(pointTotalThisRound <= [gameData returnHighScoreForLevel:currentLevel]){
            //isHighScore = NO;
        }
        //pointTotalThisRound = [gameData returnHighScoreForLevel:currentLevel];
        
        [self unschedule:@selector(startTick:)];
        [self schedule: @selector(startTick:) interval:1.0];
        
        
        
        
        self.highScoreLabel = [CCLabelTTF labelWithString:@"" dimensions:CGSizeMake(200, 80) hAlignment:kCCTextAlignmentLeft fontName:@"Marker Felt" fontSize:mediumFontSize];
        [self addChild:self.highScoreLabel z:0];
        self.highScoreLabel.position = highScoreLabelPosition;
        
        if(!isHighScore){
            if([gameData returnHighScoreForLevel:currentLevel] == pointTotalThisRound){
                [self.highScoreLabel setString:[NSString stringWithFormat:@"High Score!"]];
            }else{
                [self.highScoreLabel setString:[NSString stringWithFormat:@"Best: %i",[gameData returnHighScoreForLevel:currentLevel]]];
            }
            
        }else{
            [self.highScoreLabel setString:[NSString stringWithFormat:@"New High Score!"]];
        }
        
        
        buttDance = [[ButtDance alloc] initWithImage];
        [self addChild:buttDance];
        buttDance.scale = 1;
        buttDance.position = buttonDancePosition;
        
        //CCSpriteFrameCache *frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
        //[self showOneAlert];
        
        oneImage = [CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_ALERT_ONE]];
        
        if(IS_IPAD){
            oneImage.position = ccp(120,610);
        }else{
            oneImage.position = ccp(60,277);
        }
        
        [self addChild:oneImage];
        oneImage.opacity = 0;
        [self showOneAlert];
        
        int starRating = gameData.lastStarRating;
        was3Stars = NO;
        NSString *imageForStarSprite = IMAGE_STAR_1;
        if(starRating == 3){
            was3Stars = YES;
            imageForStarSprite = IMAGE_STAR_3;
        }else if(starRating == 2){
            imageForStarSprite = IMAGE_STAR_2;
        }
        
        CCSprite *starRatingSprite = [CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:imageForStarSprite]];
        [self addChild:starRatingSprite];
        starRatingSprite.position = starRatingPosition;
        starRatingSprite.scale = 0;

        id flipLeft = [CCSequence actions:[CCScaleTo actionWithDuration:1 scaleX:0 scaleY:0], nil];
        id scaleIn = [CCSequence actions:[CCScaleTo actionWithDuration:0.6 scaleX:1 scaleY:1], nil];
        id easeActionScaleIn = [CCEaseElasticInOut actionWithAction:scaleIn];

        
        [starRatingSprite runAction:[CCSequence actions:flipLeft,easeActionScaleIn,[CCCallFunc actionWithTarget:self selector:@selector(boom)],nil]];
        //60 277
	}
    
   	return self;
}

-(void)boom{
    
    if(was3Stars){
        starEmitter = [[CCParticleSystemQuad alloc]initWithFile:@"starEmitter.plist"];
        starEmitter.position = starRatingPosition;
        //starEmitter = p;
        [self addChild:starEmitter z:2];
        NSLog(@"BOOM");
    }
    
}

-(void)showOneAlert{
    
    GameData *gameData = [GameData sharedData];
    
    if([gameData returnTotalCoinsAvailable] >= 500){
        if(!gameData.gun1Purchased){
            oneImage.opacity = 255;
            return;
        }
        
        if([gameData returnTotalCoinsAvailable] >= 7500){
            if(!gameData.gun2Purchased){
                oneImage.opacity = 255;
                return;
            }
            
            if([gameData returnTotalCoinsAvailable] >= 15500){
                if(!gameData.gun3Purchased){
                    oneImage.opacity = 255;
                    return;
                }
            }
        }
        
    }
}

-(void)updateCoins{
    NSNumber *myDoubleNumber = [NSNumber numberWithDouble:[[GameData sharedData] returnTotalCoinsAvailable]];
    [self.coinLabel setString:[NSString stringWithFormat:@"Total  $%@",[myDoubleNumber stringValue]]];
}

-(void)startTick: (ccTime) dt
{
    [self unschedule:@selector(startTick:)];
    [self schedule: @selector(duringTick:) interval:0.01];
}

-(void)duringTick: (ccTime) dt

{
    
    if(pointsScoreAnimation < pointTotalThisRound)
    {
        int counterIncrement = pointTotalThisRound * .03;
        pointsScoreAnimation += counterIncrement;
        NSString *myString = [NSString stringWithFormat:@"Score: %d",pointsScoreAnimation];
        scoreLabelString = [lab stringByAppendingString:myString];
        [self.scoreLabel setString:scoreLabelString];
    }
    else
    {
        if(pointsScoreAnimation != pointTotalThisRound)
        {
            
            NSString *myString = [NSString stringWithFormat:@"Score: %d",pointTotalThisRound];
            scoreLabelString = [lab stringByAppendingString:myString];
            [self.scoreLabel setString:scoreLabelString];
            pointsScoreAnimation = pointTotalThisRound;
            
        }
        
    }
    if(scoreLabelString)
    {
        [self.scoreLabel setString:scoreLabelString];
    }
    
    
}

- (void)destroyFacebook{
    NSLog(@"About to release hsv in MenuBetweenLevels");
    [[fbView view] removeFromSuperview];
    //[[[CCDirector sharedDirector] view] removeFromSuperview:[hsv view]];
    [fbView release];
    fbView = nil;
    helper = nil;
}
- (void)dialogClosed{
    [self destroyFacebook];
}

- (void)successFacebook{
    NSLog(@"SUCCESS FACEBOOK");
    
    [Flurry logEvent:@"Facebook Update Sent"];
    [self destroyFacebook];
    GameData *gameData = [GameData sharedData];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSNumber *myDoubleNumber = [NSNumber numberWithDouble:[gameData returnTotalCoinsAvailable] + 5000];
    
    if(!gameData.hasFacebooked){
        
        
        
        CCSpriteFrameCache *frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
        [fbButton setNormalImage:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_BL_FB_NORMAL]]];
        [fbButton setSelectedImage:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_BL_FB_NORMAL]]];
        
        gameData.hasFacebooked = YES;
        [defaults setBool:YES forKey:SHARE_KEY];
        [defaults synchronize];
        [self.coinLabel setString:[NSString stringWithFormat:@"Total  $%@",[myDoubleNumber stringValue]]];
        [gameData addToTotalNumberOfCoins:5000];
        
        [self showOneAlert];
    }
}
- (void)facebookClicked:(id)sender{
    [Flurry logEvent:@"Facebook Button Clicked"];
    
    GameData *gameData = [GameData sharedData];
    if([gameData isRunningiOS6]){
        fbView = [[UIViewController alloc] init];
        [[[CCDirector sharedDirector] view] addSubview:[fbView view]];
        
        int theHighScore = [gameData returnHighScoreForLevel:currentLevel];
        NSString *fbString = [NSString stringWithFormat:@"Check this out! I just scored %i playing Amazing Mr Pants Lite : Can you beat it?", theHighScore];
        
        helper = [[SocialHelper alloc]init];
        helper.delegate = self;
        UIImage * image = [UIImage imageNamed:@"fb-icon.png"];
        NSURL *  url = [NSURL URLWithString:@"http://bit.ly/14cB77P"];
        [helper postMessage:fbString image:image  andURL:url forService:SLServiceTypeFacebook andTarget:fbView];
    }
}

- (BOOL)connectedToNetwork {
    // Create zero addy
    struct sockaddr_in zeroAddress;
    bzero(&zeroAddress, sizeof(zeroAddress));
    zeroAddress.sin_len = sizeof(zeroAddress);
    zeroAddress.sin_family = AF_INET;
	
    // Recover reachability flags
    SCNetworkReachabilityRef defaultRouteReachability = SCNetworkReachabilityCreateWithAddress(NULL, (struct sockaddr *)&zeroAddress);
    SCNetworkReachabilityFlags flags;
	
    BOOL didRetrieveFlags = SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags);
    CFRelease(defaultRouteReachability);
	
    if (!didRetrieveFlags)
    {
        NSLog(@"Error. Could not recover network reachability flags");
        return NO;
    }
	
    BOOL isReachable = flags & kSCNetworkFlagsReachable;
    BOOL needsConnection = flags & kSCNetworkFlagsConnectionRequired;
	BOOL nonWiFi = flags & kSCNetworkReachabilityFlagsTransientConnection;
	
	NSURL *testURL = [NSURL URLWithString:@"http://www.apple.com/"];
	NSURLRequest *testRequest = [NSURLRequest requestWithURL:testURL  cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:20.0];
	NSURLConnection *testConnection = [[NSURLConnection alloc] initWithRequest:testRequest delegate:self];
	
    return ((isReachable && !needsConnection) || nonWiFi) ? (testConnection ? YES : NO) : NO;
}

- (void)tweetAboutGame:(id)sender{
    
    [Flurry logEvent:@"Twitter Button Clicked"];
    GameData *gameData = [GameData sharedData];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    /*
     if(gameData.hasTweeted){
     UIAlertView *alertView = [[UIAlertView alloc]
     initWithTitle:@"Thanks for tweeting"
     message:@"You've already earned your 1000 coins! Awesome!"
     delegate:self
     cancelButtonTitle:@"OK"
     otherButtonTitles:nil];
     [alertView show];
     
     return;
     }
     */
    UIViewController *hsv = [[UIViewController alloc] init];
	[[[CCDirector sharedDirector] view] addSubview:[hsv view]];
    
    NSNumber *myDoubleNumber = [NSNumber numberWithDouble:[gameData returnTotalCoinsAvailable] + 5000];
    
    
    if ([TWTweetComposeViewController canSendTweet])
    {
        tweetSheet = [[TWTweetComposeViewController alloc] init];
        int theHighScore = [gameData returnHighScoreForLevel:currentLevel];
        NSString *tweetString = [NSString stringWithFormat:@"Check this out! I just scored %i playing @MrPantsGame : Can you beat it? http://bit.ly/14cB77P #AmazingMrPants", theHighScore];
        [tweetSheet setInitialText:tweetString];
        [hsv presentModalViewController:tweetSheet animated:YES];
        
        TWTweetComposeViewControllerCompletionHandler
        completionHandler =
        ^(TWTweetComposeViewControllerResult result) {
            switch (result)
            {
                case TWTweetComposeViewControllerResultCancelled:
                    NSLog(@"Twitter Result: canceled");
                    break;
                case TWTweetComposeViewControllerResultDone:
                    NSLog(@"Twitter Result: sent");
                    
                    [Flurry logEvent:@"Twitter Update Sent"];
                    
                    if(!gameData.hasTweeted){
                        
                        
                        if([self connectedToNetwork]){
                            CCSpriteFrameCache *frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
                            [tweetButton setNormalImage:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_BL_TWEET_NORMAL]]];
                            [tweetButton setSelectedImage:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_BL_TWEET_NORMAL]]];
                            
                            if(!gameData.isRunningiOS6){
                                [fbButton setNormalImage:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_BL_TWEET_NORMAL]]];
                                [fbButton setSelectedImage:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_BL_TWEET_NORMAL]]];
                            }
                            
                            gameData.hasTweeted = YES;
                            [defaults setBool:YES forKey:TWEET_KEY];
                            [defaults synchronize];
                            [self.coinLabel setString:[NSString stringWithFormat:@"Total  $%@",[myDoubleNumber stringValue]]];
                            [gameData addToTotalNumberOfCoins:5000];
                            
                            [self showOneAlert];
                        }
                        
                    }
                    break;
                default:
                    NSLog(@"Twitter Result: default");
                    break;
            }
            [hsv dismissModalViewControllerAnimated:YES];
            
        };
        [tweetSheet setCompletionHandler:completionHandler];
        
        NSLog(@"About to release tweetSheet in MenuBetweenLevels");
        [tweetSheet release];
        tweetSheet = nil;
        
    }
    else
    {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:@"Sorry"
                                  message:@"You can't send a tweet right now, make sure your device has an internet connection and you have at least one Twitter account setup"
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
        NSLog(@"About to release alertView in MenuBetweenLevels");
        [alertView release];
        alertView = nil;
        
    }
    NSLog(@"About to release hsv in MenuBetweenLevels");
    [[hsv view] removeFromSuperview];
    //[[[CCDirector sharedDirector] view] removeFromSuperview:[hsv view]];
    [hsv release];
    hsv = nil;
}


- (void)weaponsButtonTapped:(id)sender{
    
    [self addChild:[MenuBuyGuns scene]];
    //CCTransitionSplitRows *transition=[CCTransitionFade transitionWithDuration:.2 scene:[MenuBuyGuns scene]];
    //[[CCDirector sharedDirector] replaceScene:transition];
    
}
- (void)menuButtonTapped:(id)sender
{
    CCTransitionSplitRows *transition=[CCTransitionSplitRows transitionWithDuration:.7 scene:[MenuSelectLevel scene]];
    //MenuSelectLevel
    [[CCDirector sharedDirector] replaceScene:transition];
    //[self actionNextLevel];
}
- (void)replayButtonTapped:(id)sender
{
    GameData *gameData = [GameData sharedData];
    [gameData loadThisLevel:currentLevel];
    
    CCTransitionSplitCols *transition=[CCTransitionSplitCols transitionWithDuration:1 scene:[BaseLevel scene]];
    [[CCDirector sharedDirector] replaceScene:transition];
}
- (void)nextButtonTapped:(id)sender
{
    //CCTransitionSlideInR *transition=[CCTransitionSlideInR transitionWithDuration:1 scene:[BaseLevel scene]];
    [[CCDirector sharedDirector] replaceScene:[BaseLevel scene]];
}

-(void)dealloc
{
    [buttDance release];
    buttDance = nil;
    self.scoreLabel = nil;
    self.bestTimeLabel = nil;
    self.timeLabel = nil;
    
    self.highScoreLabel = nil;
    self.bestTimeLabel = nil;
    self.coinBonusLabel = nil;
    self.coinLabel = nil;
    self.coinThisLevelLabel = nil;
    [super dealloc];
}

@end