#import "MyContactListener.h"
#import "CCSprite.h"
#import "Constants.h"
MyContactListener::MyContactListener() : _contacts() {
}

MyContactListener::~MyContactListener() {
}

void MyContactListener::BeginContact(b2Contact* contact) {
    
    MyContact myContact = {contact->GetFixtureA(),contact->GetFixtureB()};
    _contacts.push_back(myContact);
    
    CCSprite *spriteA = (CCSprite *) contact->GetFixtureA()->GetBody()->GetUserData();
    CCSprite *spriteB = (CCSprite *) contact->GetFixtureB()->GetBody()->GetUserData();
    
    b2Fixture *breakableFixture;
    if (TAG_BULLET == spriteA.tag && TAG_BREAKABLE_ITEM == spriteB.tag)
    {
        breakableFixture = contact->GetFixtureA();
    }
    else if (spriteA.tag == TAG_BREAKABLE_ITEM && spriteB.tag == TAG_BULLET)
    {
        breakableFixture = contact->GetFixtureB();
    }else{
        breakableFixture = nil;
    }
    
    float currentSpeed;
    if(breakableFixture)
    {
        
        
        currentSpeed = myContact.force;
        //NSLog(@"current speed %f",currentSpeed);
        if(currentSpeed > .8)
        {
            
        }
    }
    
    
}

void MyContactListener::EndContact(b2Contact* contact) {
    MyContact myContact = { contact->GetFixtureA(), contact->GetFixtureB() };
    std::vector<MyContact>::iterator pos;
    pos = std::find(_contacts.begin(), _contacts.end(), myContact);
    if (pos != _contacts.end()) {
        _contacts.erase(pos);
        
    }
}

void MyContactListener::PreSolve(b2Contact* contact, const b2Manifold* oldManifold) {
    
    const b2Manifold* manifold = contact->GetManifold();
    
	if (manifold->pointCount == 0)
	{
		return;
	}
    
	b2PointState state1[b2_maxManifoldPoints], state2[b2_maxManifoldPoints];
	b2GetPointStates(state1, state2, oldManifold, manifold);
    
	b2WorldManifold worldManifold;
	contact->GetWorldManifold(&worldManifold);
    
    MyContact myContact = {
        contact->GetFixtureA(),
        contact->GetFixtureB(),
        worldManifold.normal,
        worldManifold.points[0],
        state2[0]
    };
    
    std::vector<MyContact>::iterator pos;
    pos = std::find(_contacts.begin(), _contacts.end(), myContact);
    if (pos != _contacts.end()) {
        _contacts.erase(pos);
        
    }
    _contacts.push_back(myContact);
    
}

void MyContactListener::PostSolve(b2Contact* contact, const b2ContactImpulse* impulse) {
    
    MyContact myContact = {
        
        contact->GetFixtureA(),
        contact->GetFixtureB(),
        
    };
    b2Vec2 position;
    b2Vec2 normal;
    b2PointState state;
    
    float force = impulse->normalImpulses[0];
    float frictionForce = impulse->tangentImpulses[0];
    
    std::vector<MyContact>::iterator pos;
    pos = std::find(_contacts.begin(), _contacts.end(), myContact);
    if (pos != _contacts.end()) {
        
        normal = pos->normal;
        position = pos->position;
        state = pos->state;
        _contacts.erase(pos);
    }
    MyContact myContact2 = {
        
        contact->GetFixtureA(),
        contact->GetFixtureB(),
        normal,
        position,
        state,
        force,
        frictionForce
        
    };
    
    _contacts.push_back(myContact2);
    
}

