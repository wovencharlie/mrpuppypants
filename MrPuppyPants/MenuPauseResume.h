//
//  MenuBetweenLevels.h
//  MrPants
//
//  Created by Charles Schulze on 9/15/12.
//  Copyright 2012 CS54 INC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "ButtDance.h"
#import "GameData.h"
#import "BaseLevel.h"

@interface MenuPauseResume : CCLayer
{
    CCMenuItemImage *restartButton;
    CCMenuItemImage *resumeButton;
    CCMenuItemImage *exitButton;
    CCMenuItemImage *weaponsButton;
    

    GameData *gameData;
}
+(CCScene *) scene;
@property (nonatomic,retain) CCLabelTTF *scoreLabel;
@end
