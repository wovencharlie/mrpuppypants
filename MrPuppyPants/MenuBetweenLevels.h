//
//  MenuBetweenLevels.h
//  MrPants
//
//  Created by Charles Schulze on 9/15/12.
//  Copyright 2012 CS54 INC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "ButtDance.h"
#import <RevMobAds/RevMobAds.h>
#import <Twitter/Twitter.h>
#import "SocialHelper.h"

@interface MenuBetweenLevels : CCLayer <SocialHelperDelegate>
{
    CCMenuItemImage *menuButton;
    CCMenuItemImage *replayButton;
    
    CCMenuItemImage *weaponsButton;
    CCMenuItemImage *tweetButton;
    CCMenuItemImage *nextButton;
    CCMenuItemImage *fbButton;
    
    int pointsScoreAnimation;
    int pointTotalThisRound;
    
    NSString *lab;
    NSString *scoreLabelString;
    ButtDance *buttDance;
    int currentLevel;
    //BOOL didShowAd;
    TWTweetComposeViewController *tweetSheet;
    SocialHelper * helper;
    UIViewController *fbView;
    CCParticleSystemQuad *starEmitter;
    
    CGPoint starRatingPosition;
    
    NSString *fbImageName;
    NSString *tweetImageName;
    
    CCSprite *oneImage;
    
    BOOL was3Stars;
}
+(CCScene *) scene;
- (void) resumeBackgroundMusic;
- (void) updateCoins;
@property (nonatomic,retain) CCLabelTTF *scoreLabel;
@property (nonatomic,retain) CCLabelTTF *highScoreLabel;
@property (nonatomic,retain) CCLabelTTF *timeLabel;
@property (nonatomic,retain) CCLabelTTF *bestTimeLabel;
@property (nonatomic,retain) CCLabelTTF *coinBonusLabel;
@property (nonatomic,retain) CCLabelTTF *coinLabel;
@property (nonatomic,retain) CCLabelTTF *coinThisLevelLabel;
@end
