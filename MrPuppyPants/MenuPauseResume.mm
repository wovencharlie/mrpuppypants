//
//  MenuBetweenLevels.m
//  MrPants
//
//  Created by Charles Schulze on 9/15/12.
//  Copyright 2012 CS54 INC. All rights reserved.
//

#import "MenuPauseResume.h"
#import "GameData.h"
#import "MenuSelectLevel.h"
#import "Constants.h"
#import "ButtDance.h"
#import "BaseLevel.h"
#import "MenuBuyGuns.h"
#import "IntroLayer.h"
#import "MenuSelectLevel.h"

@implementation MenuPauseResume

+(CCScene *) scene
{
	CCScene *scene = [CCScene node];
    MenuPauseResume *layer = [MenuPauseResume node];
    [scene addChild: layer];
    return scene;
}

-(id) init
{
    if((self = [super init]))
    {
        [Flurry logEvent:@"Paused Game Menu Displayed"]; 
        //175
        CGSize size = [[CCDirector sharedDirector] winSize];
        
        gameData = [GameData sharedData];
        //int currentLevel = [gameData returnLevel];
        CGPoint menuButtonPosition = ccp(105, 280);
        CGPoint mainMenusPosition = ccp(size.width/2,(size.height)/2);
        
        if(IS_IPAD){
            menuButtonPosition = ccp(210, 680);
        }
        
        CCSprite *imageSprite = [CCSprite spriteWithFile:IMAGE_BG_PAUSE_SCREEN];
        imageSprite.position = ccp(size.width / 2,size.height/2);
        //[self addChild:imageSprite];

        CCSpriteFrameCache *frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
       
        
        CCMenuItemImage *bgButton = [CCMenuItemImage itemWithNormalImage:IMAGE_BG_PAUSE_SCREEN selectedImage:IMAGE_BG_PAUSE_SCREEN];
        //musicToggleButton = [CCMenuItemToggle itemWithTarget:self selector:@selector(musicOnOffTapped:) items:musicOnButton,musicOffButton, nil];
        
        restartButton = [CCMenuItemImage
                                 itemWithNormalSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_RESTART_BUTTON]]
                                 selectedSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_RESTART_BUTTON]]
                                 target:self selector:@selector(restartTapped:)];
        
        resumeButton = [CCMenuItemImage
                         itemWithNormalSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_RESUME_BUTTON]]
                         selectedSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_RESUME_BUTTON]]
                         target:self selector:@selector(resumeTapped:)];
        
        weaponsButton = [CCMenuItemImage itemWithNormalSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_BL_WEAPONS_BUTTON]]
                                               selectedSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_BL_WEAPONS_BUTTON]]
                                                       target:self selector:@selector(weaponsButtonTapped:)];
        
        //
        exitButton = [CCMenuItemImage
                      itemWithNormalSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_EXIT_BUTTON]]
                      selectedSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_EXIT_BUTTON]]
                      target:self selector:@selector(exitTapped:)];

        
        
        CCMenu *bgMenu = [CCMenu menuWithItems:bgButton, nil];
        bgMenu.position = ccp(size.width/2,(size.height)/2);
        [self addChild:bgMenu];

        
        CCMenu *menu = [CCMenu menuWithItems:resumeButton,restartButton,weaponsButton,exitButton, nil];
        menu.position = mainMenusPosition;
        [menu alignItemsVertically];
        [self addChild:menu];
        menu.anchorPoint = ccp(0,0);
        
    }
    
   	return self;
}

- (void)exitTapped:(id)sender{
    [Flurry logEvent:@"Paused Game Menu : exit tapped"];
    [self removeFromParentAndCleanup:YES];
    BaseLevel *baseLevelReference = (BaseLevel *)self.parent.parent;
    [baseLevelReference removeFromParentAndCleanup:YES];
    baseLevelReference = nil;
    [[CCDirector sharedDirector] replaceScene:[MenuSelectLevel scene]];
}
- (void)restartTapped:(id)sender{
    [Flurry logEvent:@"Paused Game Menu : restart tapped"];
    [self removeFromParentAndCleanup:YES];
    [[CCDirector sharedDirector] replaceScene:[BaseLevel scene]];
}

- (void)resumeTapped:(id)sender
{
    [Flurry logEvent:@"Paused Game Menu : resume tapped"]; 
    BaseLevel *baseLevelReference = (BaseLevel *)self.parent.parent;
    [baseLevelReference resumeLevel];
    baseLevelReference = nil;
    [self removeFromParentAndCleanup:YES];
}

- (void)backButtonTapped:(id)sender
{
    [Flurry logEvent:@"Paused Game Menu : back tapped"]; 
    [[CCDirector sharedDirector] replaceScene:[IntroLayer scene]];
}

- (void)weaponsButtonTapped:(id)sender{
    
    [self addChild:[MenuBuyGuns scene]];
    
}

-(void)dealloc
{
    [super dealloc];
}

@end
