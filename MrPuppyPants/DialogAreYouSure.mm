//
//  MenuBetweenLevels.m
//  MrPants
//
//  Created by Charles Schulze on 9/15/12.
//  Copyright 2012 CS54 INC. All rights reserved.
//

#import "DialogAreYouSure.h"
#import "GameData.h"
#import "MenuSelectLevel.h"
#import "Constants.h"
#import "ButtDance.h"
#import "BaseLevel.h"
#import "MenuBuyGuns.h"
#import "IntroLayer.h"

@implementation DialogAreYouSure

+(CCScene *) scene
{
	CCScene *scene = [CCScene node];
    DialogAreYouSure *layer = [DialogAreYouSure node];
    [scene addChild: layer];
    return scene;
}

-(id) init
{
    if((self = [super init]))
    {
        [Flurry logEvent:@"Are you sure : Buy Gun : Menu Displayed"]; 

        CGSize size = [[CCDirector sharedDirector] winSize];
        
        gameData = [GameData sharedData];
        //int currentLevel = [gameData returnLevel];
        CGPoint menuButtonPosition = ccp(105, 280);
        CGPoint mainMenusPosition = ccp(size.width/2,(size.height)/2);
        
        if(IS_IPAD){
            menuButtonPosition = ccp(210, 680);
        }
        
        CCSprite *imageSprite = [CCSprite spriteWithFile:IMAGE_BG_PAUSE_SCREEN];
        imageSprite.position = ccp(size.width / 2,size.height/2);
        //[self addChild:imageSprite];

        CCSpriteFrameCache *frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
       
        
        CCMenuItemImage *bgButton = [CCMenuItemImage itemWithNormalImage:IMAGE_BG_PAUSE_SCREEN selectedImage:IMAGE_BG_PAUSE_SCREEN];
        
        
        CCSprite *dialogSprite = [CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_ARE_YOU_SURE_DIALOG]];//[CCSprite spriteWithFile:IMAGE_ARE_YOU_SURE_DIALOG];
        dialogSprite.position = ccp(size.width / 2,size.height/2);
        
        //musicToggleButton = [CCMenuItemToggle itemWithTarget:self selector:@selector(musicOnOffTapped:) items:musicOnButton,musicOffButton, nil];
        
        closeButton = [CCMenuItemImage
                                 itemWithNormalSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_BUY_CLOSE_BUTTON]]
                                 selectedSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_BUY_CLOSE_BUTTON]]
                                 target:self selector:@selector(closedTapped:)];
        
        buyButton = [CCMenuItemImage
                         itemWithNormalSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_BUY_BUY_BUTTON]]
                         selectedSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_BUY_BUY_BUTTON]]
                         target:self selector:@selector(buyTapped:)];

        
        
        CCMenu *bgMenu = [CCMenu menuWithItems:bgButton, nil];
        bgMenu.position = ccp(size.width/2,(size.height)/2);
        [self addChild:bgMenu];

        [self addChild:dialogSprite];
        
        CCMenu *menu = [CCMenu menuWithItems:closeButton,buyButton, nil];
        menu.position = mainMenusPosition;
        [menu alignItemsHorizontally];
        [self addChild:menu];
        menu.position = ccp(size.width / 2,(size.height/2) - 35);
        
        
        //menu.anchorPoint = ccp(0,0);
        
    }
    
   	return self;
}


- (void)closedTapped:(id)sender{
    [Flurry logEvent:@"Are you sure : Buy Gun : close tapped"]; 
    MenuBuyGuns *baseLevelReference = (MenuBuyGuns *)self.parent.parent;
    [baseLevelReference cancelPurchase];
    baseLevelReference = nil;
    [self removeFromParentAndCleanup:YES];
}

- (void)buyTapped:(id)sender
{
    [Flurry logEvent:@"Are you sure : Buy Gun : buy tapped"]; 
    MenuBuyGuns *baseLevelReference = (MenuBuyGuns *)self.parent.parent;
    [baseLevelReference completePurchase];
    baseLevelReference = nil;
    [self removeFromParentAndCleanup:YES];
}

-(void)dealloc
{
    [super dealloc];
}

@end
