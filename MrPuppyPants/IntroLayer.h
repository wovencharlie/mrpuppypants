//
//  IntroLayer.h
//  Squigglebottoms
//
//  Created by Charles Schulze on 9/29/12.
//  Copyright Charles Schulze 2012. All rights reserved.
//


// When you import this file, you import all the cocos2d classes
#import "cocos2d.h"
#import "TapjoyConnect.h"
#import <RevMobAds/RevMobAds.h>
// HelloWorldLayer

@interface IntroLayer : CCLayer
{
    CCSprite *cs54BG;
}

// returns a CCScene that contains the HelloWorldLayer as the only child
+(CCScene *) scene;

@end
