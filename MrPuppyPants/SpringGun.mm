//
//  SpringGun.m
//  MustachioRope
//
//  Created by Charles Schulze on 6/17/12.
//  Copyright 2012 CS54 INC. All rights reserved.
//

#import "SpringGun.h"
#import "Box2D.h"
#import "Constants.h"
#import "CCAnimationHelper.h"
@implementation SpringGun

- (id) initInParent:(CGPoint)pos
{
	if((self = [super init])) 
    {
        [self createExplosion];
        pawsAndGun = [CCSprite spriteWithSpriteFrameName:IMAGE_MR_PANTS_PAWS_GUN];
        spring = [CCSprite spriteWithSpriteFrameName:IMAGE_SPRING];
        spring.scale = .08;
        spring.scaleX = 0;
        spring.anchorPoint = ccp(1,0);
        [self addChild:pawsAndGun];
        [self addChild:spring];
        
        CGPoint pawsPoint = ccp(-5, -4);
        [spring setPosition:pawsPoint];
        
        //[pawsAndGun setAnchorPoint:ccp(0.2, .42)];
	}
    
   	return self;
}



-(void)createExplosion{
    explosion = [CCSprite spriteWithSpriteFrameName:GUN_EXPLOSION_IDLE_FRAME];
    
    //CCAnimation* anim = [CCAnimation animationWithFrame:GUN_EXPLOSION_BASE_FRAME frameCount:12 delay:0.04f startFrame:0];
    
    //CCAnimate* animate = [CCAnimate actionWithAnimation:anim];
    //repeat = [CCRepeat actionWithAction:animate times:1];
    //CCRepeatForever* repeat = [CCRepeatForever actionWithAction:animate];
    //[explosion runAction:repeat];
    explosion.position = dccp(32,0);
    [self addChild:explosion z:-100];
}

-(void)showExplosion{
    CCAnimation* anim = [CCAnimation animationWithFrame:GUN_EXPLOSION_BASE_FRAME frameCount:12 delay:0.04f startFrame:0];
    CCAnimate* animate = [CCAnimate actionWithAnimation:anim];
    repeat = [CCRepeat actionWithAction:animate times:1];
    [explosion runAction:repeat];
}

-(void)setSpringScale:(float)scale
{
    //float springScale = .1;
    id scaleUpAction;
    
    spring.scale = 0;//scale / 3;
    if(scale < .001)
    {
        //scaleUpAction =  [CCEaseIn actionWithAction:[CCScaleTo actionWithDuration:.1 scaleX:scale scaleY:.08] rate:.1];
    }
    else
    {
        //scaleUpAction =  [CCEaseIn actionWithAction:[CCScaleTo actionWithDuration:.5 scaleX:scale scaleY:.08] rate:2.0];
        
    }
    
    //id scaleDownAction = [CCEaseInOut actionWithAction:[CCScaleTo actionWithDuration:0.5 scaleX:0.8 scaleY:0.8] rate:2.0];
    //CCSequence *scaleSeq = [CCSequence actions:scaleUpAction, scaleDownAction, nil];
    //[spring runAction:scaleUpAction];

    
    //CCScaleTo *tween = [CCScaleTo actionWithDuration:2 scale:scale];
    // CCActionTween *tween = [CCActionTween actionWithDuration:2 key:@"scaleX" from:0.0f to:springScale];
    //[spring runAction:tween];
    
    //spring.scaleX = scale;
}



- (void)ball1Selected{
    
    CCSpriteFrameCache* cache = [CCSpriteFrameCache sharedSpriteFrameCache];
    [pawsAndGun setDisplayFrame:[cache spriteFrameByName:IMAGE_MR_PANTS_PAWS_GUN]];
}

- (void)ball2Selected{
    
    CCSpriteFrameCache* cache = [CCSpriteFrameCache sharedSpriteFrameCache];
    [pawsAndGun setDisplayFrame:[cache spriteFrameByName:IMAGE_MR_PANTS_PAWS_GUN_02]];
}

- (void)ball3Selected{
    CCSpriteFrameCache* cache = [CCSpriteFrameCache sharedSpriteFrameCache];
    [pawsAndGun setDisplayFrame:[cache spriteFrameByName:IMAGE_MR_PANTS_PAWS_GUN_03]];
}

- (void)ball4Selected{
    CCSpriteFrameCache* cache = [CCSpriteFrameCache sharedSpriteFrameCache];
    [pawsAndGun setDisplayFrame:[cache spriteFrameByName:IMAGE_MR_PANTS_PAWS_GUN_04]];
}



-(void)dealloc
{
    pawsAndGun = nil;
    spring = nil;
    [super dealloc];
}


@end
