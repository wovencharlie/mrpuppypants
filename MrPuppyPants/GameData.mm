
#import "GameData.h"
#import "Constants.h"
#import "SimpleAudioEngine.h"

@implementation GameData

#define START_WITH_LEVEL 1


#define GUN_1_PURCHASED @"gun1Purchased_TEST"
#define GUN_2_PURCHASED @"gun2Purchased_TEST"
#define GUN_3_PURCHASED @"gun3Purchased_TEST"

BOOL RUN_ON_AT_LEAST_IOS4_3;
BOOL RUN_ON_AT_LEAST_IOS5;
BOOL RUN_ON_AT_LEAST_IOS5_0_1;
BOOL RUN_ON_AT_LEAST_IOS5_1;
BOOL RUN_ON_AT_LEAST_IOS6;

static GameData *sharedData = nil;

#define HIGH_SCORE_KEY @"highScoreForLevel_TEST"

#define BEST_TIME_FOR_LEVEL_KEY @"bestTimeForLevel_TEST"

@synthesize pointsScoredCurrentLevel;
+(GameData*) sharedData {
    
    if (sharedData == nil) {
        sharedData = [[GameData alloc] init] ;
        
    }
    return  sharedData;
    
}

-(int)getStarRatingForLevel:(int)theLevel{
    NSString *starKeyForLevel = STAR_KEY;
    starKeyForLevel = [starKeyForLevel stringByAppendingFormat:@"%i",theLevel];
    NSLog(@"starRating is %i for level %i",[defaults integerForKey:starKeyForLevel],theLevel);
    return [defaults integerForKey:starKeyForLevel];
}

-(void)showActivtyIndicator{
    activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    activityView.frame = CGRectMake(139.0f-18.0f, 80.0f, 37.0f, 37.0f);
	[[[CCDirector sharedDirector] view] addSubview:activityView];
    
    [activityView startAnimating];
    
}

-(void)hideActivtyIndicator{
    [activityView removeFromSuperview];
    activityView = nil;
}

-(void)applyStarRatingForScore:(int)theScore{
    int ballCountToUseInCalc = 9;
    int maxBallScore;
    
    NSMutableArray *scoreArray = [NSMutableArray arrayWithObjects:@"2",@"3",@"5",@"5",@"5",@"7",@"4",@"3",@"6",@"4",@"5",@"3",@"5",@"3",@"4",@"3",@"6",@"6",@"6",@"4",@"3",@"3",@"4",@"6",nil];
    GameData *gameData = [GameData sharedData];
    int catCount = [[scoreArray objectAtIndex:level-1] integerValue];
    int maxCatScore = catCount * 5000;
    
    if([gameData gun1Purchased]){
        ballCountToUseInCalc = 14;
    }
    if([gameData gun2Purchased]){
        ballCountToUseInCalc = 18;
    }
    if([gameData gun3Purchased]){
        ballCountToUseInCalc = 22;
    }
    
    maxBallScore = (ballCountToUseInCalc - catCount) * 2500;
    int maxScore = maxCatScore + maxBallScore;
    int scoreAdd = 0;
    
    //adding in for time multiplier
    maxScore += 5000;

    int threeStarScore = maxScore + scoreAdd;
    int twoStarScore = (threeStarScore - 3500);
    
    NSLog(@"STAR theScore %i of maxScore %i",theScore,maxScore);
    if(theScore >= maxScore){
        NSLog(@"STAR : 3");
        [self setStarRating:3 forLevel:level];
        return;
    }else if(theScore >= twoStarScore){
        NSLog(@"STAR : 2");
        [self setStarRating:2 forLevel:level];
        return;
    }
    NSLog(@"STAR : 1");
    [self setStarRating:1 forLevel:level];
}

-(void)setStarRating:(int)rating forLevel:(int)theLevel{
    
    NSString *starKeyForLevel = STAR_KEY;
    starKeyForLevel = [starKeyForLevel stringByAppendingFormat:@"%i",theLevel];

    int currentStarForLevel = [defaults integerForKey:starKeyForLevel];
    if(!currentStarForLevel){
        currentStarForLevel = 0;
    }
    
    if (rating > currentStarForLevel){
        [defaults setInteger:rating forKey:starKeyForLevel];
    }
    
    self.lastStarRating = rating;
    [defaults synchronize];
}


-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self=[super init])) {
        
        
        sharedData = self;
        
        defaults = [NSUserDefaults standardUserDefaults];
        
        if(![defaults boolForKey:@"didShowAd"]){
            //reset each launch
            [defaults setBool:YES forKey:@"didShowAd"];
        }
        
        self.hasTweeted = [defaults boolForKey:TWEET_KEY];
        self.hasFacebooked = [defaults boolForKey:SHARE_KEY];
        self.hasWon24thLevel = [defaults boolForKey:@"goodBoyWon"];
        //[defaults removeObjectForKey:LEVEL_COMPLETED_ALL_TIME];
        //[defaults objectForKey:@"highScoreLevel1"];
        soundFXMuted = [defaults boolForKey:@"soundFXMutedKey"];   //will default to NO if there's no previous default value
        voiceFXMuted = [defaults boolForKey:@"voiceFXMutedKey"];   //will default to NO if there's no previous default value
        ambientFXMuted = [defaults boolForKey:@"ambientFXMutedKey"];   //will default to NO if there's no previous default value
        
        pointTotalForAllLevels = 0;
        //self.pointsScoredCurrentLevel = 0;
        levelsCompleted = [defaults integerForKey:LEVEL_COMPLETED_ALL_TIME];
        //
        level = START_WITH_LEVEL; //use 0 to show a testbed of shapes
        
        maxLevels = 24; // change to whatever, could be more or less
        
        CCLOG(@" Levels completed over all time: %i", levelsCompleted  );
        
        eachLevelSectionIsHowManyLevels = 24;
        
        totalNumberOfCoins = [defaults doubleForKey:USER_COIN_COUNT];
        
        self.highScoreDictionary = [NSMutableDictionary dictionary];
        
        BOOL canSubmitHighScore = YES;
        double totalTime = 0;
        for(int i = 0;i<24;i++){
            NSString *highScoreForLevel = HIGH_SCORE_KEY;
            highScoreForLevel = [highScoreForLevel stringByAppendingFormat:@"%i",i+1];
            NSNumber *scoreNumber = [NSNumber numberWithInt:[defaults integerForKey:highScoreForLevel]];
            [self.highScoreDictionary setObject:scoreNumber forKey:highScoreForLevel];
            
            
            NSString *bestTimeForLevel = BEST_TIME_FOR_LEVEL_KEY;
            bestTimeForLevel = [bestTimeForLevel stringByAppendingFormat:@"%i",i+1];
            NSNumber *numberHighScoreForLevel = [NSNumber numberWithDouble:[defaults doubleForKey:bestTimeForLevel]];
            [self.highScoreDictionary setObject:numberHighScoreForLevel forKey:bestTimeForLevel];
            
            if([defaults doubleForKey:bestTimeForLevel] < 1){
                canSubmitHighScore = NO;
            }else if([defaults doubleForKey:bestTimeForLevel] > 0){
                totalTime += [defaults doubleForKey:bestTimeForLevel];
            }
        }
        
        //totalTime *= 7;
        
        if(canSubmitHighScore){
            NSLog(@"CAN SUBMIT HIGH SCORE");
        }else{
            NSLog(@"You cannot submit high score yet");
        }
        
        if(![defaults integerForKey:@"timesPaused"])
        {
            [defaults setInteger:1 forKey:@"timesPaused"];
        }
        timesPaused = [defaults integerForKey:@"timesPaused"];
        [defaults synchronize];
        /*
         double gameKitTime = theTime * 60000;
         
         DDGameKitHelper *gameKitHelper = [DDGameKitHelper sharedGameKitHelper];
         [gameKitHelper submitScore:gameKitTime category:@"time.leaderboard"];
         */
        
        [self performSelector:@selector(hasBeenEnoughTimeToShowNewAd) withObject:self afterDelay:120];

    }
    
    return self;
}

-(void)hasBeenEnoughTimeToShowNewAd{
    hasBeenEnoughTimeSinceLastAd = YES;
    NSLog(@"Has been 2 minutes");
}


-(BOOL)shouldShowAd{

    if(!hasBeenEnoughTimeSinceLastAd){
        return NO;
    }
    
    hasBeenEnoughTimeSinceLastAd = NO;
    [self performSelector:@selector(hasBeenEnoughTimeToShowNewAd) withObject:self afterDelay:120];
    return YES;

    NSString *didShowAdBool = @"didShowAd";
    if([defaults boolForKey:didShowAdBool]){
        [defaults setBool:NO forKey:didShowAdBool];
        [defaults synchronize];
        return NO;
    }else{
        [defaults setBool:YES forKey:didShowAdBool];
        [defaults synchronize];
    }
    return YES;
}

- (BOOL)isGameCenterAvailable{
    DDGameKitHelper *gameHelper = [DDGameKitHelper sharedGameKitHelper];
    return gameHelper.isGameCenterAvailable;
}

-(void)submitBestTimeToGameCenter:(double)time{
    if(![self isGameCenterAvailable]){
        return;
    }
    DDGameKitHelper *gameKitHelper = [DDGameKitHelper sharedGameKitHelper];
    [gameKitHelper submitScore:time category:GC_TIME_LEADER];
}

-(void)submitBestScoreToGameCenter:(double)score{
    if(![self isGameCenterAvailable]){
        return;
    }
    DDGameKitHelper *gameKitHelper = [DDGameKitHelper sharedGameKitHelper];
    [gameKitHelper submitScore:score category:GC_LEADERBOARD];
}

-(void)submitAchievementName:(NSString *)name percent:(int)percent{
    //GC_USE_WORMHOLE
    if(![self isGameCenterAvailable]){
        return;
    }
    DDGameKitHelper *gameKitHelper = [DDGameKitHelper sharedGameKitHelper];
    [gameKitHelper reportAchievement:name percentComplete:percent];
}

-(void)canSubmitBestTimeToGameCenter{
    
    //BOOL canSubmitHighScore = YES;
    BOOL canSubmitBestTime = YES;
    double totalScore = 0;
    double totalTime = 0;
    
    for(int i = 0;i<24;i++){
        NSString *highScoreForLevel = HIGH_SCORE_KEY;
        highScoreForLevel = [highScoreForLevel stringByAppendingFormat:@"%i",i+1];
        //totalScore += [defaults integerForKey:highScoreForLevel];
        
        if([defaults doubleForKey:highScoreForLevel] < 1){
            //canSubmitHighScore = NO;
        }else if([defaults doubleForKey:highScoreForLevel] > 0){
            totalScore += [defaults doubleForKey:highScoreForLevel];
        }
        
        //NSLog(@"i %i and High Score %f and score for level %i",i, totalScore,[defaults integerForKey:highScoreForLevel]);
        
        NSString *bestTimeForLevel = BEST_TIME_FOR_LEVEL_KEY;
        bestTimeForLevel = [bestTimeForLevel stringByAppendingFormat:@"%i",i+1];
        totalTime += [defaults doubleForKey:bestTimeForLevel];

        if([defaults doubleForKey:bestTimeForLevel] < 1){
            canSubmitBestTime = NO;
        }else if([defaults doubleForKey:bestTimeForLevel] > 0){
            totalTime += [defaults doubleForKey:bestTimeForLevel];
        }
    }
    
    totalTime *= 7;
    
    if(canSubmitBestTime){
        [self submitBestTimeToGameCenter:totalTime];
        NSLog(@"CAN SUBMIT BEST TIME");
    }else{
        NSLog(@"You cannot submit best time yet");
    }
    
    if(totalScore > 0){
        [self submitBestScoreToGameCenter:totalScore];
        NSLog(@"CAN SUBMIT BEST SCORE");
    }else{
        NSLog(@"You cannot submit best score yet");
    }
}




-(int) currentSection
{
    return 1;
}
-( unsigned char ) returnLevel {
    
    return level;
}

#pragma mark LEVEL UP

-(void)setLevelWon:(int)levelWon
{
    if ( levelWon > levelsCompleted ) {
        
        //levelsCompleted ++;
        
        [defaults setInteger:levelsCompleted forKey:LEVEL_COMPLETED_ALL_TIME];
        [defaults synchronize];
        CCLOG(@"level %i completed", levelsCompleted);
    }
}

-( void ) levelUp {
    
    NSLog(@"current level %i",[self returnLevel]);
    NSLog(@"level complted -1 %i",(levelsCompleted + 1));
    
    
    if([self returnLevel] == (levelsCompleted + 1)){
        level ++;
        NSLog(@"UNLOCK LEVEL");
        if ( level > levelsCompleted ) {
            
            levelsCompleted ++;
            
            [defaults setInteger:levelsCompleted forKey:LEVEL_COMPLETED_ALL_TIME];
            [defaults synchronize];
            CCLOG(@"level %i completed", levelsCompleted);
        }
        
        if( level > maxLevels) {
            
            level = 1;
        }
    }else{
        level ++;
        NSLog(@"NO WAY WE UNLOCK THIS LEVEL");
    }
    
    lastLevelLeveledUp = level;
  
    
}

#pragma mark LEVEL JUMPING


-(BOOL) canYouGoToTheFirstLevelOfThisSection:(int) theSection {
    
    int thePreviousSection = theSection - 1;
    
     if (levelsCompleted >= ( thePreviousSection * eachLevelSectionIsHowManyLevels) ) {
         
         return  YES;
     }  
     else {
         
         CCLOG(@"you need to pass level %i before jumping to here",  ( thePreviousSection * eachLevelSectionIsHowManyLevels));
         return  NO;
         
     }
    
}

-(void) changeLevelToFirstInThisSection:(int)theSection {
    
    int thePreviousSection = theSection - 1;
    
    level = ( thePreviousSection * eachLevelSectionIsHowManyLevels) + 1;
     CCLOG(@"Level now equals %i, which is the first level in Section: %i", level, theSection);
}



-(void) attemptToGoToFirstLevelOfSection:(int)theSection {
    
    
    if ( [self canYouGoToTheFirstLevelOfThisSection:theSection] == YES ) {
        
        [self changeLevelToFirstInThisSection:theSection];
        
    }
}

#pragma mark RESET GAME / POINT TOTAL ALL LEVELS (not called ever) 

-(void) resetGame {   //this method never gets called in my version. Not really a need to since I'm not showing the pointTotalForAllLevels ever
    
    level = 1;
    pointTotalForAllLevels = 0;
    
    for(int i = 1; i < 20; i++ )
    {
        NSString *myLevelHighScore = [NSString stringWithFormat:@"highScoreLevel%d",i];
        [defaults removeObjectForKey:myLevelHighScore];
    }
    
}
-(BOOL)isExactlyiOS6{
    NSString *deviceVersion = [UIDevice currentDevice].systemVersion;
    float deviceVersionAsFloat = [deviceVersion floatValue];
    NSLog(@"[UIDevice currentDevice].systemVersion: %@ - %f", deviceVersion, deviceVersionAsFloat);
    RUN_ON_AT_LEAST_IOS4_3 = deviceVersionAsFloat >= 4.29;
    RUN_ON_AT_LEAST_IOS5 = deviceVersionAsFloat >= 5;
    RUN_ON_AT_LEAST_IOS5_0_1 = (deviceVersionAsFloat > 5.0 || [deviceVersion isEqualToString:@"5.0.1"]);
    RUN_ON_AT_LEAST_IOS5_1 = (deviceVersionAsFloat >= 5.09);
    RUN_ON_AT_LEAST_IOS6 = (deviceVersionAsFloat == 6.0);
    
    NSLog(@"RUN_ON_AT_LEAST_IOS4.3  : %d", RUN_ON_AT_LEAST_IOS4_3);
    NSLog(@"RUN_ON_AT_LEAST_IOS5.0  : %d", RUN_ON_AT_LEAST_IOS5);
    NSLog(@"RUN_ON_AT_LEAST_IOS5.0.1: %d", RUN_ON_AT_LEAST_IOS5_0_1);
    NSLog(@"RUN_ON_AT_LEAST_IOS5.1  : %d", RUN_ON_AT_LEAST_IOS5_1);
    NSLog(@"RUN_ON_AT_LEAST_IOS6    : %d", RUN_ON_AT_LEAST_IOS6);
    
    if (RUN_ON_AT_LEAST_IOS5) {
        if(RUN_ON_AT_LEAST_IOS6){
            return YES;
        }
    }
    
    return NO;
}
-(BOOL)isRunningiOS6{
    NSString *deviceVersion = [UIDevice currentDevice].systemVersion;
    float deviceVersionAsFloat = [deviceVersion floatValue];
    NSLog(@"[UIDevice currentDevice].systemVersion: %@ - %f", deviceVersion, deviceVersionAsFloat);
    RUN_ON_AT_LEAST_IOS4_3 = deviceVersionAsFloat >= 4.29;
    RUN_ON_AT_LEAST_IOS5 = deviceVersionAsFloat >= 5;
    RUN_ON_AT_LEAST_IOS5_0_1 = (deviceVersionAsFloat > 5.0 || [deviceVersion isEqualToString:@"5.0.1"]);
    RUN_ON_AT_LEAST_IOS5_1 = (deviceVersionAsFloat >= 5.09);
    RUN_ON_AT_LEAST_IOS6 = (deviceVersionAsFloat >= 6.0);
    
    NSLog(@"RUN_ON_AT_LEAST_IOS4.3  : %d", RUN_ON_AT_LEAST_IOS4_3);
    NSLog(@"RUN_ON_AT_LEAST_IOS5.0  : %d", RUN_ON_AT_LEAST_IOS5);
    NSLog(@"RUN_ON_AT_LEAST_IOS5.0.1: %d", RUN_ON_AT_LEAST_IOS5_0_1);
    NSLog(@"RUN_ON_AT_LEAST_IOS5.1  : %d", RUN_ON_AT_LEAST_IOS5_1);
    NSLog(@"RUN_ON_AT_LEAST_IOS6    : %d", RUN_ON_AT_LEAST_IOS6);
    
    if (RUN_ON_AT_LEAST_IOS5) {
        if(RUN_ON_AT_LEAST_IOS6){
            return YES;
        }
    }
    
    return NO;
}

-(double)returnTotalCoinsAvailable{
    return [defaults doubleForKey:USER_COIN_COUNT];
}

-(void)addToTotalNumberOfCoins:(double)coins{
    coinsThisLevel = 0;
    totalNumberOfCoins += coins;
    [defaults setDouble:totalNumberOfCoins forKey:USER_COIN_COUNT];
    [defaults synchronize];
    coinsThisLevel += coins;
    
    double myCoinTotal = [defaults doubleForKey:USER_COIN_COUNT];
    NSNumber *myDoubleNumber = [NSNumber numberWithDouble:myCoinTotal];
    NSLog(@"addToTotalNumberOfCoins TOTAL COINS NOW :%@",[myDoubleNumber stringValue]);
}

-(void)addBonusCoins{
    int coins = [self returnCoinMultiplier];
    totalNumberOfCoins += coins;
    [defaults setDouble:totalNumberOfCoins forKey:USER_COIN_COUNT];
    [defaults synchronize];
    coinsThisLevel += coins;
    
    
    
    double myCoinTotal = [defaults doubleForKey:USER_COIN_COUNT];
    NSNumber *myDoubleNumber = [NSNumber numberWithDouble:myCoinTotal];
    NSLog(@"addBonusCoins TOTAL COINS NOW :%@",[myDoubleNumber stringValue]);
}

-(int)returnCoinMultiplier{
    int multiplier = 0;
    double actualLeveltime = levelCurrentTime * 7;
    if(actualLeveltime < 31){
        multiplier = 100;
    }else if(actualLeveltime < 61 && actualLeveltime > 30){
        multiplier = 90;
    }else if(actualLeveltime < 121 && actualLeveltime > 60){
        multiplier = 80;
    }else if(actualLeveltime < 241 && actualLeveltime > 120){
        multiplier = 50;
    }else if(actualLeveltime < 241 && actualLeveltime > 120){
        multiplier = 30;
    }else if(actualLeveltime < 301 && actualLeveltime > 240){
        multiplier = 20;
    }else if(actualLeveltime < 361 && actualLeveltime > 300){
        multiplier = 10;
    }
    
    return multiplier;
}

-(int)returnScoreAddition{
    int multiplier = 100;
    double actualLeveltime = levelCurrentTime * 7;
    if(actualLeveltime < 31){
        multiplier = 10000;
    }else if(actualLeveltime < 61 && actualLeveltime > 30){
        multiplier = 9000;
    }else if(actualLeveltime < 121 && actualLeveltime > 60){
        multiplier = 7000;
    }else if(actualLeveltime < 241 && actualLeveltime > 120){
        multiplier = 5000;
    }else if(actualLeveltime < 241 && actualLeveltime > 120){
        multiplier = 3000;
    }else if(actualLeveltime < 301 && actualLeveltime > 240){
        multiplier = 1500;
    }else if(actualLeveltime < 361 && actualLeveltime > 300){
        multiplier = 500;
    }
    
    return multiplier;
}

-(void)addCoinMultiplier{
    
}

-(double)returnCoinsThisLevel{
    return coinsThisLevel;
}

-(void)spendCoins:(double)removeThisAmountOfCoins{
    totalNumberOfCoins -= removeThisAmountOfCoins;
    [defaults setDouble:totalNumberOfCoins forKey:USER_COIN_COUNT];
    [defaults synchronize];
}

-(BOOL)canSpendThisManyCoins:(double)coinAmount{
    if(totalNumberOfCoins >= coinAmount){
        return YES;
    }
    return NO;
}

-(void)loadThisLevel:(int)newLevel
{
    level = newLevel;
}


-(void) addToPointTotalForAllLevels:(int)pointThisLevel {  //this method gets called, but at no point am I ever showing the pointTotalForAllLevels
    
    
    pointTotalForAllLevels = pointTotalForAllLevels + pointThisLevel;
    
}

#pragma mark METHODS TheLevel Class calls to return settings for the current level being played



-(void)populateLevelObjects:(LevelObjects *)levelObject
{
    //Setting some defaults;
    levelObject.bgImage = IMAGE_CIRCLE_SKY_BACKGROUND;
    levelObject.sunImage = IMAGE_SUN;
    levelObject.midBgImage = IMAGE_MID_BACKGROUND;
    levelObject.levelImage = IMAGE_GROUND_LEVEL;
    levelObject.bgImagePoint = ccp(0, 0);
    levelObject.sunImagePoint = ccp(490, 160);
    levelObject.midImagePoint = ccp(0, 0);
    levelObject.altImagePoint = ccp(0, 0);
    levelObject.maxAltitude = 76 + 136;
    levelObject.minAltitude = 78;
    levelObject.ballsArray = [NSMutableArray arrayWithObjects:@"10",@"0",@"0",@"0", nil];
    levelObject.mrPantsStartPoint = dccp(60,30);
  
    if(level > 24){
        levelObject.levelImage = IMAGE_LEVEL;
    }
    
    NSString *gun1Amount = @"0";
    NSString *gun2Amount = @"0";
    NSString *gun3Amount = @"0";
  
    if([self gun1Purchased]){
        gun1Amount = @"5";
    }
    if([self gun2Purchased]){
        gun2Amount = @"5";
    }
    if([self gun3Purchased]){
        gun3Amount = @"10";
    }
    
    levelObject.ballsArray = [NSMutableArray arrayWithObjects:@"10",gun1Amount,gun2Amount,gun3Amount, nil];
}


#pragma mark HIGH SCORES 

-(void) setHighScoreForLevel:(int)theScore {
    
    self.isHighScore = NO;
    CCLOG(@" checking to see if %i is a new high score", theScore);
    
    NSString *highScoreForLevel = HIGH_SCORE_KEY;
    highScoreForLevel = [highScoreForLevel stringByAppendingFormat:@"%i",level];
    NSNumber *numberHighScoreForLevel = [self.highScoreDictionary objectForKey:highScoreForLevel];
    int intHighScoreForLevel = [numberHighScoreForLevel intValue];
    
    currentHighScore = theScore;
    
    if (theScore > intHighScoreForLevel){
        [self.highScoreDictionary setObject:[NSNumber numberWithInt:theScore] forKey:highScoreForLevel];
        [defaults setInteger:theScore forKey:highScoreForLevel];
        self.isHighScore = YES;
    }
    
    [self applyStarRatingForScore:theScore];
    [defaults synchronize];
}



-(int) returnHighScoreForLevel:(int)lvl {
    
    NSString *highScoreForLevel = HIGH_SCORE_KEY;
    highScoreForLevel = [highScoreForLevel stringByAppendingFormat:@"%i",lvl];
    NSNumber *numberHighScoreForLevel = [self.highScoreDictionary objectForKey:highScoreForLevel];
    int intHighScoreForLevel = [numberHighScoreForLevel intValue];
    return intHighScoreForLevel;
}

-(BOOL) setBestTimeForLevel:(NSTimeInterval)theTime {
    
    NSNumberFormatter *fmt = [[NSNumberFormatter alloc] init];
    [fmt setPositiveFormat:@"0.#"];
    NSString *timeString = [fmt stringFromNumber:[NSNumber numberWithFloat:theTime]];
    double timeFormattedFromString = [timeString doubleValue];
    
    [fmt release];
    fmt = nil;

    
    NSString *bestTimeForLevel = BEST_TIME_FOR_LEVEL_KEY;
    bestTimeForLevel = [bestTimeForLevel stringByAppendingFormat:@"%i",level];
    NSNumber *numberBestTimeForLevel = [self.highScoreDictionary objectForKey:bestTimeForLevel];
    NSNumber *theTimeAsNumber = [NSNumber numberWithDouble:timeFormattedFromString];
    
    double bestTimeDouble = [numberBestTimeForLevel doubleValue];
    double theTimeDouble = [theTimeAsNumber doubleValue];

    levelCurrentTime = theTimeDouble;
    self.isNewBestTime = NO;
    
    if (theTimeDouble < bestTimeDouble || bestTimeDouble == 0){
        [self.highScoreDictionary setObject:[NSNumber numberWithDouble:theTimeDouble] forKey:bestTimeForLevel];
        [defaults setDouble:theTimeDouble forKey:bestTimeForLevel];
        levelBestTime = theTimeDouble;
        [defaults synchronize];
        self.isNewBestTime = YES;
    }else
    {
        levelBestTime = bestTimeDouble;
    }
    
    [self canSubmitBestTimeToGameCenter];
    
    return self.isNewBestTime;
}


-(NSString *) returnCurrentTimeForLevel {
    return [self formattedTimeForLevel:levelCurrentTime];
}

-(NSString *) returnBestTimeForLevel:(int)lvl {
    
    NSString *bestTimeForLevel = BEST_TIME_FOR_LEVEL_KEY;
    bestTimeForLevel = [bestTimeForLevel stringByAppendingFormat:@"%i",lvl];
    NSNumber *numberHighScoreForLevel = [self.highScoreDictionary objectForKey:bestTimeForLevel];
    NSString *stringBestTimeForLevel = [self formattedBestTimeForLevel:[numberHighScoreForLevel doubleValue]];
    
    if([numberHighScoreForLevel doubleValue] < .1){
        return @"";
    }
  
    return stringBestTimeForLevel;
}
-(NSString *)formattedBestTimeForLevel:(double )currentTime{
    
    currentTime *= 7;
    
    int mins = (int) (currentTime / 60.0);
    currentTime -= mins * 60;
    int secs = (int) (currentTime);
    currentTime -= secs;
    //int fraction = currentTime * 10.0;
    
    return [NSString stringWithFormat:@"Best Time: %u:%02u",mins,secs];
}

-(NSString *)formattedTimeForLevel:(double )currentTime{
    
    currentTime *= 7;
    
    int mins = (int) (currentTime / 60.0);
    currentTime -= mins * 60;
    int secs = (int) (currentTime);
    currentTime -= secs;
    //int fraction = currentTime * 10.0;
    
    return [NSString stringWithFormat:@"Puppy Time: %u:%02u",mins,secs];
}



#pragma mark sounds

-(void) turnSoundFXOn {
    soundFXMuted = NO;
    [defaults setBool:soundFXMuted forKey:@"soundFXMutedKey"];
}

-(void) turnSoundFXOff {
    soundFXMuted = YES;
    [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
    [defaults setBool:soundFXMuted forKey:@"soundFXMutedKey"];
    [defaults synchronize]; //maybe unneccessary    
}

-(BOOL) areSoundFXMuted {
    
    return soundFXMuted;
}

/////////////////////////

-(void) turnVoiceFXOn {
    voiceFXMuted = NO;
    
    [defaults setBool:voiceFXMuted forKey:@"voiceXMutedKey"];
}

-(void) turnVoiceFXOff {
    voiceFXMuted = YES;
    [defaults setBool:voiceFXMuted forKey:@"voiceFXMutedKey"];
    [defaults synchronize]; //maybe unneccessary   
}

-(BOOL) areVoiceFXMuted {
    
    return voiceFXMuted;
}

/////////////////////////

-(void) turnAmbientFXOn {
    ambientFXMuted = NO;
    [defaults setBool:ambientFXMuted forKey:@"ambientFXMutedKey"]; 
}
-(void) turnAmbientFXOff {
    ambientFXMuted = YES;
    [defaults setBool:ambientFXMuted forKey:@"ambientFXMutedKey"];
    [defaults synchronize]; //maybe unneccessary  
}

-(BOOL) areAmbientFXMuted {
    
    return ambientFXMuted;
}

-(void)setGun1Purchased{
    
    [defaults setBool:YES forKey:GUN_1_PURCHASED];
    [defaults synchronize];
}

-(void)setGun2Purchased{
    
    [defaults setBool:YES forKey:GUN_2_PURCHASED];
    [defaults synchronize];
}

-(void)setGun3Purchased{
    
    [defaults setBool:YES forKey:GUN_3_PURCHASED];
    [defaults synchronize];
}

-(BOOL)gun1Purchased
{
    return [defaults boolForKey:GUN_1_PURCHASED];
}

-(BOOL)gun2Purchased
{
    return [defaults boolForKey:GUN_2_PURCHASED];
}

-(BOOL)gun3Purchased
{
    return [defaults boolForKey:GUN_3_PURCHASED];
}

-(void)startMainScreenLoops{
    if(ENABLE_AUDIO && ![[GameData sharedData]areSoundFXMuted]){
        if(![[SimpleAudioEngine sharedEngine] isBackgroundMusicPlaying] || ![currentBGFilePlaying isEqualToString:LOOP_SECTION_03]){
            // do nothing
            [[SimpleAudioEngine sharedEngine] playBackgroundMusic:LOOP_SECTION_03 loop:YES];
            [[SimpleAudioEngine sharedEngine] setBackgroundMusicVolume:0.6];
            
            currentBGFilePlaying = LOOP_SECTION_03;
        }
        
    }
}

-(void)startLevel_1_Loop{
    if(ENABLE_AUDIO && ![[GameData sharedData]areSoundFXMuted]){
        
        if([[SimpleAudioEngine sharedEngine] isBackgroundMusicPlaying] && ![currentBGFilePlaying isEqualToString:LOOP_SECTION_01]){
            [[SimpleAudioEngine sharedEngine]stopBackgroundMusic];
        }
        if(![[SimpleAudioEngine sharedEngine] isBackgroundMusicPlaying]){
            [[SimpleAudioEngine sharedEngine] playBackgroundMusic:LOOP_SECTION_01 loop:YES];
            [[SimpleAudioEngine sharedEngine] setBackgroundMusicVolume:0.6];
        }
        
        
        currentBGFilePlaying = LOOP_SECTION_01;
        
    }
}






@end
