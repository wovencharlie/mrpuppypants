//
//  CraneHook.m
//  MrPants
//
//  Created by Charles Schulze on 9/1/12.
//  Copyright 2012 CS54 INC. All rights reserved.
//

#import "CraneHook.h"


@implementation CraneHook

- (id) initInParent:(CCLayer *)parentLayer At:(CGPoint)pos inWorld:(b2World *)world{
	if((self = [super init]))
    {
        _world = world;
        parent = parentLayer;
        craneHook = [CCSprite spriteWithSpriteFrameName:IMAGE_CRANE_HOOK];
        craneHook.anchorPoint = ccp(.5,1);
        craneHook.position = pos;
        [self addChild:craneHook];
        
        ball = [CCSprite spriteWithSpriteFrameName:IMAGE_WREAKING_BALL];
        ball.position = ccp(craneHook.position.x, craneHook.position.y - 95);
        [self addChild:ball];
        
        
        id moveToRight = [CCMoveTo actionWithDuration:2 position:ccp(pos.x + 60,pos.y)];
        id moveToLeft = [CCMoveTo actionWithDuration:2 position:ccp(pos.x,pos.y)];
        
        CCSequence *leftRight = [CCSequence actionOne:moveToRight two:moveToLeft];
        CCRepeatForever *repeat = [CCRepeatForever actionWithAction:leftRight];
        [craneHook runAction:repeat];
        
        [self schedule: @selector(tick:)];
        
        ballReleased = NO;
        
	}
	return self;
}

-(void)releaseBall
{
    if(ballReleased)
    {
        NSLog(@"ball already released");
        return;
    }
    
    ballReleased = YES;
    
    ball.opacity = 0;
    [craneHook stopAllActions];
    
    
    
    b2BodyDef bodyDef1;
    bodyDef1.type = b2_dynamicBody;
    CCSprite *boxSprite1 = [CCSprite spriteWithSpriteFrameName:IMAGE_WREAKING_BALL];
    [self addChild:boxSprite1 z:0 tag:TAG_WREAKING_BALL];
    
    bodyDef1.userData = boxSprite1;
    b2Body *body1 = _world->CreateBody(&bodyDef1);
    
    b2CircleShape shape1;
    float radiusInMeters = 0.42f;
    shape1.m_radius = radiusInMeters;
    b2FixtureDef fixtureDef1;
    fixtureDef1.shape = &shape1;
    fixtureDef1.density = 40.0f;
    fixtureDef1.friction = 1.0f;
    fixtureDef1.restitution = 0.1f;
    body1->SetTransform(b2Vec2(ball.position.x / PTM_RATIO, ball.position.y / PTM_RATIO),0);
    body1->CreateFixture(&fixtureDef1);

}

-(void)tick:(ccTime)dt
{
    ball.position = ccp(craneHook.position.x, craneHook.position.y - 95);
    //NSLog(@"I'm ticking");
}

@end
