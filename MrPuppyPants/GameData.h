#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "LevelObjects.h"
#import "DDGameKitHelper.h"


@interface GameData : CCNode {
 
    int level;
    int maxLevels;
    
    BOOL soundFXMuted;
    BOOL voiceFXMuted;
    BOOL ambientFXMuted;
    
    NSUserDefaults* defaults;
    
    NSString* backgroundHillsFileName;
    NSString* groundPlaneFileName;
    
    int ninjasToTossThisLevel;
    
    int pointsToPassLevel;
    
    int bonusPerExtraNinja;
    
    int pointTotalForAllLevels;
    
    int levelsCompleted;
    
    int eachLevelSectionIsHowManyLevels;

    
    double levelBestTime;
    double levelCurrentTime;
    
    
    double totalNumberOfCoins;
    
    double coinsThisLevel;
    
    NSString *currentBGFilePlaying;
    
    int currentHighScore;
    
    int lastLevelLeveledUp;
    
    BOOL didShowAd;
    
    int timesPaused;
    
    BOOL hasBeenEnoughTimeSinceLastAd;
    
    UIActivityIndicatorView *activityView;
}

-(void)showActivtyIndicator;
-(void)hideActivtyIndicator;
@property (nonatomic,retain) NSMutableDictionary *highScoreDictionary;
@property (nonatomic, assign) BOOL isNewBestTime;
@property (nonatomic, assign) BOOL hasSeenCS54;
@property (nonatomic, assign) BOOL isHighScore;
@property (nonatomic, assign) BOOL hasTweeted;
@property (nonatomic, assign) BOOL hasFacebooked;
@property (nonatomic, assign) BOOL hasWon24thLevel;
@property (nonatomic,readwrite,assign) int pointsScoredCurrentLevel;
@property (nonatomic,readwrite,assign) int lastStarRating;
-(int)getStarRatingForLevel:(int)theLevel;
-(void)setStarRatingForLevel:(int)theLevel;
-(void)setStarRating:(int)rating forLevel:(int)theLevel;

+(GameData*) sharedData;
-(BOOL)shouldShowAd;
-(BOOL)shouldShowPausedAd;
//-(void)tweetAboutGame;
- (BOOL)isGameCenterAvailable;
-(void)addBonusCoins;
-(void)submitAchievementName:(NSString *)name percent:(int)percent;
-(BOOL)isBestTimeForThisLevel;
-(double)returnCoinsThisLevel;
-(double)returnTotalCoinsAvailable;
-(void)addToTotalNumberOfCoins:(double)coins;
-(void)spendCoins:(double)removeThisAmountOfCoins;
-(BOOL)canSpendThisManyCoins:(double)coinAmount;
-(NSString *)formattedTimeForLevel:(double )currentTime;
-(int)returnCoinMultiplier;
-( unsigned char ) returnLevel ;
-(void) levelUp;
-(void) resetGame;
-(int) currentSection;

//-(NSString*) returnBackgroundHillsFileName;
//-(NSString*) returnGroundPlaneFileName ;
//-(int) returnNumberOfNinjasToTossThisLevel;
-(void)populateLevelObjects:(LevelObjects *)levelObject;


//-(int) returnPointsToPassLevel;
-(void) addToPointTotalForAllLevels:(int)pointThisLevel;
//-(int) returnBonusPerExtraNinja ;

-(void)loadThisLevel:(int)newLevel;

-(void) attemptToGoToFirstLevelOfSection:(int)theSection;
-(BOOL) canYouGoToTheFirstLevelOfThisSection:(int) theSection ;
-(void) changeLevelToFirstInThisSection:(int)theSection;

-(void)setLevelWon:(int)levelWon;
-(int)returnScoreAddition;
-(int) returnHighScoreForLevel:(int)lvl;
-(void) setHighScoreForLevel:(int)theScore;

-(BOOL) setBestTimeForLevel:(NSTimeInterval)theTime;
-(NSString *) returnCurrentTimeForLevel;

-(NSString *) returnBestTimeForLevel:(int)lvl;

-(void) turnSoundFXOff;
-(void) turnSoundFXOn;
-(BOOL) areSoundFXMuted; 

-(void) turnVoiceFXOff;
-(void) turnVoiceFXOn;
-(BOOL) areVoiceFXMuted; 

-(void) turnAmbientFXOff;
-(void) turnAmbientFXOn;
-(BOOL) areAmbientFXMuted;

-(BOOL)gun1Purchased;
-(BOOL)gun2Purchased;
-(BOOL)gun3Purchased;

-(void)startMainScreenLoops;
-(void)startLevel_1_Loop;

-(void)setGun1Purchased;
-(void)setGun2Purchased;
-(void)setGun3Purchased;

-(BOOL)isRunningiOS6;
-(BOOL)isExactlyiOS6;

@end
