//
//  MenuBetweenLevels.m
//  MrPants
//
//  Created by Charles Schulze on 9/15/12.
//  Copyright 2012 CS54 INC. All rights reserved.
//

#import "MenuBuyGuns.h"
#import "GameData.h"
#import "MenuSelectLevel.h"
#import "Constants.h"
#import "ButtDance.h"
#import "BaseLevel.h"
#import "DialogAreYouSure.h"

@implementation MenuBuyGuns

+(CCScene *) scene
{
	CCScene *scene = [CCScene node];
    MenuBuyGuns *layer = [MenuBuyGuns node];
    [scene addChild: layer];
    return scene;
}

- (void) resumeBackgroundMusic{
    [[SimpleAudioEngine sharedEngine] resumeBackgroundMusic];
}

-(id) init
{
    if((self = [super init]))
    {
        purchaseGun1 = NO;
        purchaseGun2 = NO;
        purchaseGun3 = NO;
        [Flurry logEvent:@"Buy Guns Clicked"];
        CGSize size = [[CCDirector sharedDirector] winSize];
        
        gameData = [GameData sharedData];
       
        CGPoint menuButtonPosition = ccp(105, 280);
        CGPoint coinLabelPosition = ccp(size.width - 50,size.height - 40);
        CGPoint coinPosition = ccp(size.width - 190,size.height - 50);
        CGFloat fontSize = 22;
        //136 48
        
        if(IS_IPAD){
            menuButtonPosition = ccp(210, 680);
            fontSize = 36;
            
            coinLabelPosition = ccp(size.width - 130,size.height - 70);
            coinPosition = ccp(size.width - 350,size.height - 100);
            //coinPosition = ccp(0,0);
        }
        
        //105 280
        
        CCSprite *imageSprite = [CCSprite spriteWithFile:IMAGE_BG_BUY_GUNS_SCREEN];
        imageSprite.position = ccp(size.width / 2,size.height/2);
        [self addChild:imageSprite];
        
        CCSpriteFrameCache *frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
       
        
        gun1Button = [CCMenuItemImage itemWithNormalSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_GUN_1_BUTTON]]
                                            selectedSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_GUN_1_BUTTON]]
                                                    target:self selector:@selector(buyGun1:)];
        
        gun2Button = [CCMenuItemImage itemWithNormalSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_GUN_2_BUTTON]]
                                            selectedSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_GUN_2_BUTTON]]
                                                    target:self selector:@selector(buyGun2:)];
        
        gun3Button = [CCMenuItemImage itemWithNormalSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_GUN_3_BUTTON]]
                                              selectedSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_GUN_3_BUTTON]]
                                                      target:self selector:@selector(buyGun3:)];
        
        
        backButton = [CCMenuItemImage itemWithNormalSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_BL_BACK_BUTTON]]
                                            selectedSprite:[CCSprite spriteWithSpriteFrame:[frameCache spriteFrameByName:IMAGE_BL_BACK_BUTTON]]
                                                    target:self selector:@selector(backButtonTapped:)];
        
        
        if([[GameData sharedData] gun1Purchased]){
            gun1Button.opacity = 50;
        }
        if([[GameData sharedData] gun2Purchased]){
            gun2Button.opacity = 50;
        }
        if([[GameData sharedData] gun3Purchased]){
            gun3Button.opacity = 50;
        }

        CCMenu *menu = [CCMenu menuWithItems:gun1Button, gun2Button, gun3Button, nil];
        menu.position = ccp(size.width/2, (size.height/2)-30);
        [menu alignItemsVertically];
        [self addChild:menu];
        menu.anchorPoint = ccp(0,0);
        
        CCMenu *menuLower = [CCMenu menuWithItems:backButton, nil];
        menuLower.position = menuButtonPosition;
        [menuLower alignItemsHorizontally];
        [self addChild:menuLower];

        int coinLblWidth = 200;
        int xOffset = 0;
        if(IS_IPAD){
            coinLblWidth = 300;
            xOffset = 20;
        }
        self.coinLabel = [CCLabelTTF labelWithString:@"" dimensions:CGSizeMake(coinLblWidth, 40) hAlignment:kCCTextAlignmentLeft fontName:@"Marker Felt" fontSize:fontSize];
        [self addChild:self.coinLabel z:0];
        self.coinLabel.position = coinLabelPosition;
        [self updateCoinLabel];
        
        //80 and 5
        CCSprite *coin = [CCSprite spriteWithSpriteFrameName:HUD_COIN];
        coin.position = coinPosition;
        coin.anchorPoint = ccp(0,0);
        [self addChild:coin];
        
        
	}
    
   	return self;
}

- (void)updateCoinLabel{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    double myCoinTotal = [defaults doubleForKey:USER_COIN_COUNT];
    NSNumber *myDoubleNumber = [NSNumber numberWithDouble:myCoinTotal];
    
    [self.coinLabel setString:[NSString stringWithFormat:@"Total  $%@",[myDoubleNumber stringValue]]];
}

-(void)showPurchaseWindow{
    
    /*CGSize size = [[CCDirector sharedDirector] winSize];
    CCSprite *overlay = [CCSprite spriteWithFile:MENU_IMAGE_YOU_DONT_HAVE_ENOUGH];
    overlay.position = ccp(size.width / 2,size.height/2);
    [self addChild:overlay];
    */
    
    CGSize size = [[CCDirector sharedDirector] winSize];
    
    buyMore = [CCMenuItemImage itemWithNormalSprite:[CCSprite spriteWithFile:MENU_IMAGE_YOU_DONT_HAVE_ENOUGH]
                                        selectedSprite:[CCSprite spriteWithFile:MENU_IMAGE_YOU_DONT_HAVE_ENOUGH]
                                                target:self selector:@selector(closeBuyMoreMenu:)];
    
    buyMoreMenu = [CCMenu menuWithItems:buyMore, nil];
    buyMoreMenu.position = ccp(size.width / 2,size.height/2);
    [self addChild:buyMoreMenu];
}

- (void)closeBuyMoreMenu:(id)sender{
    [self removeChild:buyMoreMenu cleanup:YES];
}
-(void)cancelPurchase{
    purchaseGun1 = NO;
    purchaseGun2 = NO;
    purchaseGun3 = NO;
}
-(void)completePurchase{
    
    if(purchaseGun1){
        if([gameData gun1Purchased]){
            [Flurry logEvent:@"Repurchase - could not repurchase gun 1"];
            return;
        }
        [Flurry logEvent:@"Purchase - purchased gun 1"];
        [gameData setGun1Purchased];
        [gameData addToTotalNumberOfCoins:-500];
        [self updateCoinLabel];
        gun1Button.opacity = 50;
        
    }else if(purchaseGun2){
        if([gameData gun2Purchased]){
            [Flurry logEvent:@"Repurchase - could not repurchase gun 2"];
            return;
        }
        [Flurry logEvent:@"Purchase - purchased gun 2"];
        [gameData setGun2Purchased];
        [gameData addToTotalNumberOfCoins:-7500];
        [self updateCoinLabel];
        gun2Button.opacity = 50;
    }else if(purchaseGun3){
        if([gameData gun3Purchased]){
            [Flurry logEvent:@"Repurchase - could not repurchase gun 3"];
            return;
        }
        [Flurry logEvent:@"Purchase - purchased gun 3"];
        [gameData setGun3Purchased];
        [gameData addToTotalNumberOfCoins:-15500];
        [self updateCoinLabel];
        gun3Button.opacity = 50;
    }
}

-(void)showDialog{
    [self addChild:[DialogAreYouSure scene]];
}
- (void)buyGun1:(id)sender{
    
    if([gameData gun1Purchased]){
        [Flurry logEvent:@"Repurchase - could not repurchase gun 1"];
        return;
    }
    if([gameData returnTotalCoinsAvailable] < 500){
        [self showPurchaseWindow];
        [Flurry logEvent:@"Purchase - not enought coins to buy gun 1"];
        return;
    }
    
    purchaseGun1 = YES;
    [self showDialog];
    
    
}
- (void)buyGun2:(id)sender
{
    if([gameData gun2Purchased]){
        [Flurry logEvent:@"Repurchase - could not repurchase gun 2"];
        return;
    }
    if([gameData returnTotalCoinsAvailable] < 7500){
        [Flurry logEvent:@"Purchase - not enought coins to buy gun 2"];
        [self showPurchaseWindow];
        return;
    }
    purchaseGun2 = YES;
    [self showDialog];
    
}

- (void)buyGun3:(id)sender
{
    if([gameData gun3Purchased]){
        [Flurry logEvent:@"Repurchase - could not repurchase gun 3"];
        return;
    }
    if([gameData returnTotalCoinsAvailable] < 15500){
        [Flurry logEvent:@"Purchase - not enought coins to buy gun 3"];
        [self showPurchaseWindow];
        return;
    }
    purchaseGun3 = YES;
    [self showDialog];
    
}

- (void)backButtonTapped:(id)sender
{
    
    if([self.parent.parent isKindOfClass:[MenuBetweenLevels class]])
    {
        MenuBetweenLevels *betweenLevels = (MenuBetweenLevels *)self.parent.parent;
        [betweenLevels updateCoins];
        [Flurry logEvent:@"Exit Buy Guns Screen from : MenuBetweenLevels"];
    }else{
        [Flurry logEvent:@"Exit Buy Guns Screen from : Pause popup"];
    }
    
    [self removeFromParentAndCleanup:YES];
    //[[CCDirector sharedDirector] replaceScene:[MenuBetweenLevels scene]];
}

-(void)dealloc
{
    [super dealloc];
}

@end
