//
//  SpringGun.h
//  MustachioRope
//
//  Created by Charles Schulze on 6/17/12.
//  Copyright 2012 CS54 INC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "Box2D.h"
#import "BallsMenu.h"


@interface SpringGun : CCNode
{
    CCSprite *pawsAndGun;
    CCSprite *spring;
    CCSprite *explosion;
    CCRepeat *repeat;
}


-(void)setBallsMenu:(BallsMenu *)ballsMenu;
-(id) initInParent:(CGPoint)pos;
-(void)setSpringScale:(float)scale;
-(void)showExplosion;

- (void)ball1Selected;
- (void)ball2Selected;
- (void)ball3Selected;
- (void)ball4Selected;

@end
