//
//  Ship.h
//  SpriteBatches
//
//  Created by Steffen Itterheim on 04.08.10.
//
//  Updated by Andreas Loew on 20.06.11:
//  * retina display
//  * framerate independency
//  * using TexturePacker http://www.texturepacker.com
//
//  Copyright Steffen Itterheim and Andreas Loew 2010-2011. 
//  All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "SpringGun.h"
#import "BallsMenu.h"

@interface MrPants : CCSprite
{
    CCSprite *pawsAndGun;
    CCSprite *jetpack;
    CCSprite *mrPants;
    CCSprite *dot;
    SpringGun *gun;
    
}
@property (nonatomic, assign) BOOL hasStoppedWalking;
+(id) ship;
-(void)setSpringScale:(float)scale;
-(void)setRotationOfGun:(float)rotation;
-(void)shootGun;

- (void)ball1Selected;
- (void)ball2Selected;
- (void)ball3Selected;
- (void)ball4Selected;

-(void)startWalkAnimation;
-(void)stopWalkAnimation;
-(void)startJetpackAnimation;

@end
